#include "utils.hxx"
#include "ExtendedModel.hxx"
#include "ExtendedMinimizer.hxx"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class ExtendedModel+;
#pragma link C++ class ExtendedMinimizer+;

#endif // __CINT__
