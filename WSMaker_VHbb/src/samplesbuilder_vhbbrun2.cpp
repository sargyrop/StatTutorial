#include "samplesbuilder_vhbbrun2.hpp"

#include <set>
#include <unordered_map>
#include <utility>
#include <iostream>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/sample.hpp"


void SamplesBuilder_VHbbRun2::declareSamples() {


  TString massPoint = m_config.getValue("MassPoint", "125");
  bool doSTXS = m_config.getValue("DoSTXS", false);

  // data or pseudo-data should always be there
  addData();


  // Signal samples and corresponding backgrounds
  if(m_config.getValue("DoDiboson", false)){ // considering diboson as the signal
    addSample(Sample("WZ", SType::Sig, kGray));
    addSample(Sample("ZZ", SType::Sig, kGray + 1));
    // higgs background at 125GeV
    addBkgs( { {"WlvH125", kRed}, {"qqWlvH125", kRed}, {"ggZvvH125", kRed}, {"qqZvvH125", kRed},
             {"ggZllH125", kRed}, {"qqZllH125", kRed}, {"ZllH125", kRed} } );
    addBkgs( { {"WW", kGray + 3} } );
  }
  else if (doSTXS) {
    declareSamples_STXS();
    // diboson bkg
    addBkgs( { {"WZ", kGray}, {"ZZ", kGray + 1}, {"WW", kGray + 3} } );
  }
  else { // H125 signal
    // signal
    addSample(Sample("ZllH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("ggZllH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("qqZllH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("WlvH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("qqWlvH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("ggZvvH"+massPoint, SType::Sig,  kRed));
    addSample(Sample("qqZvvH"+massPoint, SType::Sig,  kRed));

    // diboson bkg
    addBkgs( { {"WZ", kGray}, {"ZZ", kGray + 1}, {"WW", kGray + 3} } );

  }

  // Top, single-top, W/Z+jets and multijet
  // W+jets
  addBkgs( { {"Wl", kGreen - 9} } );
  addBkgs( { {"Wcl", kGreen - 6}, {"Wcc", kGreen - 3}, {"Wbl", kGreen + 2}, {"Wbc", kGreen + 3}, {"Wbb", kGreen + 4} } );

  // Z+jets
  addBkgs( { {"Zl", kAzure - 9} } );
  addBkgs( { {"Zcl", kAzure - 4}, {"Zcc", kAzure - 2}, {"Zbl", kAzure + 2}, {"Zbc", kAzure + 3}, {"Zbb", kAzure + 4} } );

  // Top and single-top
  addBkgs( { {"ttbar", kOrange} } );
  addBkgs( { {"stopt", kYellow - 7}, {"stops", kYellow - 5}, {"stopWt", kOrange + 3} } );

  // Multijet. Declare all 3. Veto multijet for 1 lepton in engine.
  addSample(Sample("multijet", SType::DataDriven, kPink + 1));
  addSample(Sample("multijetEl", SType::DataDriven, kPink + 1));
  addSample(Sample("multijetMu", SType::DataDriven, kPink + 2));

}


void SamplesBuilder_VHbbRun2::declareKeywords() {

  TString massPoint = m_config.getValue("MassPoint", "125");
  bool doSTXS = m_config.getValue("DoSTXS", false);
  // from run1
  // First, some user-definitions
  m_keywords = {
    {"Diboson", {"WZ", "ZZ", "WW"}},
    {"Zjets", {"Zcl", "Zcc", "Zbl", "Zbb", "Zl", "Zbc"}},
    {"Zhf", {"Zbl", "Zbb", "Zbc", "Zcc"}},
    {"Wjets", {"Wcl", "Wcc", "Wbl", "Wbb", "Wl", "Wbc"}},
    {"Whf", {"Wcc", "Wbl", "Wbb", "Wbc"}},
    {"Stop", {"stops", "stopt", "stopWt"}},
    {"Top", {"ttbar", "stops", "stopt", "stopWt"}}
  };

  for (const auto& spair : m_samples) {
    const Sample& s = spair.second;
    // Higgs samples deserve shorthands too
    if(s.name().Contains("ZvvH")) {
      m_keywords["ZvvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["ZH"].insert(s.name());
    }
    if(s.name().Contains("WlvH")) {
      m_keywords["WlvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["WH"].insert(s.name());
    }
    if(s.name().Contains("ZllH")) {
      m_keywords["ZllH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["ZH"].insert(s.name());
    }
    if(s.name().BeginsWith("qq") || s.name().BeginsWith("Wlv")) {
      m_keywords["qqVH"].insert(s.name());
    }
    if(s.name().BeginsWith("qqZ")) {
      m_keywords["qqZH"].insert(s.name());
    }
    if(s.name().BeginsWith("ggZ")) {
      m_keywords["ggZH"].insert(s.name());
    }
  }
  if (doSTXS) {
    declareKeywords_STXS();
  }

}

void SamplesBuilder_VHbbRun2::declareSamplesToMerge() {
  bool doDiboson = m_config.getValue("DoDiboson", false);
  bool doWHZH = m_config.getValue("FitWHZH", false);
  bool doWZZZ = m_config.getValue("FitWZZZ", false);
  bool doVHVZ = m_config.getValue("FitVHVZ", false);
  bool doSTXS = m_config.getValue("DoSTXS", false);
  TString massPoint = m_config.getValue("MassPoint", "125");

  // VH sample merging
  if(doWHZH) {
    declareMerging(Sample("ZH"+massPoint, SType::Sig, kRed), {"ZH"});
    declareMerging(Sample("WH"+massPoint, SType::Sig, kRed), {"WH"});
  }
  else if (doDiboson) {
    declareMerging(Sample("VH"+massPoint, SType::Bkg, kRed), {"Higgs"});
  }
  else if (doSTXS) {
    declareSamplesToMerge_STXS();
  }
  else {
    declareMerging(Sample("VH"+massPoint, SType::Sig, kRed), {"Higgs"});
  }

  // diboson sample merging
  if(!doWZZZ) {
    if(doDiboson) {
      declareMerging(Sample("VZ", SType::Sig, kGray), {"Sig"});
    }
    else if(doVHVZ) {
      declareMerging(Sample("VZ", SType::Bkg, kGray), {"WZ", "ZZ"});
    }
    else {
      declareMerging(Sample("diboson", SType::Bkg, kGray), {"Diboson"});
    }
  }

  declareMerging(Sample("stop", SType::Bkg, kYellow - 7), {"Stop"});
  declareMerging(Sample("Whf", SType::Bkg, kGreen + 4), {"Wbb", "Wbc", "Wbl", "Wcc"});
  declareMerging(Sample("Zhf", SType::Bkg, kAzure + 4), {"Zbb", "Zbc", "Zbl", "Zcc"});
}

void SamplesBuilder_VHbbRun2::declareSamples_STXS() {

  // STXS signals stage1++ 36
  addSample(Sample("QQ2HNUNUxFWDH", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVx0x75", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVx75x150", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVx150x250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVx150x250xGE1J", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVxGT250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HNUNUxPTVxGT250xGE1J", SType::Sig,  kRed));

  addSample(Sample("QQ2HLNUxFWDH", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVx0x75", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVx75x150", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVx150x250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVx150x250xGE1J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVxGT250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLNUxPTVxGT250xGE1J", SType::Sig,  kRed));

  addSample(Sample("GG2HNUNUxFWDH", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVx0x75", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVx75x150", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVx150x250x0J", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVx150x250xGE1J", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVxGT250x0J", SType::Sig,  kRed));
  addSample(Sample("GG2HNUNUxPTVxGT250xGE1J", SType::Sig,  kRed));

  addSample(Sample("QQ2HLLxFWDH", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVx0x75", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVx75x150", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVx150x250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVx150x250xGE1J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVxGT250x0J", SType::Sig,  kRed));
  addSample(Sample("QQ2HLLxPTVxGT250xGE1J", SType::Sig,  kRed));

  addSample(Sample("GG2HLLxFWDH", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVx0x75", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVx75x150", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVx150x250x0J", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVx150x250xGE1J", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVxGT250x0J", SType::Sig,  kRed));
  addSample(Sample("GG2HLLxPTVxGT250xGE1J", SType::Sig,  kRed));

  addSample(Sample("UNKNOWN", SType::Sig,  kRed));

}

void SamplesBuilder_VHbbRun2::declareKeywords_STXS() {

  for (const auto& spair : m_samples) {
    const Sample& s = spair.second;
    TString name = s.name();
    if(name.Contains("2HNUNU")) {
      m_keywords["ZvvH"].insert(name);
      m_keywords["Higgs"].insert(name);
      m_keywords["ZH"].insert(name);
    }
    if(name.Contains("QQ2HLNU")) {
      m_keywords["WlvH"].insert(name);
      m_keywords["Higgs"].insert(name);
      m_keywords["WH"].insert(name);
    }
    if(name.Contains("2HLL")) {
      m_keywords["ZllH"].insert(name);
      m_keywords["Higgs"].insert(name);
      m_keywords["ZH"].insert(name);
    }
    if(name.BeginsWith("QQ") ) {
      m_keywords["STXS"].insert(name);
      m_keywords["qqVH"].insert(name);
    }
    if(name.BeginsWith("QQ2HLL") || name.BeginsWith("QQ2HNUNU")) {
      m_keywords["qqZH"].insert(name);
    }
    if(name.BeginsWith("GG")) {
      m_keywords["STXS"].insert(name);
      m_keywords["ggZH"].insert(name);
    }
    if( name.Contains("UNKNOWN") || name.Contains("FWDH") ) m_keywords["STXSRest"].insert(name);
  }

  // stage 1 splitting
  //{"STXSQQWH150x250PTV0J",   {"QQ2HLNUxPTVx150x250x0J"}}, // 1
  //{"STXSQQWH150x250PTVGE1J", {"QQ2HLNUxPTVx150x250xGE1J"}}, // 1
  m_keywords["STXSQQZH0x150PTV"] =        {"QQ2HNUNUxPTVx0x75", "QQ2HNUNUxPTVx75x150", "QQ2HLLxPTVx0x75", "QQ2HLLxPTVx75x150"}; // 4
  m_keywords["STXSQQZH150x250PTV0J"] =    {"QQ2HNUNUxPTVx150x250x0J", "QQ2HLLxPTVx150x250x0J"}; // 2
  m_keywords["STXSQQZH150x250PTVGE1J"] =  {"QQ2HNUNUxPTVx150x250xGE1J", "QQ2HLLxPTVx150x250xGE1J"}; // 2
  m_keywords["STXSQQZHGT250PTV"] =        {"QQ2HNUNUxPTVxGT250x0J", "QQ2HNUNUxPTVxGT250xGE1J", "QQ2HLLxPTVxGT250x0J","QQ2HLLxPTVxGT250xGE1J"}; // 4
  m_keywords["STXSQQWH0x150PTV"] =        {"QQ2HLNUxPTVx0x75","QQ2HLNUxPTVx75x150"}; // 2
  m_keywords["STXSQQWHGT250PTV"] =        {"QQ2HLNUxPTVxGT250x0J","QQ2HLNUxPTVxGT250xGE1J"}; // 2
  m_keywords["STXSGGZH0x150PTV"] =        {"GG2HNUNUxPTVx0x75","GG2HNUNUxPTVx75x150", "GG2HLLxPTVx0x75","GG2HLLxPTVx75x150"}; // 4
  m_keywords["STXSGGZHGT150PTV0J"] =      {"GG2HNUNUxPTVx150x250x0J","GG2HNUNUxPTVxGT250x0J", "GG2HLLxPTVx150x250x0J","GG2HLLxPTVxGT250x0J"}; // 4
  m_keywords["STXSGGZHGT150PTVGE1J"] =    {"GG2HNUNUxPTVx150x250xGE1J","GG2HNUNUxPTVxGT250xGE1J", "GG2HLLxPTVx150x250xGE1J","GG2HLLxPTVxGT250xGE1J"}; // 4

}

void SamplesBuilder_VHbbRun2::declareSamplesToMerge_STXS(){
  int  FitSTXSScheme = m_config.getValue("FitSTXSScheme", 1);
  switch( FitSTXSScheme ){
    case 1: // merging all STXS signals into an inclusive signal, should be same as VHbb
      declareMerging(Sample("VHSTXS",   SType::Sig, kRed), {"STXS"});
      break;
    case 2:
      declareMerging(Sample("VHSTXSQQZH0x150PTV",       SType::Sig, kRed), {"STXSQQZH0x150PTV"});
      declareMerging(Sample("VHSTXSQQZH150x250PTV0J",   SType::Sig, kRed), {"STXSQQZH150x250PTV0J"});
      declareMerging(Sample("VHSTXSQQZH150x250PTVGE1J", SType::Sig, kRed), {"STXSQQZH150x250PTVGE1J"});
      declareMerging(Sample("VHSTXSQQZHGT250PTV",       SType::Sig, kRed), {"STXSQQZHGT250PTV"});
      declareMerging(Sample("VHSTXSQQWH0x150PTV",       SType::Sig, kRed), {"STXSQQWH0x150PTV"});
      declareMerging(Sample("VHSTXSQQWHGT250PTV",       SType::Sig, kRed), {"STXSQQWHGT250PTV"});
      declareMerging(Sample("VHSTXSGGZH0x150PTV",       SType::Sig, kRed), {"STXSGGZH0x150PTV"});
      declareMerging(Sample("VHSTXSGGZHGT150PTV0J",     SType::Sig, kRed), {"STXSGGZHGT150PTV0J"});
      declareMerging(Sample("VHSTXSGGZHGT150PTVGE1J",   SType::Sig, kRed), {"STXSGGZHGT150PTVGE1J"});
      declareMerging(Sample("VHSTXSRest",               SType::Sig, kRed), {"STXSRest"});
      break;
  }
}
