#include "systematiclistsbuilder_vhbbrun2.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/properties.hpp"
#include "WSMaker/regiontracker.hpp"
#include "WSMaker/sample.hpp"
#include "WSMaker/systematic.hpp"
#include "WSMaker/finder.hpp"

void SystematicListsBuilder_VHbbRun2::fillHistoSystsRenaming() {
  // rewrite rules

  // todo Ask 2 lepton guys to follow same convention as 0+1 lepton so we can get rid of this
  std::vector<std::string> btagSysts { "extrapolation_from_charm", "extrapolation",
    "Eigen_Light_0", "Eigen_Light_1", "Eigen_Light_2", "Eigen_Light_3", "Eigen_Light_4",
    "Eigen_Light_5", "Eigen_Light_6", "Eigen_Light_7", "Eigen_Light_8", "Eigen_Light_9",
    "Eigen_Light_10", "Eigen_Light_11", "Eigen_Light_12", "Eigen_Light_13",
    "Eigen_C_0", "Eigen_C_1", "Eigen_C_2", "Eigen_C_3",
    "Eigen_B_0", "Eigen_B_1", "Eigen_B_2", "Eigen_B_3", "Eigen_B_4", "Eigen_B_5"
  };

  for (auto sysname : btagSysts) {
    m_renameHistoSysts.emplace( "SysFT_EFF_"+sysname+"_AntiKt4EMTopoJets" , "SysFT_EFF_"+sysname);
  }
}

void SystematicListsBuilder_VHbbRun2::listAllUserSystematics(const RegionTracker& regTrk, bool useFltNorms) {
  using P = Property;

  bool doDiboson = m_config.getValue("DoDiboson", false);
  bool doWHZH = m_config.getValue("FitWHZH", false);
  bool doWZZZ = m_config.getValue("FitWZZZ", false);
  bool doVHVZ = m_config.getValue("FitVHVZ", false);
  bool doSTXS = m_config.getValue("DoSTXS", false);
  bool doCutBase = m_config.getValue("CutBase", false);
  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");

  TString massPoint = m_config.getValue("MassPoint", "125");

  bool defltFitConfig = m_config.getValue("DefaultFitConfig", false);
  bool hasZeroLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==0);
  bool hasOneLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==1);
  bool hasTwoLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==2);

  // DO NOT APPLY ! RESCALE ALL MC TO A DIFFERENT INTEGRATED LUMINOSITY
  //  normFact("lumirescale", {"MC"}, 5.81/13.18, -10, 10, true); // this one is constant
  //  normFact("lumirescale", {"MC"}, 7.37/13.18, -10, 10, true); // this one is constant

  // list them all !

  if(doWHZH) {
    addPOI("SigXsecOverSMWH", {"WH"}, 1, -40, 40);
    addPOI("SigXsecOverSMZH", {"ZH"}, 1, -40, 40);
  }
  else if(doWZZZ) {
    addPOI("SigXsecOverSMWZ", {"WZ"}, 1, -40, 40);
    addPOI("SigXsecOverSMZZ", {"ZZ"}, 1, -40, 40);
  }
  else if(doSTXS) {
    AddPOIsForSTXS();
  }
  else if(sig_decorr.size() != 0) { // TODO or just extend decorrSysForCategories for the POIs ?
    std::vector<Property> decorrelations;
    for(auto& d : sig_decorr) {
      decorrelations.push_back(Properties::props_from_names.at(d));
    }
    addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 1, -40, 40);
  } else {
    //addPOI("SigXsecOverSM", {"Sig", {{}}, {P::nLep}}, 1, -40, 40);
    addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
  }

  // ttbar
  // NM 17-05-16
  // floating parameters decorrelated between 2 and (0+1) lepton channels.
  // In 2 lepton, float 2jet and 3jet independently
  normFact("ttbar", SysConfig{"ttbar"}.decorrIn({ (P::nLep==2)&&(P::nJet==2), (P::nLep==2)&&(P::nJet==3) }));

  // 3/2J uncertainty
  // NM 17-05-19
  // correlated (0+1) lepton channels
  // 9% prior for 1-lepton, 9% prior for 0 lepton
  // Applied on 2jet regions
  normSys("SysttbarNorm", 0.09, SysConfig{"ttbar"}.applyIn((P::nJet==2)&&(P::nLep==0)).decorr(P::nJet));
  normSys("SysttbarNorm", 0.09, SysConfig{"ttbar"}.applyIn((P::nLep==1)&&(P::nJet==2)).decorr(P::nJet));

  // 0L/1L uncertainty: 8%
  // NM 17-05-19
  if(hasOneLep) {
    normSys("SysttbarNorm", 0.08, SysConfig{"ttbar"}.applyIn(P::nLep==0).decorr(P::nLep));
  }

  // 1L WhfCR vs SR: 25%
  // NM 17-05-19
  normSys("SysttbarNorm", 0.25, SysConfig{"ttbar"}.applyIn(P::descr=="WhfCR").decorr({P::descr, P::nLep}));


  // single top
  // cross-sections from Top MC Twiki
  sampleNormSys("stops", 0.046);
  sampleNormSys("stopt", 0.044);//PF
  sampleNormSys("stopWt", 0.062);//PF
  normSys("SysstoptAcc", 0.17, SysConfig{"stopt"}.applyIn(P::nJet==2));//PF
  normSys("SysstoptAcc", 0.20, SysConfig{"stopt"}.applyIn(P::nJet==3));//PF
  normSys("SysstopWtAcc", 0.35, SysConfig{"stopWt"}.applyIn(P::nJet==2));//PF
  normSys("SysstopWtAcc", 0.41, SysConfig{"stopWt"}.applyIn(P::nJet==3));//PF

  // V+jets
  //
  // UPDATE of all V+jets normalizations uncertainties:
  // NM 17-05-19

  // Wl and Zl
  sampleNormSys("Wl", 0.32);
  sampleNormSys("Zl", 0.18);

  // Wcl and Zcl
  sampleNormSys("Wcl", 0.37);
  sampleNormSys("Zcl", 0.23);

  // floating Wbb+Wbc+Wbl+Wcc - put extra systs on Wbl and Wcc to allow ratios to change
  if(hasOneLep) {
    normFact("Wbb", SysConfig{"Whf"}.decorr(P::nJet));
    // 0/1L
    normSys("SysWbbNorm", 0.05, SysConfig{"Whf"}.applyIn(P::nLep==0).decorr(P::nLep));
  }
  else {
    normSys("SysWbbNorm", 0.33, SysConfig{"Whf"}.decorr(P::nJet));
  }

  // CR to SR extrapolation: 7% on SR
  if (!doCutBase) {
    normSys("SysWbbNorm", 0.10, SysConfig{"Whf"}.applyIn(P::descr=="WhfSR").decorr(P::descr)); //Only use WhfCR when running MVA fit
  }

  // FLAVOUR COMPOSITION
  normSys("SysWblWbbRatio", 0.10, SysConfig{"Wbl"}.applyIn(P::nLep==0));
  normSys("SysWblWbbRatio", 0.30, SysConfig{"Wbl"}.applyIn(P::nLep==1));
  //
  normSys("SysWccWbbRatio", 0.26, SysConfig{"Wcc"}.applyIn(P::nLep==0));
  normSys("SysWccWbbRatio", 0.23, SysConfig{"Wcc"}.applyIn(P::nLep==1));
  //
  normSys("SysWbcWbbRatio", 0.15, SysConfig{"Wbc"}.applyIn(P::nLep==0));
  normSys("SysWbcWbbRatio", 0.30, SysConfig{"Wbc"}.applyIn(P::nLep==1));

  // floating Zbb+Zbc+Zbl+Zcc - put extra systs on Zbl and Zcc to allow ratios to change
  if(hasTwoLep || hasZeroLep) {
    normFact("Zbb", SysConfig{"Zhf"}.decorr(P::nJet));
  }
  else {
    normSys("SysZbbNorm", 0.48, {"Zhf"});
  }
  // 0/2L channel
  if(hasTwoLep) {
    normSys("SysZbbNorm", 0.07, SysConfig{"Zhf"}.applyIn(P::nLep==0).decorr(P::nLep));
  }

  // Let's introduce helpers to define the uncertainties
  PropertiesSet ps_2Lep2Jet = (P::nLep==2)&&(P::nJet==2);
  PropertiesSet ps_2Lep3Jet = (P::nLep==2)&&(P::nJet==3);
  PropertiesSet ps_0Lep2Jet = (P::nLep==0)&&(P::nJet==2);
  PropertiesSet ps_0Lep3Jet = (P::nLep==0)&&(P::nJet==3);

  // Flavour composition
  normSys("SysZblZbbRatio", 0.25, SysConfig{"Zbl"}.applyIn(P::nLep==0));
  normSys("SysZblZbbRatio", 0.28, SysConfig{"Zbl"}.applyIn(ps_2Lep2Jet));
  normSys("SysZblZbbRatio", 0.20, SysConfig{"Zbl"}.applyIn(ps_2Lep3Jet));
  //
  normSys("SysZccZbbRatio", 0.15, SysConfig{"Zcc"}.applyIn(P::nLep==0));
  normSys("SysZccZbbRatio", 0.16, SysConfig{"Zcc"}.applyIn(ps_2Lep2Jet));
  normSys("SysZccZbbRatio", 0.13, SysConfig{"Zcc"}.applyIn(ps_2Lep3Jet));
  //
  normSys("SysZbcZbbRatio", 0.40, SysConfig{"Zbc"}.applyIn(P::nLep==0));
  normSys("SysZbcZbbRatio", 0.40, SysConfig{"Zbc"}.applyIn(ps_2Lep2Jet));
  normSys("SysZbcZbbRatio", 0.30, SysConfig{"Zbc"}.applyIn(ps_2Lep3Jet));

  // Diboson
  // Overall norm
  normSys("SysWWNorm", 0.25, {"WW"});
  if ( doVHVZ ) {
    // do VH fit using floating VZ
    normFact("VZ", {{"WZ", "ZZ"}});
  } else if ( !doDiboson ) {
    normSys("SysZZNorm", 0.20, {"ZZ"});
    normSys("SysWZNorm", 0.26, {"WZ"});
  }

  normSys("SysVZUEPSAcc", 0.056, SysConfig{"ZZ"}.applyIn(P::nLep==0));
  normSys("SysVZUEPSAcc", 0.039, {"WZ"});
  normSys("SysVZUEPSAcc", 0.058, SysConfig{"ZZ"}.applyIn(P::nLep==2));

  normSys("SysVZUEPS", 0.073, SysConfig{"ZZ"}.applyIn(ps_0Lep3Jet).decorr(P::nJet));
  normSys("SysVZUEPS", 0.108, SysConfig{"WZ"}.applyIn(P::nJet==3).decorr(P::nJet));
  normSys("SysVZUEPS", 0.031, SysConfig{"ZZ"}.applyIn(ps_2Lep3Jet).decorr(P::nJet));

  normSys("SysZZUEPSResid", 0.06, SysConfig{"ZZ"}.applyIn(P::nLep==0).decorr(P::nLep));
  normSys("SysWZUEPSResid", 0.11, SysConfig{"WZ"}.applyIn(P::nLep==0).decorr(P::nLep));

  normSys("SysVZQCDscale_J2", 0.103, SysConfig{"ZZ"}.applyIn(ps_0Lep2Jet));
  normSys("SysVZQCDscale_J2", 0.127, SysConfig{"WZ"}.applyIn(P::nJet==2));
  normSys("SysVZQCDscale_J2", 0.119, SysConfig{"ZZ"}.applyIn(ps_2Lep2Jet));

  normSys("SysVZQCDscale_J3", -0.152, SysConfig{"ZZ"}.applyIn(ps_0Lep2Jet));
  normSys("SysVZQCDscale_J3", 0.174, SysConfig{"ZZ"}.applyIn(ps_0Lep3Jet));
  normSys("SysVZQCDscale_J3", -0.177, SysConfig{"WZ"}.applyIn(P::nJet==2));
  normSys("SysVZQCDscale_J3", 0.212, SysConfig{"WZ"}.applyIn(P::nJet==3));
  normSys("SysVZQCDscale_J3", -0.164, SysConfig{"ZZ"}.applyIn(ps_2Lep2Jet));
  normSys("SysVZQCDscale_J3", 0.101, SysConfig{"ZZ"}.applyIn(ps_2Lep3Jet));

  normSys("SysVZQCDscale_JVeto", 0.182, SysConfig{"ZZ"}.applyIn(ps_0Lep3Jet));
  normSys("SysVZQCDscale_JVeto", 0.190, SysConfig{"WZ"}.applyIn(P::nJet==3));

  // extrapolation uncertainties

  // Higgs
  //add Higgs normalisation systematic of 50% when running diboson fit
  if(doDiboson) {
    normSys("SysHiggsNorm", 0.50, {"Higgs"});
  }

  // Branching ratio
  // numbers from Paolo, 2016/07/19 PF
  normSys("SysTheoryBRbb", 0.017, {"Higgs"});

  // SIGNAL SYS
  normSys("SysTheoryAcc_J2" , 0.069, SysConfig{"ZvvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J2" , 0.088, SysConfig{"WlvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J2" , 0.033, SysConfig{"ZllH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF

  normSys("SysTheoryAcc_J3" , -0.07,  SysConfig{"ZvvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J3" , 0.05,   SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J3" , -0.086, SysConfig{"WlvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J3" , 0.068,  SysConfig{"WlvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J3" , -0.032, SysConfig{"ZllH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_J3" , 0.039,  SysConfig{"ZllH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF

  normSys("SysTheoryAcc_JVeto", -0.025, SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryAcc_JVeto", 0.038,  SysConfig{"WlvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF


  normSys("SysTheoryPDFAcc", 0.011, SysConfig{"ZvvH"}.decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryPDFAcc", 0.013, SysConfig{"WlvH"}.decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryPDFAcc", 0.005, SysConfig{"ZllH"}.decorrTo({"qqVH", "ggZH"}));//PF


  // UE/PS uncertainties on the Higgs

  normSys("SysTheoryUEPSAcc", 0.100, {"ZvvH"});//PF
  normSys("SysTheoryUEPSAcc", 0.121, {"WlvH"});//PF
  normSys("SysTheoryUEPSAcc", 0.139, {"ZllH"});//PF
  normSys("SysTheoryUEPSAcc", 0.13,  SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorr(P::nJet));//PF
  normSys("SysTheoryUEPSAcc", 0.129, SysConfig{"WlvH"}.applyIn(P::nJet==3).decorr(P::nJet));//PF
  normSys("SysTheoryUEPSAcc", 0.134, SysConfig{"ZllH"}.applyIn(P::nJet==3).decorr(P::nJet));//PF

  // PDF and scales for total x-sec uncertainty

  normSys("SysTheoryQCDscale", 0.007, SysConfig{"qqVH"}.decorrTo({"qqVH", "ggZH"}));//PF
  normSys("SysTheoryQCDscale", 0.27, SysConfig{"ggZH"}.decorrTo({"qqVH", "ggZH"}));//PF

  normSys("SysTheoryPDF_qqVH", 0.019, {"WlvH"});//PF
  normSys("SysTheoryPDF_qqVH", 0.016, {"qqZH"});//PF
  normSys("SysTheoryPDF_ggZH", 0.05, {"ggZH"});//PF


  // multijet

  /* From Yanhui MA for v28 inputs!
   * https://yama.web.cern.ch/yama/slides/yanhui20170323.pdf
   *
   * Here I also list the normalization uncertainties for el and mu in different signal regions :
   *
   * For el 2jet :  +66% -62%
   * For el 3jet : +532% -100%
   * For mu 2jet :  +88% -100%
   * For mu 3jet : +595% -100%
   *
   * cut based:
   * For el 2jet : +220% -100%
   * For el 3jet : +208% -100%
   * For mu 2jet : + 75% -100%
   * For mu 3jet : +363% -100%
   *
   */

  if (!doCutBase){
    normSys("SysMJNorm_2J_El", 0.41, 1.63, SysConfig{"multijetEl"}.applyIn(P::nJet==2));//sys_V28 For v18.v03
    normSys("SysMJNorm_3J_El", 0.0,  5.16, SysConfig{"multijetEl"}.applyIn(P::nJet==3));//sys_V28 For v18.v03
    normSys("SysMJNorm_2J_Mu", 0.0,  1.56, SysConfig{"multijetMu"}.applyIn(P::nJet==2));//sys_V28 For v18.v03
    normSys("SysMJNorm_3J_Mu", 0.0,  2.98, SysConfig{"multijetMu"}.applyIn(P::nJet==3));//sys_V28 For v18.v03
  }else{
    normSys("SysMJNorm_2J_El", 0.0, 2.9,  SysConfig{"multijetEl"}.applyIn(P::nJet==2));//sys_V28 for v18.v03.cutbased
    normSys("SysMJNorm_3J_El", 0.0, 2.95, SysConfig{"multijetEl"}.applyIn(P::nJet==3));//sys_V28 for v18.v03.cutbased
    normSys("SysMJNorm_2J_Mu", 0.0, 1.72, SysConfig{"multijetMu"}.applyIn(P::nJet==2));//sys_V28 for v18.v03.cutbased
    normSys("SysMJNorm_3J_Mu", 0.0, 3.75, SysConfig{"multijetMu"}.applyIn(P::nJet==3));//sys_V28 for v18.v03.cutbased
  }

  normSys("ATLAS_LUMI_2015_2016", 0.032, SysConfig{"MC"}.applyIn(P::year==2015)); //Update lumi uncertainty for 2015+2016 GRLs: 36.07 fb-1

}


void SystematicListsBuilder_VHbbRun2::listAllHistoSystematics(const RegionTracker& regTrk) {
  using T   = SysConfig::Treat;
  using S   = SysConfig::Smooth;
  using Sym = SysConfig::Symmetrise;
  using P   = Property;

  SysConfig::SmoothingFunction VHbbSmoothing = [](const PropertiesSet& pset, const Sample&) {
    TString dist = pset.getStringProp(Property::dist);
    if(dist == "mjj" || dist == "mBB" || dist == "mBBMVA") {
      return S::smoothRebinParabolic;
    }
    return S::smoothRebinMonotonic;
  };

  SysConfig noSmoothConfig          { T::shape, S::noSmooth, Sym::noSym };
  SysConfig noSmoothShapeOnlyConfig { T::shapeonly, S::noSmooth, Sym::noSym };
  SysConfig smoothConfig            { T::shape, VHbbSmoothing, Sym::noSym };
  SysConfig smoothAndSymConfig      { T::shape, VHbbSmoothing, Sym::symmetriseAverage };

  // AT THE MOMENT ONLY RECO SYSTEMATIC CONSIDERED
  m_histoSysts.insert({ "SysEG_RESOLUTION_ALL" , smoothConfig});
  m_histoSysts.insert({ "SysEG_SCALE_ALL" , smoothConfig});

  m_histoSysts.insert({ "SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});

  //m_histoSysts.insert({ "SysEL_EFF_ID_TotalCorrUncertainty" , noSmoothConfig});
  //m_histoSysts.insert({ "SysEL_EFF_Iso_TotalCorrUncertainty" , noSmoothConfig});
  //m_histoSysts.insert({ "SysEL_EFF_Reco_TotalCorrUncertainty" , noSmoothConfig});

  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , smoothAndSymConfig});

  m_histoSysts.insert({ "SysMETTrigStat" , smoothConfig});
  m_histoSysts.insert({ "SysMETTrigTop" , smoothConfig});
  m_histoSysts.insert({ "SysMETTrigZ" , smoothConfig});
  m_histoSysts.insert({ "SysMET_JetTrk_Scale" , smoothConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPerp" , smoothAndSymConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPara" , smoothAndSymConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_Scale" , smoothConfig});

  m_histoSysts.insert({ "SysMUON_EFF_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_STAT" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_ISO_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_ISO_STAT" , noSmoothConfig});

  // FIXME NM 17-05-24: bugged syst for events with pT<10GeV. Leave out for now
  //m_histoSysts.insert({ "SysMUON_TTVA_SYS" , noSmoothConfig});
  //m_histoSysts.insert({ "SysMUON_TTVA_STAT" , noSmoothConfig});

  m_histoSysts.insert({ "SysMUON_SCALE" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_MS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_ID" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RHO", smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RESBIAS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_STAT_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_SYST_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_SYS_LOWPT" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysMUON_EFF_TrigSystUncertainty" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysMUON_EFF_TrigStatUncertainty" , smoothConfig}); // sys_V28


  // new v28, tau systematic uncertainty in 0 lepton
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_DETECTOR" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_INSITU" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL" , smoothConfig}); // sys_V28

  m_histoSysts.insert({ "SysJET_JvtEfficiency" , smoothConfig});
  m_histoSysts.insert({ "SysPRW_DATASF" , smoothConfig});

  // 21NP Scheme
  std::vector<std::string> jetSysts {"Flavor_Response", "SingleParticle_HighPt", "PunchThrough_MC15",
    "BJES_Response", "Pileup_RhoTopology", "Pileup_PtTerm", "Pileup_OffsetNPV", "Pileup_OffsetMu",
    "EtaIntercalibration_NonClosure", "EtaIntercalibration_TotalStat", "EtaIntercalibration_Modelling",
    "EffectiveNP_1", "EffectiveNP_2", "EffectiveNP_3", "EffectiveNP_4", "EffectiveNP_5",
    "EffectiveNP_6", "EffectiveNP_7", "EffectiveNP_8restTerm"
  };

  for(auto sysname: jetSysts) {
    m_histoSysts.insert({ "SysJET_21NP_JET_"+sysname , smoothConfig});
  }

  // Decorrelated:
  // - Correlate W+jets and Z+jets in all channels (call it
  // Flavor_Composition_Vjets)
  // - Correlate top in the 0- and 1-lepton channels (call it
  // Flavor_Composition_top)
  // - Have an uncorrelated uncertainty for top in the 2-lepton channel (call
  // it Flavor_Composition_top_L2)
  // - Single-top (all channels) go with ttbar in 0/1lep
  // - Higgs and Diboson stay together on their own.
  SysConfig JET_Flav_Comp_Config = smoothConfig;
  JET_Flav_Comp_Config.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                 if( s.name() == "ttbar" && pset[P::nLep] == 2) return "_ttbar_L2";
                                 else if( s.hasKW("Top") ) return "_Top";
                                 else if( s.hasKW("Higgs") || s.hasKW("Diboson") ) return "_VV";
                                 else if( s.hasKW("Wjets") || s.hasKW("Zjets")) return "_Vjets";
                                 else return "";
                                 });
  m_histoSysts.insert( { "SysJET_21NP_JET_Flavor_Composition", JET_Flav_Comp_Config} );

  // B-tag
  std::vector<std::string> btagSysts { "extrapolation_from_charm", "extrapolation",
    "Eigen_Light_0", "Eigen_Light_1", "Eigen_Light_2", "Eigen_Light_3", "Eigen_Light_4",
    "Eigen_Light_5", "Eigen_Light_6", "Eigen_Light_7", "Eigen_Light_8", "Eigen_Light_9",
    "Eigen_Light_10", "Eigen_Light_11", "Eigen_Light_12", "Eigen_Light_13",
    "Eigen_C_0", "Eigen_C_1", "Eigen_C_2", "Eigen_C_3",
    "Eigen_B_0", "Eigen_B_1", "Eigen_B_2", "Eigen_B_3", "Eigen_B_4", "Eigen_B_5"
  };

  for(auto sysname: btagSysts) {
    m_histoSysts.insert({ "SysFT_EFF_"+sysname , noSmoothConfig});
  }

  // Background modelling
  // 1D
  m_histoSysts.insert( {"SysTTbarPTV", SysConfig{T::shape, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});
  m_histoSysts.insert( {"SysTTbarMBB", SysConfig{T::shapeonly, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});
  // 2D
  //m_histoSysts.insert( {"SysTTbarPTVMBB", SysConfig{T::shape, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});
  //m_histoSysts.insert( {"SysTTbarPTVMBB", SysConfig{T::shape, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});//sys_V28, 1lep WhfCR, cross checked
  m_histoSysts.insert({ "SysWPtV" , noSmoothConfig});//Test
  m_histoSysts.insert({ "SysZPtV" , noSmoothConfig});//Test
  m_histoSysts.insert({ "SysWMbb" , noSmoothShapeOnlyConfig});//Test
  m_histoSysts.insert({ "SysZMbb" , noSmoothShapeOnlyConfig});//Test
  //
  // 2D
  //m_histoSysts.insert( {"SysWPtVMbb",  noSmoothConfig});
  //m_histoSysts.insert( {"SysZPtVMbb",  noSmoothConfig});

  //m_histoSysts.insert({ "SysVVJetScalePtST1" , noSmoothConfig});
  m_histoSysts.insert({ "SysVVMbbME" , noSmoothShapeOnlyConfig});
  m_histoSysts.insert({ "SysVVPTVME" , noSmoothConfig});
  m_histoSysts.insert({ "SysVVPTVPSUE" , noSmoothConfig}); //sys_V28
  m_histoSysts.insert({ "SysVVMbbVPSUE" , noSmoothShapeOnlyConfig}); //sys_V28



  m_histoSysts.insert({ "SysVHQCDscalePTV" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHQCDscaleMbb" , noSmoothShapeOnlyConfig});
  m_histoSysts.insert({ "SysVHPDFPTV" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHQCDscalePTV_ggZH" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHQCDscaleMbb_ggZH" , noSmoothShapeOnlyConfig});
  m_histoSysts.insert({ "SysVHPDFPTV_ggZH" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHUEPSPTV" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHUEPSMbb" , noSmoothShapeOnlyConfig});
  m_histoSysts.insert({ "SysVHNLOEWK" , noSmoothConfig});

  m_histoSysts.insert({ "SysStoptPTV" , noSmoothConfig});
  m_histoSysts.insert({ "SysStoptMBB" , noSmoothShapeOnlyConfig});
  m_histoSysts.insert({ "SysStopWtPTV" , noSmoothConfig});
  m_histoSysts.insert({ "SysStopWtMBB" , noSmoothShapeOnlyConfig});

  m_histoSysts.insert({ "SysMJ_El_METstr" , smoothAndSymConfig});//PF
  m_histoSysts.insert({ "SysMJ_Mu_METstr" , smoothAndSymConfig});//PF
  m_histoSysts.insert({ "SysMJ_El_EWK"    , smoothAndSymConfig});
  //m_histoSysts.insert({ "SysMJ_El_EWK" , smoothAndSymConfig});//SysConfig{ T::shapeonly, S::smoothRebinMonotonic }});
  m_histoSysts.insert({ "SysMJ_El_flavor" , smoothAndSymConfig});//PF

  // multijet
  // normalization systematic uncertainty added separately above, here shapeonly

  m_histoSysts.insert({"SysMJTrigger", SysConfig{ T::shapeonly, S::noSmooth, Sym::symmetriseAverage, {"multijetEl","multijetMu"},{}, {}, {}, {"multijetEl", "multijetMu"} } }); //decorrelated between different components, sys_V28
  m_histoSysts.insert({"SysMJReduced", SysConfig{ T::shapeonly, S::noSmooth, Sym::symmetriseAverage, {"multijetEl","multijetMu"},{}, {}, {}, {"multijetEl", "multijetMu"} } }); //decorrelated between different components, sys_V28
  m_histoSysts.insert({"SysMJWOCorrection", SysConfig{ T::shapeonly, S::noSmooth, Sym::symmetriseAverage, "multijetEl",{}, {}, {}, {"multijetEl", "multijetMu"} } }); //decorrelated between different components, sys_V28
  m_histoSysts.insert({"SysMJ2Tag", SysConfig{ T::shapeonly, S::noSmooth, Sym::symmetriseAverage, {"multijetEl","multijetMu"},{}, {}, {}, {"multijetEl", "multijetMu"} } }); //decorrelated between different components, sys_V28
  m_histoSysts.insert({"SysMJSFsCR", SysConfig{ T::shapeonly, VHbbSmoothing, Sym::symmetriseAverage, {"multijetEl","multijetMu"},{}, {}, {}, {"multijetEl", "multijetMu"} } });

  /////////////////////////////////////////////////////////////////////////////////////////
  //
  //                          Tweaks, if needed
  //
  /////////////////////////////////////////////////////////////////////////////////////////

  // later can add some switches, e.g looping through m_histoSysts and
  // putting to T::skip all the JES NP

}

void SystematicListsBuilder_VHbbRun2::AddPOIsForSTXS() {
  int  FitSTXSScheme = m_config.getValue("FitSTXSScheme", 1);
  switch( FitSTXSScheme ){
    case 1: // merging all STXS signals into an inclusive signal, should be same as VHbb
      addPOI("SigXsecOverSMSTXS", {"STXS"}, 1, -40, 40); // 30
      normSys("SysSTXSRestNorm", 0.50, {"UNKNOWN"}); // 5 + 1
      break;
    case 2:
      //addPOI("SigXsecOverSMSTXSQQZHFWDH",           {"STXSQQZHFWDH"},           1, -40, 40); 
      addPOI("SigXsecOverSMSTXSQQZH0x150PTV",       {"STXSQQZH0x150PTV"},       1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQZH150x250PTV0J",   {"STXSQQZH150x250PTV0J"},   1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQZH150x250PTVGE1J", {"STXSQQZH150x250PTVGE1J"}, 1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQZHGT250PTV",       {"STXSQQZHGT250PTV"},       1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQWH0x150PTV",       {"STXSQQWH0x150PTV"},       1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQWHGT250PTV",       {"STXSQQWHGT250PTV"},       1, -40, 40);
      //addPOI("SigXsecOverSMSTXSGGZHFWDH",           {"STXSGGZHFWDH"},           1, -40, 40); 
      addPOI("SigXsecOverSMSTXSGGZH0x150PTV",       {"STXSGGZH0x150PTV"},       1, -40, 40);
      addPOI("SigXsecOverSMSTXSGGZHGT150PTV0J",     {"STXSGGZHGT150PTV0J"},     1, -40, 40);
      addPOI("SigXsecOverSMSTXSGGZHGT150PTVGE1J",   {"STXSGGZHGT150PTVGE1J"},   1, -40, 40);

      //addPOI("SigXsecOverSMSTXSQQWHFWDH",           {"QQ2HLNUxFWDH"},   1, -40, 40); 
      addPOI("SigXsecOverSMSTXSQQWH150x250PTV0J",   {"QQ2HLNUxPTVx150x250x0J"},   1, -40, 40);
      addPOI("SigXsecOverSMSTXSQQWH150x250PTVGE1J", {"QQ2HLNUxPTVx150x250xGE1J"},   1, -40, 40);

      normSys("SysSTXSRestNorm",     0.50,          {"VHSTXSRest"});         // 5 FWD + 1 UNKNOWN

      break;
  }
}
