# Changelog

## Unreleased


## [11-00-00] - 2018-03-20
First tag with the new package structure
### Added
  - A "Generic" AnalysisType, can be used by analyses that don't need analysis-specific bits
  in common code

### Removed
  - All analysis-specific scripts, configs and classes (analysis-specific bits inside common
  code still exist)
  - HistFactorySchema.dtd, now fetched from $ROOTSYS instead
  - unused compile.C
  - util/, as executables are to be implemented by each analysis

### Changed
Needed changes for the new structure:
  - setup script, CMakeLists
  - scripts for submission on lxbatch
  - other scripts
  - headers are now in WSMaker/
  - Construction of the Engine, which takes the Analysis object as a parameter


## [10-04-00] - 2018-03-19
Last tag with the monolitic package structure
### Fixed
  - Minor bugfix on how the unsmoothed copies of histograms where merged (#28)
  - Remove hardcoded values of parameters in newGetMuHat. Would have affected analyses
  in their Asimov expected impact if they had parameters: `ATLAS_norm_ttbar`
  `ATLAS_norm_ttbar_J2_L2_Y` `ATLAS_norm_ttbar_J3_L2_Y` `ATLAS_norm_Wbb_J2` `ATLAS_norm_Wbb_J3`
  `ATLAS_norm_Zbb_J2` `ATLAS_norm_Zbb_J3`

### Added
  - Possibility to do pruning after merging of samples. Config switch `PruneAferMerge`
  - Possibility to do smoothing after merging of samples. Config switch `SmoothAfterMerge`
  - New functions findAll and hasMatching in CategoryIndex. Can be used to achieve
  same functionality as RegionTracker, without the need for that class. RegionTracker
  can be removed in the future.
  - Proper option `draw_error_band_on_b_only` in plottingConfig to draw the error band
  in postfit plots on S+B (false) or B only (true). Works for both regular plots and
  "sum" plots.

### Removed
  - NPRanking/ directory, which contains old, not used code

### Changed
  - Move default colors for correlation matrices. Use VHcc color palette.
  - Huge overhaul of smoothing and symmetrizing of systematics. See #7. Each smoothing alg
  is an item in an enum, so the user can select a different algo per systematic. Also
  possibility to choose the algo depending on the distribution (actually region and sample)
  it acts on (VHbb use-case). Averaging can be selected by a specific bit in SysConfig, so
  independently for each syst. Remove hardcoded symmetrization of some systs (JER), they
  should now be marked individually by the user with the symmetrization bit.
  Overall much cleaner configuration, transparent for the user and customizable.

### Changes for analyses
  - VHbb uses the new functions instead of RegionTracker. Better for readability.
  - VHbb: add code for STXS fits
  - VHbb: add mini-tutorial


## [10-03-00] - 2018-02-23
### Added
  - new API for SysConfig. Allows hopefully more readable code in systematiclistbuilders
  - syntaxic sugar for PropertiesSet, which allows more readable code in systematiclistbuilders

### Changed
  - Technical improvements. Pure code cleaning, no impact on any workspace.
  - Use variants in PropertiesSet. Simplifies the struct, with only 1 map instead of 2.

### Removed
  - Classes for the old, unmaintained VHRes and HVT analyses

### Changes for analyses
  - the VHbb systematiclistbuilder uses the new APIs for better readability


## [10-02-00] - 2018-02-20
### Fixed
  - Smoothing algorithm now works as intended. 4-y.o bug fixed.
  - bad interaction of renamed systs and SkipUnknownSysts
  - Make CoreRegions works again in Run2

### Removed
  - Lots of inputConfigs files for old analyses

### Added
  - Changelog.md

### Changes for analyses
  - VHbb: cleanup of systs config. Config for first tests with rel21/mc16ac


## [10-01-00] - 2018-02-07
### Changed
  - ROOT 6.12 version
  - build now based on CMake

### Removed
  - Makefile

### Changes for analyses
  - mono-V


## [WSMaker-10-00-00] - 2017-11-30
### First tag on git
  - Make TransformTool a git submodule of the project

### Changed
  - Small HowTo update
  - Commented out inconsistent additional column in tex table in newGetMuHat

### Changes for analyses
  - mono-V
  - HHbbtautau
  - SM Htautau
  - new Vprime and VprimeHVT analyses


## [WSMaker-07-00-00] - 2017-10-02
Corresponds to VHbb data15+16 analysis
### Last tag on SVN.
### Beginning of Changelog
