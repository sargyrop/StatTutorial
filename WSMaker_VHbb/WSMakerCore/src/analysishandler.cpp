#include "analysishandler.hpp"

#include <iostream>

#include "analysis.hpp"
#include "configuration.hpp"

std::unique_ptr<Analysis> AnalysisHandler::m_analysis(nullptr);

AnalysisHandler::~AnalysisHandler() {}

AnalysisHandler::AnalysisHandler(const Configuration& /*conf*/)
{

  // emptied

}

AnalysisHandler::AnalysisHandler(std::unique_ptr<Analysis>&& ana) {
  m_analysis.reset(ana.release());
}
