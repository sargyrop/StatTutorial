#include "properties.hpp"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <utility>

#include <TString.h>

#include "containerhelpers.hpp"

const std::unordered_map<Property, TString> Properties::names({
  {Property::year, "Y"}, {Property::nLep, "L"}, {Property::nJet, "J"}, {Property::nFatJet, "Fat"}, {Property::nTag, "T"},
    {Property::tagType, "TType"}, {Property::bin, "B"}, {Property::lepFlav, "Flv"}, {Property::lepSign, "Sgn"},
    {Property::type, "isMVA"}, {Property::dist, "dist"}, {Property::spec, "Spc"}, {Property::highVhf, "Vhf"},
    {Property::binMin, "BMin"}, {Property::binMax, "BMax"}, {Property::descr, "D"}, {Property::incTag, "incTag"},
    {Property::incJet, "incJet"}, {Property::incFat, "incFat"}, {Property::incAddTag, "incAddTag"},
    {Property::nAddTag, "nAddTag"}, {Property::nPh, "Ph"}, {Property::LTT, "LTT"}
});

const std::unordered_map<TString, Property> Properties::props_from_names(Utils::reverseMap<Property, TString>(Properties::names));

bool PropertiesSet::hasProperty(const Property p) const {
  return properties.count(p);
}

int PropertiesSet::getIntProp(const Property p) const {
  const auto& pos = properties.find(p);
  if(pos != properties.end() && mpark::holds_alternative<int>(pos->second)) {
    return mpark::get<int>(pos->second);
  }
  return -1;
}

TString PropertiesSet::getStringProp(const Property p) const {
  const auto& pos = properties.find(p);
  if(pos != properties.end() && mpark::holds_alternative<TString>(pos->second)) {
    return mpark::get<TString>(pos->second);
  }
  return "";
}

int PropertiesSet::requestIntProp(const Property p) const {
  return mpark::get<int>(properties.at(p));
}

TString PropertiesSet::requestStringProp(const Property p) const {
  return mpark::get<TString>(properties.at(p));
}

void PropertiesSet::setIntProp(Property p, int i) {
  properties[p] = i;
}

void PropertiesSet::setStringProp(Property p, const TString& s) {
  properties[p] = s;
}

void PropertiesSet::setProp(Property p, const prop_value v) {
  properties[p] = v;
}

bool PropertiesSet::match(const PropertiesSet& pset) const {
  // check if all defined props are present and have same value
  bool res = std::all_of(std::begin(pset.properties), std::end(pset.properties),
                         [this](const std::pair<Property, prop_value>& p) {
                           return this->hasProperty(p.first) &&
                             (this->properties).at(p.first) == p.second;
                         });

  return res;
}

void PropertiesSet::merge(const PropertiesSet& other) {
  for(const auto& p : other.properties)
    properties[p.first] = p.second;
}

void PropertiesSet::print() const {
  std::cout << "\nPrinting properties:\n" << std::endl;
  for(const auto& p : properties) {
    std::cout << "    - " << Properties::names.at(p.first) << " = ";
    mpark::visit([](auto&& arg){std::cout << arg << '\n';}, p.second);
  }
  std::cout << std::endl;
}

struct PropPrinter {
  std::stringstream& m_ss;
  PropPrinter(std::stringstream& ss) : m_ss(ss) {}
  void operator() (int i) { m_ss << i; }
  void operator() (TString s) { s.ReplaceAll("_", ""); m_ss << s; }
};

TString PropertiesSet::getPropertyTag(const Property p) const {
  const auto& pos = properties.find(p);
  if(pos == properties.end()) {
    std::cout << "ERROR:   PropertiesSet::printProperty\n";
    std::cout << "Property " << Properties::names.at(p) << " does not exist here" << std::endl;
    throw;
  }
  std::stringstream ss;
  ss << "_" << Properties::names.at(pos->first);
  mpark::visit(PropPrinter(ss), pos->second);

  return ss.str().c_str();
}

TString PropertiesSet::getPropertiesTag() const {
  std::stringstream ss;
  for(const auto& p : properties) {
    ss << "_" << Properties::names.at(p.first);
    mpark::visit(PropPrinter(ss), p.second);
  }
  return ss.str().c_str();
}

PropertiesSet PropertiesSet::copyExcept(const Property p, prop_value i) const {
  PropertiesSet pset(*this);
  pset[p] = i;
  return pset;
}
