#lxplus only

export WORKDIR=$PWD
export BUILDDIR=${ANALYSISDIR}/build

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "root 6.12.04-x86_64-slc6-gcc62-opt"
lsetup "sft releases/doxygen/1.8.11-68a7c"
lsetup cmake

# increase stack size - needed for large workspaces
ulimit -S -s 15360

export LD_LIBRARY_PATH=${ROOTSYS}/lib:${LD_LIBRARY_PATH}
export PATH=${ROOTSYS}/bin:${PATH}
# export PATH=/afs/cern.ch/sw/lcg/external/doxygen/1.8.2/x86_64-slc6-gcc48-opt/bin:$PATH
export PYTHONPATH=${ROOTSYS}/lib:${PYTHONDIR}/lib:${PYTHONPATH}

# include local lib in library path
export LD_LIBRARY_PATH=${BUILDDIR}:${BUILDDIR}/WSMakerCore:${WORKDIR}:${LD_LIBRARY_PATH}
# include local bin in binary path
export PATH=${BUILDDIR}:${WORKDIR}/scripts:${ANALYSISDIR}/scripts:${PATH}

export PYTHONPATH=${WORKDIR}/WSMakerCore/scripts:${ANALYSISDIR}/scripts:${PYTHONPATH}

#mkdir -vp ${BUILDDIR}
#mkdir -vp ${ANALYSISDIR}/configs

echo ""
echo "What to do next:"
echo "Now you must compile the package"
echo "$ cd build && cmake .."
echo "$ make -j5"
