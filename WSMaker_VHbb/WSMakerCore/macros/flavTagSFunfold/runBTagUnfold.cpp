
void runBTagUnfold(string WSName, bool conditionnal) {

  //gROOT->ProcessLine(".L $WORKDIR/macros/AtlasStyle.C");
  //SetAtlasStyle();
  //gStyle -> SetPalette(1);

  string CDIdir = "/afs/cern.ch/user/d/dbuesche/public/CDIunfoldRebin";
  gSystem->SetIncludePath(("-I" + CDIdir).c_str());
  gROOT->ProcessLine((".L " + CDIdir + "/scripts/libCalibrationDataInterface.so").c_str());
  gROOT->ProcessLine(".L $WORKDIR/macros/flavTagSFunfold/flavTagSFunfold.cpp+");

  string envFile = "$WORKDIR/macros/flavTagSFunfold/BTagCalibration2012.env";
  string calibFile = "$WORKDIR/macros/flavTagSFunfold/Continuous_Higgs_2014_MV1c_EM_V17_AddVHbbEff.root";
  system(("ln -sv " + envFile + " ./").c_str());
  system(("ln -sv " + calibFile + " ./").c_str());
  string inFile = "FitCrossChecks_" + WSName + "_combined/FitCrossChecks.root";
  string plotFolder = "plots/" + WSName + "/btag/";
  system(("mkdir -pv " + plotFolder).c_str());
  
  flavTagSFunfold(inFile, plotFolder, "B", 2012, conditionnal);
  flavTagSFunfold(inFile, plotFolder, "C", 2012, conditionnal);
  flavTagSFunfold(inFile, plotFolder, "L", 2012, conditionnal);

  //flavTagSFunfold(inFile, plotFolder, "B", 2011);
  //flavTagSFunfold(inFile, plotFolder, "C", 2011);
  
  //gSystem -> Exit(0);
}
