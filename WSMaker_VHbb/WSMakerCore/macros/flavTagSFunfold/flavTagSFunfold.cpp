
#include <fstream>
#include <iostream>
#include <iomanip>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLine.h>
#include <TPad.h>
#include <TDirectory.h>
#include <TGraphAsymmErrors.h>
#include <TVectorD.h>
#include <RooFitResult.h>
#include <RooArgList.h>
#include <RooRealVar.h>

#include "CalibrationDataInterface/CalibrationDataInterfaceBase.h"
#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"
#include "CalibrationDataInterface/CalibrationDataContainer.h"
#include "CalibrationDataInterface/CalibrationDataEigenVariations.h"

#include "TMatrixT.h"
#include "TVectorT.h"
#include "TMatrixDSym.h"

using namespace std;

//string graphicsExt = ".png";
string graphicsExt = ".eps";

TH1* reduceSFhisto(TH1* histoSFsOrig, TMatrixT<double> matrixVariationJacobian) {
  int cols = matrixVariationJacobian.GetNcols();
  int rows = matrixVariationJacobian.GetNrows();
  cout << "Reducing SF hist..." << endl;
  cout << "Jacobian:   cols = " << cols << "   rows = " << rows << endl;

  TH1* histoSFs = new TH1D("histoSFs", "histoSFs", rows, 0, rows);
  int ibin = 0;
  for (int i = 0; i < cols; i++) {
    if (matrixVariationJacobian[ibin][i] == 0) {
      continue;
    }
    ibin++;
    double val = histoSFsOrig -> GetBinContent(i);
    double err = histoSFsOrig -> GetBinError(i);
    histoSFs -> SetBinContent(ibin, val);
    histoSFs -> SetBinError(ibin, err);
    if (ibin >= rows) break;
  }
  return histoSFs;
}

TH1* expandSFhisto(TH1* histoSFsOrig, TMatrixT<double> matrixVariationJacobian, TVectorT<double> sfShiftpT, TMatrixT<double> covMatrixPt) {
  int cols = matrixVariationJacobian.GetNcols();
  int rows = matrixVariationJacobian.GetNrows();
  cout << "Expanding SF hist..." << endl;
  cout << "Jacobian:   cols = " << cols << "   rows = " << rows << endl;

  TH1* histoSFs = (TH1*) histoSFsOrig -> Clone();
  int ibin = 0;
  for (int i = 0; i < cols; i++) {
    if (matrixVariationJacobian[ibin][i] == 0) {
      continue;
    }
    double nominal = histoSFsOrig -> GetBinContent(i);
    double shifted = sfShiftpT[ibin] + nominal;
    double err = sqrt(covMatrixPt[ibin][ibin]);
    histoSFs -> SetBinContent(i, shifted);
    histoSFs -> SetBinError(i, err);
    ibin++;
    if (ibin >= rows) break;
  }
  return histoSFs;
}

double getGlobalExtremum(TH1* histo, bool maximum) {
  int nbins = histo -> GetNbinsX() + 2;
  nbins *= histo -> GetNbinsY() + 2;
  nbins *= histo -> GetNbinsZ() + 2;
  double extremum = 0;
  int sign = 2 * maximum - 1;
  for (int i = 0; i <= nbins+1; i++) {
    double val = histo -> GetBinContent(i);
    val += sign * histo -> GetBinError(i);
    if ((sign * val > sign * extremum && val) || !extremum)
      extremum = val;
  }
  return extremum;
}

void drawSFslice(TH1* slicePre, TH1* slicePost, string plotPrefix, TString sliceName, double min = 0, double max = 0) {
  TCanvas* canvas = new TCanvas();
  TLegend* leg = new TLegend(0.75, 0.75, 0.9, 0.9);

  if (min || max) {
    double delta = max - min;
    double minY = min - 0.05 * delta;
    double maxY = max + 0.05 * delta;
    slicePre -> GetYaxis() -> SetRangeUser(minY, maxY);
  }

  slicePre -> SetMarkerSize(0);
  slicePre -> SetMarkerColor(kAzure + 1);
  slicePre -> SetFillColor(kAzure + 1);
  slicePre -> GetYaxis() -> SetTitle("scale-factor");
  slicePre -> Draw("E2");
  slicePost -> Draw("same");

  leg -> SetFillColor(0);
  leg -> AddEntry((TObject*) slicePre, "pre-fit", "f");
  leg -> AddEntry((TObject*) slicePost, "post-fit", "l");
  leg -> Draw();

  string filename = plotPrefix + "_sf_" + sliceName.Data();
  canvas -> Print((filename + graphicsExt).c_str());
  delete leg;
  delete canvas;
}

void drawSFslices(TH1* histoSFsOrig, TH1* histoSFsPost, string plotPrefix, int dim, int axis) {

  // get global extrema
  double min = getGlobalExtremum(histoSFsOrig, false);
  double max = getGlobalExtremum(histoSFsOrig, true);
  double min2 = getGlobalExtremum(histoSFsPost, false);
  double max2 = getGlobalExtremum(histoSFsPost, true);
  if (min2 < min) min = min2;
  if (max2 > max) max = max2;

  // slice and draw
  int nx = histoSFsOrig -> GetNbinsX();
  int ny = histoSFsOrig -> GetNbinsY();
  int nz = histoSFsOrig -> GetNbinsZ();
  int nbinA = 0;
  int nbinB = 0;
  switch (axis) {
    case 0: nbinA = ny; nbinB = nz; break;
    case 1: nbinA = nx; nbinB = nz; break;
    case 2: nbinA = nx; nbinB = ny; break;
  }
  for (int i = 1; i <= nbinA; i++) {
    for (int j = 1; j <= nbinB; j++) {
      TH1* slicePre = 0;
      TH1* slicePost = 0;
      TString sliceName = "";
      if (axis == 0) {
        slicePre = ((TH3*) histoSFsOrig) -> ProjectionX("sfpre", i, i, j, j, "");
        slicePost = ((TH3*) histoSFsPost) -> ProjectionX("sfpost", i, i, j, j, "");
        sliceName = histoSFsOrig -> GetYaxis() -> GetTitle() + TString::Format("%i", i);
        if (dim == 3)
          sliceName += "_" + (histoSFsOrig -> GetZaxis() -> GetTitle() + TString::Format("%i", j));
      } else if (axis == 1) {
        slicePre = ((TH3*) histoSFsOrig) -> ProjectionY("sfpre", i, i, j, j, "");
        slicePost = ((TH3*) histoSFsPost) -> ProjectionY("sfpost", i, i, j, j, "");
        sliceName = histoSFsOrig -> GetXaxis() -> GetTitle() + TString::Format("%i", i);
        if (dim == 3)
          sliceName += "_" + (histoSFsOrig -> GetZaxis() -> GetTitle() + TString::Format("%i", j));
      } else {
        slicePre = ((TH3*) histoSFsOrig) -> ProjectionZ("sfpre", i, i, j, j, "");
        slicePost = ((TH3*) histoSFsPost) -> ProjectionZ("sfpost", i, i, j, j, "");
        sliceName = histoSFsOrig -> GetXaxis() -> GetTitle() + TString::Format("%i", i);
        if (dim == 3)
          sliceName += "_" + (histoSFsOrig -> GetYaxis() -> GetTitle() + TString::Format("%i", j));
      }
      drawSFslice(slicePre, slicePost, plotPrefix, sliceName, min, max);
      delete slicePre;
      delete slicePost;
    }
  }
}

void drawSFslices(TH1* histoSFsOrig, TH1* histoSFsPost, string plotPrefix) {
  int dim = histoSFsOrig -> GetDimension();
  for (int i = 0; i < dim; i++) {
    drawSFslices(histoSFsOrig, histoSFsPost, plotPrefix, dim, i);
  }
}

TH2* getCorrHistoFromCovMatrix(TMatrixT<double> covMatrixPt) {

  int nSF = covMatrixPt.GetNrows();
  TH2* hist = new TH2D("corr", "corr", nSF, 0, nSF, nSF, 0, nSF);
  hist -> GetZaxis() -> SetRangeUser(-1, 1);
  for (int i = 0; i < nSF; i++) {
    for (int j = 0; j < nSF; j++) {
      float cov = covMatrixPt[i][j];
      float sigmai = sqrt(covMatrixPt[i][i]);
      float sigmaj = sqrt(covMatrixPt[j][j]);
      hist -> SetBinContent(i + 1, j + 1, cov / (sigmai * sigmaj));
      //if (cov < 0)
      //  cout << "WARNING: found corr < 0 (not taken into account in the plotting)!" << endl;
    }
  }
  return hist;
}

double getIntegratedEfficiency(TH1* effiHisto, double pTdo, double pTup) {
  
  int xBinLo = effiHisto -> GetXaxis() -> FindBin(-2.5);
  int xBinUp = effiHisto -> GetXaxis() -> FindBin(2.5-0.0001);
  int yBinLo = effiHisto -> GetYaxis() -> FindBin(pTdo);
  int yBinUp = effiHisto -> GetYaxis() -> FindBin(pTup - 0.1);
  
  // get weighted mean
  double numerator = 0; // sum (weight*val)
  double denum = 0; // sum (weight)
  
  for (int i = xBinLo; i <= xBinUp; i++) {
    for (int j = yBinLo; j <= yBinUp; j++) {
      double val = effiHisto -> GetBinContent(i, j);
      double sigma = effiHisto -> GetBinError(i, j);
      if (sigma <= 0)
        continue;
      double weight = 1 / (sigma * sigma);
      numerator += weight * val;
      denum += weight;
    }
  }

  double mean = numerator / denum;
  double sigmaMean = sqrt(1 / denum);
  cout << "mean = " << mean << " +- " << sigmaMean << endl;
  
  return mean;
}

void flavTagSFunfold(string infile, string plotFolder, string label, int year, bool conditionnal) {

  string labelCDI = label;
  if (label == "L") labelCDI = "Light";
  
  // initialize CDI
  cout << "SFunfold::INFO: Initialize CDI..." << endl;

  string author;
  string OP;
  
  string plotPrefix = plotFolder + TString::Format("%i", year).Data() + "_" + label;
  
  Analysis::CalibrationDataInterfaceROOT* m_flavSF_calib = 0;

  if (year == 2011) {
    m_flavSF_calib = new Analysis::CalibrationDataInterfaceROOT("MV1", "BTagCalibration2011.env");
    author = "AntiKt4Topo";
    OP = "0_601713"; // 2011 70%
  } else {
    m_flavSF_calib = new Analysis::CalibrationDataInterfaceROOT("MV1c", "BTagCalibration2012.env");
    author = "AntiKt4TopoEMJVF0_5";
    //OP = "0_8119"; // 2012 70%
    OP = "continuous";
  }
  
  // get eigenvalues + vectors
  cout << "SFunfold::INFO: Getting eigenvalues + vectors from CDI..." << endl;

  // This nice method doesn't work with CDI 00-03-06 anymore...
  //  string contName = m_flavSF_calib -> fullName(author, OP, labelCDI, isSF);
  //  cout << "SFunfold::Info: CDI container name: " << contName << endl;
  //  Analysis::CalibrationDataContainer* calibCont = m_flavSF_calib -> retrieveContainer(contName, !isSF);
  //  cout << "SFunfold::Info: CDI container address: " << calibCont << endl;
  //  const Analysis::CalibrationDataEigenVariations* calibEVcont = m_flavSF_calib -> m_eigenVariationsMap[calibCont];
  
  // ugly code for CDI 00-03-06
  // have to call once
  cout << "SFunfold::INFO: Calling CDI once to initialize containers..." << endl;
  Analysis::CalibrationDataVariables m_flavSF_ajet;
  m_flavSF_ajet.jetAuthor = author;
  m_flavSF_ajet.jetPt = 100e3;
  m_flavSF_ajet.jetEta = 0;
  m_flavSF_ajet.jetTagWeight = 0.5;
  //Analysis::CalibResult res = 
  m_flavSF_calib -> getScaleFactor(m_flavSF_ajet, labelCDI, "continuous", Analysis::SFEigen, 0);
  
  if (m_flavSF_calib -> m_eigenVariationsMap.size() != 1) {
    cout << "SFunfold::ERROR: Number of EV containers is not 1! Don't know which one to use." << endl;
    exit(-1);
  }

  //  for(map<const Analysis::CalibrationDataContainer*,const Analysis::CalibrationDataEigenVariations*>::iterator iter = m_flavSF_calib -> m_eigenVariationsMap.begin();
  //          iter != m_flavSF_calib -> m_eigenVariationsMap.end(); iter++) {
  //    cout << "bb " << iter -> first << " " << iter -> second << " " << iter -> first -> GetName() << endl;
  //  }
  
  const Analysis::CalibrationDataEigenVariations* calibEVcont = m_flavSF_calib -> m_eigenVariationsMap.begin() -> second;
  TMatrixT<double> matrixVariationJacobian1(*calibEVcont -> m_matrixVariationJacobian); // not needed, actually...
//  TVectorT<double> eigenValues(*calibEVcont -> m_eigenValues);
//  TMatrixT<double> eigenVectorsOriginal(*calibEVcont -> m_eigenVectorsOriginal);

  std::vector<std::pair<TH1*, TH1*> > eigenVectorHistos(calibEVcont -> m_eigen);
  TMatrixT<double> matrixVariationJacobian(matrixVariationJacobian1.GetNcols(), matrixVariationJacobian1.GetNcols());
  for (int i = 0; i < matrixVariationJacobian.GetNcols(); i++) matrixVariationJacobian[i][i] = 1;

    
  //TMatrixT<double> matrixVariationsFromHistos(eigenVectorHistos.size(), eigenVectorHistos.size());
  TMatrixT<double> matrixVariationsFromHistos(matrixVariationJacobian1.GetNcols(), matrixVariationJacobian1.GetNcols());
  for (unsigned int i = 0; i < eigenVectorHistos.size(); i++) {
    TH1* histoUp = reduceSFhisto(eigenVectorHistos.at(i).first, matrixVariationJacobian);
    TH1* histoDo = reduceSFhisto(eigenVectorHistos.at(i).second, matrixVariationJacobian);
      for ( int j = 1; j <= histoUp->GetNbinsX(); j++) {
        double up = histoUp -> GetBinContent(j);
        double down = histoDo -> GetBinContent(j);
        double unc = fabs(up - down) / 2;
        matrixVariationsFromHistos(j-1, i + matrixVariationJacobian1.GetNcols() - eigenVectorHistos.size()) = unc;
        //cout << unc << endl;
      }
  }

//  cout << "Jacobian from CDI:" << endl;
//  matrixVariationJacobian.Print();
//  cout << "Eigenvalues from CDI:" << endl;
//  eigenValues.Print();
//  cout << "Eigenvectors from CDI:" << endl;
//  eigenVectorsOriginal.Print();

//  cout << "Matrix variations from Histos:" << endl;
//  matrixVariationsFromHistos.Print();

  // ======================= calculate scale-factors =======================

  // get nominal scale-factors (errors are different from EV method)
  
  
  //TH1* histoSFsOrig = (TH1*) (dynamic_cast<TH1*>(calibEVcont -> m_cnt->GetValue("result"))) -> Clone();
  TH1* histoSFsOrig  = (TH1*) calibEVcont -> m_nominalSF -> Clone();
  TH1* histoSFs = reduceSFhisto(histoSFsOrig, matrixVariationJacobian);

  int nSF = histoSFs -> GetNbinsX();
  
  cout << "Nominal SFs in pT bins (errors are different from EV method): " << endl;
  for (int i = 0; i < nSF; i++) {
    cout << setw(20) << histoSFs -> GetBinContent(i + 1);
    cout << " +- ";
    cout << setw(20) << histoSFs -> GetBinError(i + 1);
    cout << endl;
  }
  
  // get post-fit alphas from fit results
  
  cout << "Reading fit result from: " << infile << endl;

  TFile* fitFile = new TFile(infile.c_str(), "READ");
  
  if (fitFile->IsZombie()) {
    cout << "ERROR: fit result file not found!" << endl;
    return;
  }
  
  bool good;
  if (!conditionnal) {
    good = fitFile -> cd("PlotsAfterGlobalFit/unconditionnal");
  } else {
    good = fitFile -> cd("PlotsAfterGlobalFit/conditionnal_MuIsEqualTo_0");
  }
  if (!good) {
    cout << "ERROR: fit result folder not found!" << endl;
    return;
  }
  RooFitResult* fitResult = (RooFitResult*) gDirectory -> Get("fitResult");
  const RooArgList* pars = &fitResult -> floatParsFinal();
  TCanvas* corrMatrixFullCanvas = 0;
  TH2* corrMatrixFull = 0;
  if (!conditionnal) {
    corrMatrixFullCanvas = (TCanvas*) gDirectory -> Get("can_CorrMatrix_GlobalFit_unconditionnal_mu1");
    corrMatrixFull = (TH2*) corrMatrixFullCanvas -> FindObject("Corr_CorrMatrix_GlobalFit_unconditionnal_mu");
  } else {
    corrMatrixFullCanvas = (TCanvas*) gDirectory -> Get("can_CorrMatrix_GlobalFit_conditionnal_mu0");
    corrMatrixFull = (TH2*) corrMatrixFullCanvas -> FindObject("Corr_CorrMatrix_GlobalFit_conditionnal_mu");
  }

  string yearSuffix = TString::Format("_Y%i", year).Data();
  string alphaPrefix = "alpha_SysBTag";
  vector<string> alphaNames;
  vector<double> alphaPost;
  vector<double> alphaPostUp;
  vector<double> alphaPostDo;

  for (int j = 0; j < nSF; j++) {
    int index = nSF - 1 - j; // reverse order
    string alphaName = TString::Format((alphaPrefix + label + "%iEffic").c_str(), index).Data();
    alphaName += yearSuffix;
    alphaNames.push_back(alphaName);
  }

  cout << "Alphas from fit:" << endl;
  for (int j = 0; j < nSF; j++) {
    string alphaName = alphaNames[j];
    bool found = false;
    for (int i = 0; i < pars -> getSize(); i++) {
      RooRealVar* arg = (RooRealVar*) pars -> at(i);
      double alpha = arg -> getVal();
      double alphaUp = alpha + arg -> getErrorHi();
      double alphaDo = alpha + arg -> getErrorLo();

      if (arg -> GetName() == alphaName) {
        found = true;

        cout << setw(20) << "var " << j << "   ";
        cout << setw(20) << alphaName;
        cout << setw(20) << alpha;
        cout << " Up: ";
        cout << setw(20) << alphaUp;
        cout << " Do: ";
        cout << setw(20) << alphaDo;
        cout << endl;

        alphaPost.push_back(alpha);
        alphaPostUp.push_back(alphaUp);
        alphaPostDo.push_back(alphaDo);
      }
    }
    // add those that are ignored in the fit
    if (!found) {
      alphaPost.push_back(0);
      alphaPostUp.push_back(1);
      alphaPostDo.push_back(-1);
    }
  }

  // get correlation matrix

  int nTot = corrMatrixFull -> GetNbinsX();
  cout << "Total post fit alphas: " << nTot << endl;
  TMatrixT<double> corrMatrixPre(nSF, nSF);
  TMatrixT<double> corrMatrixPost(nSF, nSF);

  // reduce matrix to alphas 
  cout << "Read correlation matrix from fit";
  // loop over alpha names
  for (int i = 0; i < nSF; i++) {
    cout << ".";
    flush(cout);
    TString alphaNameX = alphaNames[i];
    for (int j = 0; j < nSF; j++) {
      TString alphaNameY = alphaNames[j];
      if (i == j) {
        corrMatrixPre[i][j] = 1;
        corrMatrixPost[i][j] = 1;
        continue;
      }
      int start = matrixVariationJacobian1.GetNcols() - eigenVectorHistos.size() - 1;
      if (i < start || j < start)
        continue;
      // loop over bins in corr histogram
      for (int iX = 1; iX <= nTot; iX++) {
        TString binLabelX = corrMatrixFull -> GetXaxis() -> GetBinLabel(iX);
        if (!binLabelX.Contains(alphaPrefix.c_str())) continue;
        for (int iY = nTot; iY >= 1; iY--) {
          TString binLabelY = corrMatrixFull -> GetYaxis() -> GetBinLabel(iY);
          // check names and save
          if (alphaNameX == binLabelX && alphaNameY == binLabelY) {
            corrMatrixPre[i][j] = (i == j);
            corrMatrixPost[i][j] = corrMatrixFull -> GetBinContent(iX, iY);
          }
        }
      }
    }
  }
  cout << endl;
  //cout << "Post fit correlation matrix in alpha:" << endl;
  //corrMatrixPost.Print();

  // get covariance matrix

  TMatrixT<double> covMatrixPre = corrMatrixPre;
  TMatrixT<double> covMatrixPost(nSF, nSF);

  for (int i = 0; i < nSF; i++) {
    for (int j = 0; j < nSF; j++) {
      float cov = corrMatrixPost[i][j];
      cov *= fabs(alphaPostUp[i] - alphaPostDo[i]) / 2.;
      cov *= fabs(alphaPostUp[j] - alphaPostDo[j]) / 2.;
      covMatrixPost[i][j] = cov;
    }
  }
  //cout << "Pre fit covariance matrix in alpha:" << endl;
  //covMatrixPre.Print();
  cout << "Post fit covariance matrix in alpha:" << endl;
  covMatrixPost.Print();

  // build full transform matrix (alpha -> pT)

  TMatrixT<double> transformMatrix(nSF, nSF);
  TMatrixT<double> transformMatrixT(nSF, nSF);
  for (int i = 0; i < nSF; i++) {
    for (int j = 0; j < nSF; j++) {
      //float element = 1 / sqrt(eigenValues[j]) * eigenVectorsOriginal[i][j];
      float element = matrixVariationsFromHistos[i][j];
      transformMatrix[i][j] = element;
      transformMatrixT[j][i] = element;
    }
  }
  //cout << "Transform Matrix:" << endl;
  //transformMatrix.Print();

  TVectorT<double> alphaPreVec(nSF);
  //cout << "Pre-fit alpha vector:" << endl;
  //alphaPreVec.Print();

  TVectorT<double> alphaPostVec(nSF, &alphaPost[0]);
  //cout << "Post-fit alpha vector:" << endl;
  //alphaPostVec.Print();

  TVectorT<double> pTPreVec = transformMatrix * alphaPreVec;
  //cout << "Pre-fit pT vector (relative to nominal):" << endl;
  //pTPreVec.Print();

  TVectorT<double> pTPostVec = transformMatrix * alphaPostVec;
  cout << "Post-fit pT vector (relative to nominal):" << endl;
  pTPostVec.Print();

  TMatrixT<double> covMatrixPostPt = transformMatrix * covMatrixPost * transformMatrixT;
  TMatrixT<double> covMatrixPrePt = transformMatrix * covMatrixPre * transformMatrixT;

  //cout << "Pre fit covariance matrix in pT:" << endl;
  //covMatrixPrePt.Print();

  //cout << "Post fit covariance matrix in pT:" << endl;
  //covMatrixPostPt.Print();

  TH1* histoSFsPre = expandSFhisto(histoSFsOrig, matrixVariationJacobian, pTPreVec, covMatrixPrePt);
  TH1* histoSFsPost = expandSFhisto(histoSFsOrig, matrixVariationJacobian, pTPostVec, covMatrixPostPt);
  
  // ======================= plot pre- and post-fit SFs =======================
  
  TCanvas* canvas = new TCanvas();
  canvas -> SetRightMargin(0.15);
  
  histoSFsOrig -> Draw("colz");
  string filename = plotPrefix + "_sf_orig";
  //canvas -> Print((filename + graphicsExt).c_str());
  
  histoSFsPre -> Draw("colz");
  filename = plotPrefix + "_sf_pre";
  canvas -> Print((filename + graphicsExt).c_str());
  
  histoSFsPost -> Draw("colz");
  filename = plotPrefix + "_sf_post";
  canvas -> Print((filename + graphicsExt).c_str());
  
  delete canvas;
  
  drawSFslices(histoSFsPre, histoSFsPost, plotPrefix);

  // ======================= calculate efficiencies =======================
  /*
  // get uncorrected "MC" efficiencies * SFs (pre-fit)
  
  string effiMapFileName = "effimaps/effimaps_mc12_wjets_jvf05_hadronPt5_030513.root";
  if (year == 2011)
    effiMapFileName = "effimaps/effimaps_mc11_wjets_jvf075_hadronPt5_040513.root";
  
  TFile* effiMapFile = new TFile(effiMapFileName.c_str());
  string effiHistoName = "eff_b_var";
  if (label == "C")
    effiHistoName = "eff_c_var";
  TH1* effiHisto = (TH1*) effiMapFile -> Get(effiHistoName.c_str()) -> Clone();
  effiHisto -> SetDirectory(0);

  TGraphAsymmErrors* effMC = (TGraphAsymmErrors*) graphSFPre -> Clone();
  effMC -> SetName("graphEffPre");

  //Analysis::CalibrationDataVariables m_flavSF_ajet;
  //m_flavSF_ajet.jetEta = 0;
  //m_flavSF_ajet.jetAuthor = author;

  for (int i = 0; i < nSF; i++) {

    double xval; // SFs
    double yval;
    double yvalUp = graphSFPre -> GetErrorYhigh(i);
    double yvalDo = graphSFPre -> GetErrorYlow(i);

    graphSFPre -> GetPoint(i, xval, yval);

    //m_flavSF_ajet.jetPt = xval * 1000;
    //Analysis::CalibResult res = m_flavSF_calib -> getMCEfficiency(m_flavSF_ajet, label, OP, Analysis::Total);
    //double eff = res.first;
    
    double eff = getIntegratedEfficiency(effiHisto, pTbins[i], pTbins[i+1]);

    effMC -> SetPoint(i, xval, eff * yval);
    effMC -> SetPointEYhigh(i, eff * yvalUp);
    effMC -> SetPointEYlow(i, eff * yvalDo);
  }


  // get uncorrected "MC" efficiencies * SFs (post-fit)

  TGraphAsymmErrors* effMCpost = (TGraphAsymmErrors*) graphSFPost -> Clone();
  effMCpost -> SetName("graphEffPost");

  for (int i = 0; i < nSF; i++) {

    double xval; // SFs
    double yval;
    double yvalUp = graphSFPost -> GetErrorYhigh(i);
    double yvalDo = graphSFPost -> GetErrorYlow(i);

    graphSFPost -> GetPoint(i, xval, yval);

    //m_flavSF_ajet.jetPt = xval * 1000;
    //Analysis::CalibResult res = m_flavSF_calib -> getMCEfficiency(m_flavSF_ajet, label, OP, Analysis::Total);
    //double eff = res.first;

    double eff = getIntegratedEfficiency(effiHisto, pTbins[i], pTbins[i+1]);

    effMCpost -> SetPoint(i, xval, eff * yval);
    effMCpost -> SetPointEYhigh(i, eff * yvalUp);
    effMCpost -> SetPointEYlow(i, eff * yvalDo);
  }


  // draw

  TCanvas* canvas2 = new TCanvas();

  effMC -> GetXaxis() -> SetTitle("p_{T} [GeV]");
  effMC -> GetYaxis() -> SetTitle("corrected efficiency");

  effMC -> Draw("A2");
  effMCpost -> Draw("P same");

  leg -> Draw();

  filename = plotPrefix + "_eff";
  canvas2 -> Print((filename + ".png").c_str());
  canvas2 -> Print((filename + ".eps").c_str());

  */
  // ======================= plot pre- and post-fit correlation matrices in pT =======================

  canvas = new TCanvas();
  canvas -> SetRightMargin(0.15);

  TH2* covHistoPrePt = getCorrHistoFromCovMatrix(covMatrixPrePt);
  covHistoPrePt -> Draw("colz");
  filename = plotPrefix + "_corr_pre";
  canvas -> Print((filename + graphicsExt).c_str());
  delete covHistoPrePt;

  TH2* covHistoPostPt = getCorrHistoFromCovMatrix(covMatrixPostPt);
  covHistoPostPt -> Draw("colz");
  filename = plotPrefix + "_corr_post";
  canvas -> Print((filename + graphicsExt).c_str());
  delete covHistoPostPt;

  delete canvas;

  // ======================= plot pre-fit EV variations in pT =======================
  /*
  TVectorT<double> alphaPreSingleVec(nSF);

  TCanvas* canvas5 = new TCanvas();
  canvas5 -> SetRightMargin(0.20);
  TLegend* legSingle = new TLegend(0.82, 0.4, 0.98, 0.95);
  legSingle -> SetFillColor(0);
  TLine* line = new TLine();
  line -> SetLineStyle(2);

  for (int i = 0; i < nSF; i++) {
    for (int j = 0; j < nSF; j++) {
      alphaPreSingleVec[j] = (i == j);
    }
    TString histName = TString::Format("h_EVvar_%i", i);
    TVectorT<double> pTPostSingleVec = transformMatrix * alphaPreSingleVec;
    TH1* histoPtSingleUp = new TH1D(histName, histName, nSF, 0, nSF);
    histoPtSingleUp -> SetBins(nSF, &pTbins[0]);

    double scale = 100;
    double spacing = 0.1 * scale;
    if (year == 2011 && label == "C") {
      spacing *= 2;
    }
    double offset = -spacing * i;
    for (int j = 0; j < nSF; j++) {
      histoPtSingleUp -> SetBinContent(j + 1, pTPostSingleVec[j] * scale + offset);
    }

    int color = i + 1;
    if (color >= 5)
      color++;
    if (color >= 10)
      color++;
    histoPtSingleUp -> SetLineColor(color);

    histoPtSingleUp -> GetYaxis() -> SetRangeUser(-(nSF - 1) * spacing - 2 * spacing, 1 * spacing);
    histoPtSingleUp -> GetXaxis() -> SetTitle("p_{T} [GeV]");
    histoPtSingleUp -> GetYaxis() -> SetTitle("shift of scale-factor [%]");

    if (i == 0)
      histoPtSingleUp -> Draw();
    else
      histoPtSingleUp -> Draw("SAME");

    line -> DrawLine(pTbins[0], offset, pTbins[nSF], offset);
    legSingle -> AddEntry((TObject*) histoPtSingleUp, TString::Format((label + "%i Up - %i").c_str(), i, -(int) offset) + "%", "l");
  }
  legSingle -> Draw();

  filename = plotPrefix + "_EVvar";
  canvas5 -> Print((filename + ".png").c_str());
  canvas5 -> Print((filename + ".eps").c_str());
  */
  
  // write out objects

  cout << "Writing result file ..." << endl;
  
  filename = plotPrefix + "_results.root";
  TFile* outFile = new TFile(filename.c_str(), "recreate");
  
  //histoSFsOrig -> Write();
  histoSFsPre -> Write();
  histoSFsPost -> Write();
  //effMC -> Write();
  //effMCpost -> Write();
  
  outFile -> Close();
  delete outFile;

  
  // delete objects

  cout << "Deleting objects ..." << endl;

  delete histoSFs;
  delete histoSFsOrig;
  delete histoSFsPre;
  delete histoSFsPost;
  delete fitResult;
  //  delete pars;
  delete corrMatrixFull;
  delete corrMatrixFullCanvas;
  //delete effMC;
  //delete effMCpost;
  //delete canvas5;
  //delete legSingle;
  //delete line;
  fitFile -> Close();
  delete fitFile;
  delete m_flavSF_calib;
  
  //effiMapFile -> Close();
  //delete effiMapFile;
  //delete effiHisto;

  cout << "Done!" << endl;
}
