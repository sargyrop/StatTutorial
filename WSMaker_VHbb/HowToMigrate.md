# Document migration of VHbb analysis to the new package structure

The migration is done in two steps:
* from the last tag with monolithic structure 10-04-00 to the first tag with the new structure 11-00-00
* from tag 11-00-00 to tag 11-01-00, as it contains changes which require to adapt your code

## Create an empty package on git and clone it
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker_VHbb.git
cd WSMaker_VHbb
```

## Move stuff in
Assuming you have the "old" `WSMaker/` (tag 10-04-00) and `WSMaker_VHbb/` in the same base directory.
```
mkdir include src scripts util inputConfigs
cp ../WSMaker/src/binning_vhbbrun2.cpp ../WSMaker/src/systematiclistsbuilder_vhbbrun2.cpp ../WSMaker/src/samplesbuilder_vhbbrun2.cpp ../WSMaker/src/regiontracker_vhbbrun2.cpp src/
cp ../WSMaker/include/binning_vhbbrun2.hpp ../WSMaker/include/systematiclistsbuilder_vhbbrun2.hpp ../WSMaker/include/samplesbuilder_vhbbrun2.hpp ../WSMaker/include/regiontracker_vhbbrun2.hpp include/
# if you have your own regionnamesparser or inputshandler, don't forget to take them as well.
cp -r ../WSMaker/scripts/VHbbRun2/* scripts/
cp -r ../WSMaker/util/MakeWorkspace.C util/
# if you use the Splitter in your analysis
cp -r ../WSMaker/util/SplitInputs.C util/
# finally your inputConfigs:
cp -r ../WSMaker/inputConfigs/SMVH* inputConfigs/
# also copy your configs/ if you have some. VHbb doesn't user any as they are always generated by the python scripts
# Finally, gitignore is useful
cp ../WSMaker/.gitignore .
```

## Add the new core as submodule
We will stick to tag 11-00-00
```
git submodule add ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker.git WSMakerCore
cd WSMakerCore
git checkout 11-00-00
git submodule init
git submodule update
cd ..
```

A worthy read:
https://git-scm.com/book/en/v2/Git-Tools-Submodules

## Create the setup file
Something like:

```
export ANALYSISTYPE="VHbbRun2"
echo "Setting ANALYSISTYPE to : "$ANALYSISTYPE
export ANALYSISDIR=$(pwd)
source WSMakerCore/setup.sh
export NCORE=4
```

## Create the CMakeLists.txt file
I suggest you to start from the VHbb one:

https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker\_VHbb/blob/master/CMakeLists.txt

## It's time to do a first commit
```
git add *
git commit -am"First import from WSMaker-10-04-00"
```

## Do code modifications
### Includes
In all your .cpp and .hpp files, update the includes, e.g `#include "binning.hpp"` becomes
`#include "WSMaker/binning.hpp"`

### MakeWorkspace
The main executable is now specific to each analysis, because it's where you declare from which
building blocks your analysis is made (what used to be in AnalysisHander before). You only have
to change:

* include all the analysis-specific blocks, e.g `#include "systematiclistsbuilder_vhbbrun2.hpp"`
* change how the Engine is built:

```
   using AnalysisVHbbRun2 = Analysis_Impl<RegionNamesParser_Run2, InputsHandlerRun2,
         RegionTracker_VHbbRun2, SystematicListsBuilder_VHbbRun2, SamplesBuilder_VHbbRun2,
         BinningTool_VHbbRun2>;

   Engine myEngine(config, version, std::make_unique<AnalysisVHbbRun2>());
```

### Scripts
For VHbb we had exactly 0 modifications to do. But it's maybe worth having a look to check
for hardcoded paths and similar things.

## Enjoy !
That should be it !

```
source setup.sh
cd build
cmake ..
make -j5
cd ..
# run your scripts to make workspaces and fits...
```

## Save your work
```
git commit -am"Working version after porting to new structure"
git push
```

