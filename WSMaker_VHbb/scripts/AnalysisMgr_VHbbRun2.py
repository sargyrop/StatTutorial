from AnalysisMgr import WorkspaceConfig

class WorkspaceConfig_VHbbRun2(WorkspaceConfig):
    def __init__(self, *args, **kwargs):
        kwargs["Analysis"] = "VHbbRun2"
        super(WorkspaceConfig_VHbbRun2, self).__init__(*args, **kwargs)
        self["Regions"] = []
        return

    def set_regions(self, var2tag='mva', var1tag='dRBB', zero_lepton=True, one_lepton=False, two_lepton=False, use1tag = True, use4pjet = False, doptvbin=True,doPostFit=False):
        self["Regions"] = [ ]
        self.append_regions(var2tag, var1tag, zero_lepton, one_lepton, two_lepton, use1tag, use4pjet, doptvbin, doPostFit)

    def append_regions(self, var2tag='mva', var1tag='dRBB', zero_lepton=True, one_lepton=False, two_lepton=False, use1tag = True, use4pjet = False, doptvbin=True, doPostFit=False):
        # SET PTV BINNING OK FOR CUTBASED AND MVA
        #if ( var2tag is 'mva' ):
        #    doptvbin = False
        #elif ( var2tag is 'mvadiboson' ):
        #    doptvbin = False
        #elif ( var2tag is 'mBB' ):
        #    doptvbin = True

        if zero_lepton:
            tags = ["1","2"]
            jets = ["2","3","4p"]

            # PTV BINNING
            if doptvbin:
                ptvs = ["150_200","200" ]
            else:
                ptvs = ["150"]

            # DEFINE REGIONS
            for t in tags:
                if ( (not use1tag) and (t is "1") ):
                    continue
                for j in jets:
                    if ( (use4pjet is False) and (j is "4p") ):
                        continue
                    if ( (var2tag in ['MEff3', 'mBBJ', 'pTJ3'] ) and (j is "2") ):
                        continue
                    if ( (var2tag in ['MEff'] ) and (j is "3") ):
                        continue
                    for p in ptvs:
                        if t is "2":
                            if doptvbin:
                                self["Regions"].append("13TeV_ZeroLepton_{0}tag{1}jet_{2}ptv_SRcuts_{3}".format(t, j, p, var2tag))
                            else:
                                self["Regions"].append("13TeV_ZeroLepton_{0}tag{1}jet_{2}ptv_SR_{3}".format(t, j, p, var2tag))

                        elif t is "1":
                            self["Regions"].append("13TeV_ZeroLepton_{0}tag{1}jet_{2}ptv_SR_{3}".format(t, j, p, var1tag))
                        else:
                            print 'No variable set up for %s tag, exiting...' % t
                            exit()

        if one_lepton:
            tags = ["1","2"]
            jets = ["2","3", "4p"]
            # PTV BINNING
            # NOT IMPLEMENTED IN THE INPUTS
            #if ( var2tag is 'mBB' ):
            #    ptvs = ["150_200","200" ]
            if doptvbin:
                ptvs = ["150_200","200" ]
            else:
                ptvs = ["150" ]
            # define regions
            for t in tags:
                if ( (not use1tag) and (t is "1") ):
                    continue
                for j in jets:
                    if ( (not use4pjet) and (j is "4p") ):
                        continue
                    if ( (var2tag in ['mBBJ', 'pTJ3'] ) and (j is "2") ):
                        continue
                    for p in ptvs:
                        if t is "2":
                            self["Regions"].append("13TeV_OneLepton_{0}tag{1}jet_{2}ptv_WhfSR_{3}".format(t, j, p, var2tag))
                            if ( ( var2tag is not 'mBB' ) or ( doPostFit ) ):
                                self["Regions"].append("13TeV_OneLepton_{0}tag{1}jet_{2}ptv_WhfCR_{3}".format(t, j, p, var2tag))
                        elif t is "1":
                            self["Regions"].append("13TeV_OneLepton_{0}tag{1}jet_{2}ptv_SR_{3}".format(t, j, p, var1tag))
                        else:
                            print 'No variable set up for %s tag, exiting...' % t
                            exit()

        if two_lepton:
            tags = ["1","2"]
            jets = ["2","3p"]#, "4p"]
            # ptv binning.New split at 75
            if ( doptvbin ): #CHECK IF CUTBASED ANALYSIS
#                ptvs = ["0_75","75_150","150_200","200" ]
                ptvs = ["75_150","150_200","200" ]
            else:
#                ptvs = ["0_75","75_150","150" ]
                ptvs = ["75_150","150" ]
            # define regions
            for t in tags:
                if ( (not use1tag) and (t is "1") ):
                    continue
                for j in jets:
                    for p in ptvs:
                        if t is "2":
                            self["Regions"].append("13TeV_TwoLepton_{0}tag{1}jet_{2}ptv_SR_{3}".format(t, j, p, var2tag))
                            # Add in top control region
                            if ( doPostFit ): #
                                cr_var = var2tag
                            elif ( doptvbin ) : # do cut based
                                cr_var = "mBB"
                            else : # do MVA
                                cr_var = "mBBMVA"

                            if ( doptvbin ): # high/low for cba are merged for cut based
                                if p is "200": 
                                    continue
                                else:
                                    cr_p = p.replace("_200","")
                            else:
                                cr_p = p
                            self["Regions"].append("13TeV_TwoLepton_{0}tag{1}jet_{2}ptv_topemucr_{3}".format(t, j, cr_p, cr_var))
                        elif t is "1":
                            self["Regions"].append("13TeV_TwoLepton_{0}tag{1}jet_{2}ptv_SR_{3}".format(t, j, p, var1tag))
                        else:
                            print 'No variable set up for %s tag, exiting...' % t
                            exit()
        self.check_regions()
