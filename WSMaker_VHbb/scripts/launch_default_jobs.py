#!/usr/bin/env python

import sys
import os
import AnalysisMgr_VHbbRun2 as Mgr
import BatchMgr as Batch
import time,copy

# Inputs
InputVersion = "SMVHVZ_LHCP17_MVA_v06"
#InputVersion = "SMVH_STXS_v07" # download it from /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2016/STXS_Split/
#InputVersion = "SMVHVZ_Summer18_MVA_mc16ac_v03"
GlobalRun = False # caution: when true, it will change the following settings based on the sys.argv[1]
doPostFit = False # needed when produce the post-fit plots for the variables used in BDT training, if you don't know it, please turn it off

doCutBase = False
doDiboson = False
# Channels
#channels = ["0", "1", "2", "012"]
channels = ["012"]

# Syst or not
syst_type = ["Systs"]#, "StatOnly", "MCStat"]

#Blinding / injection
signalInjection = 0 # DEFAULT 0: no injection
doExp = "1" # "0" to run observed, "1" to run expected only

# How to run
run_on_batch = False

# What to run
createSimpleWorkspace = True
runPulls = False
runBreakdown = False
runRanks = False
runLimits = False
runP0 = True
runToyStudy = False

# Turn on additional debug plots
doplots = False

# Lesser used flags:
masses = [125]
doptvbin = True if doCutBase else False # decided w.r.t docutbase
if not doPostFit:
  if doCutBase:
    vs2tag = ['mBB']
  elif doDiboson:
    vs2tag = ['mvadiboson']
  else :
    vs2tag = ['mva']
else:
  vs2tag = ['pTV','MET','pTB1','pTB2','mBB','dRBB','dEtaBB','dPhiVBB','dEtaVBB','MEff','MEff3','dPhiLBmin','mTW','mLL','dYWH','Mtop','pTJ3','mBBJ','mBBJ3']
  #vs2tag = ['mBBJ']
  runPulls = False
  runBreakdown = False
  runRanks = False
  runLimits = False
  runP0 = False
  runToyStudy = False
  doplots = False
vs1tag = ['dRBB']
use1tag = False
runRankingOnPostfitAsimov = True

if GlobalRun:
  if sys.argv[1].endswith('.01'):
    print 'run set 1'
    doExp = "1"
    runPulls = True
    runBreakdown = True
    runRanks = True
    runLimits = False
    runP0 = True
    doplots = True
    runRankingOnPostfitAsimov = True
  elif sys.argv[1].endswith('.02'):
    print 'run set 2'
    doExp = "0"
    runPulls = False
    runBreakdown = False
    runRanks = False
    runLimits = False
    runP0 = True
    doplots = False
  elif sys.argv[1].endswith('.03'):
    print 'run set 3'
    doExp = "1"
    runPulls = False
    runBreakdown = False
    runRanks = True
    runLimits = False
    runP0 = False
    doplots = False
    runRankingOnPostfitAsimov = False
  elif sys.argv[1].endswith('.04'):
    print 'run set 4'
    doExp = "0"
    runPulls = True 
    runBreakdown = True
    runRanks = True
    runLimits = False
    runP0 = True
    doplots = False
    runRankingOnPostfitAsimov = False

#the argument for ConfigFlags; delimited and camel-cased for readability (cf. scripts/AnalysisMgr.py, parse_config_flags for full options):
baseline_configs = {'UseJet21NP':True,'CutBase':False,'DoDiboson':False,'FitWHZH':False, 'FitVHVZ':False,'DoSystsPlots':False,"DoShapePlots":False, 
                    'StatThreshold':0.0, 'PlotsFormat':"png", 'CreateROOTfiles':False,'PostFit':False,"DefaultFitConfig":False, 'FitWZZZ':False, 'DoSTXS':False, 'FitSTXSScheme':1}#, 'DecorrPOI':'L'}
if doCutBase:
    baseline_configs.update({"CutBase":True})
if doDiboson:
    baseline_configs.update({"DoDiboson":True})
if doPostFit:
    baseline_configs.update({"PostFit":True})
    baseline_configs.update({"DefaultFitConfig":True})

baseline_configs_SimpleWS = copy.deepcopy(baseline_configs)
if createSimpleWorkspace and doplots:
    baseline_configs_SimpleWS.update({'DoSystsPlots':True,"DoShapePlots":True, 'SmoothingControlPlots':True, "ShowMCStatBandInSysPlot": True})


def add_config(config, mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, doptvbin=True, doPostFit=False):
    config.update({'InputVersion':InputVersion,'MassPoint':mass,'DoInjection':signalInjection,'SkipUnkownSysts':True})
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, **config)

    if "0" in channel:
        conf.append_regions(var2tag, var1tag, True, False, False,
                            use1tag, False, doptvbin)
    if "1" in channel:
        conf.append_regions(var2tag, var1tag, False, True, False,
                            use1tag, False, doptvbin, doPostFit)
    if "2" in channel:
        conf.append_regions(var2tag, var1tag, False, False, True,
                            use1tag, False, doptvbin, doPostFit)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag

    return conf.write_configs(fullversion)


if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: launch_default_jobs.py <outversion>"
        exit(0)

    outversion = sys.argv[1]
    """
    print "WARNING: if you haven't touched the base configuration, prepare to"
    print "see your lxbatch quota die quickly"

    print "I'll give you 5 seconds to abort..."
    time.sleep(5)
    """

    print "Adding config files..."

    print "Let's start by a few jobs where we run simple fits to get pulls"
    print "and other things when they are ready"
    configfiles = []
    configfiles_simpleWS = []
    if signalInjection is not 0:
        doExp="1"

    for sys in syst_type:
        for c in channels:
            for m in masses:
                for var2tag in vs2tag:
                    for var1tag in vs1tag:
                        configfiles += add_config(baseline_configs, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin, doPostFit=doPostFit)
                        configfiles_simpleWS += add_config(baseline_configs_SimpleWS, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin, doPostFit=doPostFit)
    for fi in configfiles:
        print fi

    dataForRanking = "obsData"
    if doExp == '1':
        if runRankingOnPostfitAsimov:
            dataForRanking = "asimovData_paramsVals_GlobalFit_mu1.00,conditionalGlobs_1"
        else:
            dataForRanking = "asimovData"


    if run_on_batch:

        if createSimpleWorkspace:
            print 'creating simple ws named %s' % outversion
            Batch.run_lxplus_batch(configfiles_simpleWS, outversion+"_fullRes", ["-w"], '1nh')

        # hopefully queue 8nh is enough for now.
        if runPulls:
            if outversion.endswith('.04') and GlobalRun:
                Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                       ["-w", "--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}"],
                                       '8nh')
            else:
                Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                       ["-w", "--fcc", "5,9@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"],
                                       '8nh')
        if runBreakdown:
            Batch.run_lxplus_batch(configfiles, outversion+"_breakdown",
                                   ["-w", "-l", doExp+",{MassPoint}", "-u", doExp+",{MassPoint}"],
                                   '8nh')

        if runLimits:
            #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-l", doExp+",{MassPoint}"],mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_limits",
                                   ["-w", "-l", doExp+",{MassPoint}"],
                                   '8nh')
        if runP0:
            #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-s", doExp+",{MassPoint}"],mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_p0",
                                   ["-w", "-s", doExp+",{MassPoint}"],
                                   '8nh')
        if runRanks:
            # create workspaces locally
            commands = ["-w"]
            if doExp == "1" and runRankingOnPostfitAsimov:
                commands.extend(["--fcc", "5s@{MassPoint}"])
            Batch.run_local_batch(configfiles, outversion+"_ranking", commands)
            # Then run the rankings on batch
            #Batch.run_slurm_batch(configs, out+"_fullRes",cmd_opts,jobtype='_rank',mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_ranking",['-w', '-k', "jobs", '-r','{MassPoint},'+dataForRanking],
                                   jobs=10, queue='8nh')
        if runToyStudy:
            # create workspaces locally
            Batch.run_local_batch(configfiles, outversion+"_toys", ["-w"])
            # Then run the toys on batch
            Batch.run_lxplus_batch(configfiles, outversion+"_toys",['-w', '-k', "jobs", "--fcc", "6"],
                                   jobs=50, queue='1nd')

    else: # local running
        commands = ["-w"]
        #commands = []
        if runPulls:
            if outversion.endswith('.04') and GlobalRun:
                commands.extend(["--fcc", "2,5s,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}"])
            else:
                commands.extend(["--fcc", "5,7@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"])
                #commands.extend(["-p", "0@{MassPoint}"])
        if runLimits or runBreakdown:
            commands.extend(["-l", doExp+",{MassPoint}"])
        if runBreakdown:
            commands.extend(["-u", doExp+",{MassPoint}"])
        if runP0:
            commands.extend(["-s", doExp+",{MassPoint}"])
        if runRanks:
            commands.extend(['-r','{MassPoint},'+dataForRanking])
        if runToyStudy:
            commands.extend(["--fcc", "6@{MassPoint}"])
        Batch.run_local_batch(configfiles_simpleWS, outversion+"_fullRes", commands)

