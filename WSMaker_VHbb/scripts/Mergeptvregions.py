#!/usr/bin/env python

# FOR BOOSTED ANALYSIS

import os
import subprocess

def main(var="BDT") :
  
  channels  = ["TwoLepton", "OneLepton"]
  nTag_list = ["0tag", "1tag", "2tag"]
  njet_list  = ["2jet", "2pjet", "3jet", "3pjet", "4pjet"]
  
  # high+low mBB cr
  print "variable :",var

  for nChan in channels : 

    for nTag in nTag_list : 

      for njet in njet_list : 
     
        print "Merging high ptv regions : for %s, %s, %s" % (nChan, nTag, njet)
       
        filename1  = "13TeV_%s_%s%s_150_200ptv_SR_%s.root" % (nChan, nTag, njet, var)
        filename2  = "13TeV_%s_%s%s_200ptv_SR_%s.root" % (nChan, nTag, njet, var)
     
        if os.path.isfile(filename1) and os.path.isfile(filename2):
          os.system("hadd 13TeV_"+nChan+"_"+nTag+njet+"_150ptv_SR_"+var+".root "+filename1+" "+filename2)
        else :
          print "Some files %s and %s are missing..." % (filename1, filename2)

if __name__ == "__main__":
  
  var_list = [ "mva", "mvadiboson" ]
  for var in var_list : 
      main(var)
      



