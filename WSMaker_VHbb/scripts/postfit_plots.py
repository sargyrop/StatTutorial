import subprocess,argparse,os,shutil

fcc_tag ='flumi' # this is the 'outversion' (following the convention of launch_default_jobs.py) attached to the FCC we use as the fit result in making the postfit plots
fcc_version='SMVH_mva_v14a' # this is the 'InputVersion' (following the convention of launch_default_jobs.py and inputConfigs) of the FCC we use as the fit result in making the postfit plots
fcc_mva='mva' # mva variable used in the FCC
dist_tag='wsonly' # this is the 'outversion' attached to the workspaces with distributions used as the basis of the postfit plots (i.e. the prefit distributions)
dist_version='SMVH_mva_v14a' # 'InputVersion' of the distribution workspaces
top='.'#'.'
fcc_pattern='{3}/fccs/FitCrossChecks_{0}.{1}_fullRes_VHbbRun2_13TeV_{1}_012_125_Systs_use1tagFalse_use4pjetFalse_{2}_combined/' #version, wsintag, fit var, top dir
fcc=fcc_pattern.format(fcc_version,fcc_tag,fcc_mva,top)
#fcc='fccs/nicolas'
ws_pattern='{0}.{3}_fullRes_VHbbRun2_13TeV_{3}_{1}_125_Systs_use1tagFalse_use4pjetFalse_{2}' #version, lep, var, wsouttag
#vars={0:['mBB','MET'],1:['mBB','mTW','pTV'],2:['mBB','mLL','pTV']}
vars={'012':['mvadiboson']}
vars={'0':['mBB'],'1':['mBB'],'2':['mBB']}
sums_hash   = {'0':['mBB'],'1':['mBB'],'2':['mBB'],'012':['mBB']}
master_hash = {'0':['mBB','MET','pTB1','pTB2','dRBB','dEtaBB','dPhiVBB','pTJ3','mBBJ','MEff','MEff3'],
               '1':['mBB','pTV','mTW','pTB1','PTB2','dYWH','Mtop','dPhiLBmin','dPhiVBB','dRBB','MET'],
               '2':['mBB','pTV','mLL','pTB1','pTB2','dRBB','dEtaBB','dPhiVBB'],
               '012':['mBB','mva']}
main_hash  = {'0':['mBB','MET'],
              '1':['mBB','pTV','mTW'],
              '2':['mBB','pTV','mLL']}
back_hash  = {'0':['pTB1','pTB2','dRBB','dEtaBB','dPhiVBB','pTJ3','mBBJ','MEff','MEff3'],
              '1':['pTB2','dYWH','Mtop','pTB1','dPhiLBmin','dPhiVBB','dRBB','MET'],
              '2':['pTB1','pTB2','dRBB','dEtaBB','dPhiVBB'],
              '012':['mBB','mva']}
bucket=vars
leps=vars
parser = argparse.ArgumentParser(description='Postfit derp')
parser.add_argument('-f','--full',help='Use the full hash',action='store_true')
parser.add_argument('-m','--main',help='Use the main hash',action='store_true')
parser.add_argument('-b','--back',help='Use the back hash',action='store_true')
parser.add_argument('-l','--lep',help='Which lep case to do',default='yogurt')
parser.add_argument('-o','--override',help='Overwrite plots',action='store_true')
parser.add_argument('-s','--sums',help='Just do the special S/B sub mBB plots',action='store_true')
args=parser.parse_args()

if args.lep is 'yogurt':
    leps=['0','1','2']
elif args.lep is '-1':
    leps=['0','1','2','012']
else:
    leps=[args.lep]
if args.sums:
    bucket=sums_hash
elif args.main:
    bucket=main_hash
elif args.back:
    bucket=back_hash
elif args.full:
    bucket=master_hash

ftype = {'0':'Prefit','2':'GlobalFit_unconditional_mu1','3':'GlobalFit_conditional_mu1'}

for v in leps:
    for var in bucket[v]:
        for p in ['0','2']: #the plot types you want--which is which are hashed in the ftype variable above
            cmd = ['python','scripts/doPlotFromWS.py','-p',p,'-f',fcc]
            if (var is 'pTV' and (v is '2' or v is 2)) or (var is 'mBB' and args.sums):
                cmd.append('-s')
            ws=ws_pattern.format(dist_version,v,var,dist_tag) #'camilla'
            if not os.path.exists('workspaces/{0}/combined/125.root'.format(ws)):
                print 'THE WORKSPACE {0} DOES NOT EXIST--SKIPPING!'.format(ws)
                continue
            cmd.append(ws)
            pdir='plots/{0}/{1}'.format(ws,'prefit' if p is '0' else 'postfit')
            if os.path.isdir(pdir):
                if args.override:
                    shutil.rmtree(pdir)
                elif os.path.exists('plots/{0}/{1}/plotojbs_{2}.yik'.format(ws,'prefit' if p is '0' else 'postfit',ftype[p])):
                    print 'Skipping '+cmd
                    continue
            log = 'logs/plot{0}{1}lep{2}.log'.format(var,v,p)
            print ' '.join(cmd)
            yup=open(log,'w')
            ok=subprocess.Popen(cmd,stderr=yup,stdout=yup)
            ok.wait()
            yup.close()
            print 'Complete....'
