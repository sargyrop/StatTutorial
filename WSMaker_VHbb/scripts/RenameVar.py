#!/usr/bin/env python

# FOR BOOSTED ANALYSIS

import os
import sys
import subprocess
import utility as U

def main(var_org='mva28',var_tar='mva',version='None') :
  
  channels  = ["ZeroLepton"]
  nTagnJet_list = ["2tag2jet","2tag3jet"]
  ptv_list  = ["150ptv"]
  
  #
  print "variable :",var_org
  print "regions  :",reg

  for nChan in channels : 

    for nTag in nTagnJet_list : 

      for ptv in ptv_list : 
     
        print "Rename variables : in %s, %s, %s %s" % (nChan, nTag, ptv, reg)
       
        original  = "13TeV_%s_%s_%s_%s_%s.root"  % (nChan, nTag, ptv, reg, var_org)
        target    = "13TeV_%s_%s_%s_%s_%s.root"  % (nChan, nTag, ptv, reg, var_tar)

        if version is not 'None':
          original  = 'inputs/'+version+'/'+original
          target    = 'inputs/'+version+'/'+target
     
        if os.path.isfile(original):
          os.system("mv "+original+" "+target)
        else :
          U.Error("%s is missing..." % (original))

if __name__ == "__main__":
  if len(sys.argv) is not 2:
    print "You are now running the RenameVar.py for the current inputs"
    print "TO run for the inputs in inputs/version"
    print "Usage: RenameVar.py <version>"
    version = 'None'
  else:
    version = sys.argv[1]
    
  
  var_org   = 'mva28' # original name of the variable in the split inputs
  var_tar   = 'mva'   # renamed variable
  regions  = [ "SR" ]
  for reg in regions :
    main(var_org,var_tar,version)
    main('mvadiboson28','mvadiboson',version)
