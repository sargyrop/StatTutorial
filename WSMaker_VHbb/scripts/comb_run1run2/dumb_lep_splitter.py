#!/usr/bin/env python
from ROOT import *
from commands import *
from collections import OrderedDict as odict
import time,sys,math,socket,os,copy
import matrix_manipulator as mm

"""
Notes: You have a RooCategory called "master_measurement" in Run1
    ---it has "[SnapShot_ExtRefClone]" but otherwise is identical modulo regions
    ---master_measurement shows up in the "OBSERVABLES" vomit at end instead of beginning
"""

def skip_this_np(name,blacklist):
    for thing in blacklist:
        if thing in name:
            #print '{} is in {} ~~~~~~~'.format(thing,name)
            return True
    return False
nlep=sys.argv[1] #the default put some blah blah after to make it run1
bad0,bad1,bad2=['_L0','ZeroLepton','0Lep','J0L','jet0L'],['_L1','OneLepton','1Lep','J1L','jet1L'],['_L2','TwoLepton','2Lep','J2L','jet2L']
black_list=bad0+bad1
new_pois=['SigXsecOverSM_L2']
if nlep=='1':
    black_list=bad0+bad2
    new_pois=['SigXsecOverSM_L1']
elif nlep=='0':
    black_list=bad1+bad2
    new_pois=['SigXsecOverSM_L0']
# because of signs, some b-tagging NP's need to be flipped
# signs of ttbar normalization changes for leading NPs for 7,8,13 TeV: B: -+-; C:*+-; L: +--
# flip 8 TeV BC, 7 TeV L
# right now, we are only concerned with flipping the leading B
btag=True
debug=False
datdir = '/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct' if 'harvard' in socket.gethostname() else '/afs/cern.ch/work/s/stchan/public/hcomb'
f=TFile('{0}/workspaces/Run1WS/combined/VH3poi_MVA.root'.format(datdir))
ws      = f.Get("combined")

data    = ws.data("combData")
mc      = ws.obj("ModelConfig");
pois    = mc.GetParametersOfInterest()
np      = mc.GetNuisanceParameters()
globObs = mc.GetGlobalObservables()
Obs     = mc.GetObservables()
PDF     = mc.GetPdf()
#PDF.Print("v")
#print PDF.GetName(), PDF.GetTitle(), PDF.ClassName()

#---------
#Creating the new set of NP, Observables and Global Observables
np_new=RooArgSet()
Obs_new=RooArgSet()
globObs_new=RooArgSet()
pois_new=RooArgSet()
#---------

#---------
ws_new=RooWorkspace("Combined");
ws_new.autoImportClassCode(1);

ras=data.get()
nras=RooArgSet("fine")
yokay=ras.createIterator()
fine=yokay.Next()
while fine:
    nom=fine.GetName()
    if not 'RooCategory' in fine.ClassName() and not skip_this_np(nom,black_list):
        print nom,fine.ClassName()
        nras.add(fine)
    fine=yokay.Next()

#---------
#Preparing the new RooCategory, incordpotaring all the categories in the original PDF
roocatname = mm.ws_roocat(False)
Cat=RooCategory(roocatname,roocatname)
iterPDF=PDF.serverIterator();
iPDF=iterPDF.Next()
kittens=[]

while iPDF:#Loop over the categories
    if "RooCategory" not in iPDF.ClassName() and not skip_this_np(iPDF.GetName(),black_list):
        cat=mm.catname(iPDF.GetName())
        Cat.defineType(cat.Data())
        kittens.append(str(cat))
    iPDF=iterPDF.Next()
#print '\n'.join(list(str(s) for s in kittens))
#---------
nras.add(Cat)
newdat=RooDataSet("obsData","obsData",data,nras)

#---------
#Creating the new PDF
TotalPDF=RooSimultaneous("simPDF","",Cat);
#---------


iterPDF=PDF.serverIterator();
iPDF=iterPDF.Next()
categories={}
while iPDF:#Loop over the categories
    cat=iPDF.GetName()
    if mm.catname(cat) in kittens:
        print 'Working on {0}...'.format(cat)
        newlist=RooArgList()
        subpdf=iPDF.pdfList().createIterator()
        isubpdf=subpdf.Next()
        while isubpdf: #Loop over the PDF blocks for each categories
            if not skip_this_np(isubpdf.GetName(),black_list): 
                newlist.add(isubpdf)
            else:
                print 'And we SKIPPEd....',isubpdf.GetName()
            isubpdf=subpdf.Next()
        name=TString(cat+"_new")
        title=TString()
        title.Form(iPDF.GetTitle())
        title=title+TString("_new")
        cat=mm.catname(cat)
        categories[cat]=RooProdPdf(name.Data(),title.Data(),newlist) ;
    iPDF=iterPDF.Next()

for c in categories.keys():
    TotalPDF.addPdf(categories[c],c.Data())

iter_vars=np.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    if not skip_this_np(var_vars.GetName(),black_list):
        np_new.add(var_vars)
    var_vars = iter_vars.Next()
getattr(ws_new,'import')(TotalPDF);
getattr(ws_new,'import')(newdat);

#normalizations
iter_vars=globObs.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    if not skip_this_np(var_vars.GetName(),black_list):
        globObs_new.add(var_vars)
    var_vars = iter_vars.Next()

#the actual fit distributions (mBB/MVA)
iter_vars=Obs.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    #print var_vars.GetName()
    if not skip_this_np(var_vars.GetName(),black_list):
        Obs_new.add(var_vars)
    var_vars = iter_vars.Next()

iter_vars= pois.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    nom = var_vars.GetName()
    if nom in new_pois:
        print 'We found',nom
        pois_new.add(var_vars)
    var_vars = iter_vars.Next()


mc_new =RooStats.ModelConfig("ModelConfiG", ws_new);
mc_new.SetPdf(ws_new.obj("simPDF"));
mc_new.SetParametersOfInterest(pois_new);
mc_new.SetNuisanceParameters(np_new);
mc_new.SetObservables(Obs_new);
mc_new.SetGlobalObservables(globObs_new);
getattr(ws_new,'import')(mc_new);


print '-------------------POIS-------------------'
pois_new.Print("v")
print '-------------------NUISANCE PARAMETERS-------------------'
np_new.Print("v");
print '-------------------OBSERVABLES-------------------'
Obs_new.Print("v");
print '-------------------GLOBAL OBSERVABLES-------------------'
globObs_new.Print("v");
ws_new.SaveAs('{0}/workspaces/Run1WS/combined/copy{1}.root'.format(datdir,nlep))
