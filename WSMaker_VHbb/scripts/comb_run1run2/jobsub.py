import os,sys,subprocess,socket
harvard='harvard' in socket.gethostname()
debug=len(sys.argv)==2

def submit_lsf_job(exec_sequence,runfile,queue='serial_requeue'if harvard else '1nh',exec_dir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/' if harvard else '/afs/cern.ch/work/s/stchan/vhbb/combtools/wsm/',mem='16384',lcl=False):
    script=open(runfile,'w')
    script.write("#!/bin/bash\n")
    script.write("echo START {} at $(date)\n".format(runfile))
    script.write("START=$(date +%s)\n")
    script.write("cd {}\n".format(exec_dir))
    script.write("source {}hsetup.sh\n".format(exec_dir))
    script.write("{}\n".format(' '.join(exec_sequence)))
    script.write("echo FINISH {} at $(date)\n".format(runfile))
    script.write("END=$(date +%s)\n")
    script.write("DIFF=$(( $END - $START ))\n")
    script.write("echo \"Run time in seconds : ${DIFF}\"\n")
    script.close()
    logfile=runfile.replace('scripts','logs').replace('.sh','.log')
    if lcl:
        cmd=['sh',runfile]
    else:
        cmd=["sbatch","--mem",mem,"--time","7-0:0:0","-p",queue,'-e',logfile,'-o',logfile,runfile] if harvard else ['bsub','-q',queue,'sh',runfile]
    if debug:
        print ' '.join(cmd)
        print runfile
        return
    subcmd = subprocess.Popen(cmd)
    subcmd.wait()
    #os.remove(runfile)

def cmd(corrtype,mu,fullcorr,iscond,r2pth='',exec_dir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/'if harvard else'/afs/cern.ch/work/s/stchan/vhbb/combtools/wsm/',r1pth='',r1obs=False):
    cmd = ['python','scripts/run12-combtool.py','-c',corrtype]
    if mu>=0: cmd.append('-u '+str(mu))
    if r1obs: cmd.append('-o')
    if fullcorr: cmd.append('-f')
    if iscond: cmd.append('--cond')
    if r2pth!='': cmd.append( '-r '+r2pth)
    if r1pth!='': cmd.append( '-1 '+r1pth)
    scrnm = '{4}/sub_scripts/{0}{1}{2}{3}.sh'.format(corrtype,mu,str(int(fullcorr)),str(int(iscond)),exec_dir)
    return cmd,scrnm

#for c in ['jes','poi-comp','poi-whzh','poi-012lep']:#list('poi-{0}lep'.format(s) for s in range(3)):#[]:,,'btag-b','jesu']:#,'jesu']:#'jes','jes8']: #'btag-b','btag-bl','btag-c','btag-l','btag']:#['jes','btag','normal','jes8']:
for c in ['btag-b','jesu']:#,'jesu']:#'jes','jes8']: #'btag-b','btag-bl','btag-c','btag-l','btag']:#['jes','btag','normal','jes8']:
    for f in [False]:# if c=='normal' else [True,False]: #full or partial correlation
        for mu in [-0.25]:#,0.,0.2,0.5]:#,1]:
            for cnd in [True]:#False] if mu<0 else [True,False]:
                one,two=cmd(c,mu,f,cnd,r2pth='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/SMVHVZ_LHCP17_MVA_v05/combined/125_wAsimov.root',r1pth='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/Run1WS/combined/125_asimovData_cnd1_0.51.root',r1obs=mu<0)
                one,two=cmd(c,mu,f,cnd,r2pth='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/SMVHVZ_LHCP17_MVA_v05/combined/125.root',r1pth='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/Run1WS/combined/125.root',r1obs=mu<0)
                submit_lsf_job(one,two,queue='pleiades')#'serial_requeue')
