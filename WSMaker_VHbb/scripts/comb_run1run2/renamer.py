from ROOT import *
from commands import *
import time,sys,socket
import matrix_manipulator as mm

isrun2=len(sys.argv)==1 #the default put some blah blah after to make it run1
btag=True
wstag = 'Run2WS-20170421' if isrun2 else 'Run1WS'
wstag = 'SMVHVZ_LHCP17_MVA_v05' if isrun2 else 'Run1WS'
datdir = '/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct' if 'harvard' in socket.gethostname() else '/afs/cern.ch/work/s/stchan/public/hcomb'

stupid_tag=''
if not isrun2:
    if sys.argv[1] in ['0','1','2']:
        stupid_tag=sys.argv[1] 
f=TFile('{1}/workspaces/{0}/combined/copy{2}.root'.format(wstag,datdir,stupid_tag))
ws      = f.Get("Combined")
data    = ws.data(mm.ws_datname(isrun2) if stupid_tag=='' else 'obsData')
mc      = ws.obj("ModelConfiG");
pois    = mc.GetParametersOfInterest()
np      = mc.GetNuisanceParameters()
globObs = mc.GetGlobalObservables()
Obs     = mc.GetObservables()
PDF     = mc.GetPdf()

ws_new=RooWorkspace("combined");
ws_new.autoImportClassCode(1);
getattr(ws_new,'import')(PDF);
getattr(ws_new,'import')(data);
mc_new =RooStats.ModelConfig("ModelConfig", ws_new);
rename  = ws_new.obj("simPDF")
rename.SetName(mm.ws_pdf(isrun2))
mc_new.SetPdf(rename);
mc_new.SetParametersOfInterest(pois);
mc_new.SetNuisanceParameters(np);
mc_new.SetObservables(Obs);
mc_new.SetGlobalObservables(globObs);
getattr(ws_new,'import')(mc_new);

rename.Print("v")
pois.Print("v")
globObs.Print("v");
if not isrun2:
    if sys.argv[1]=='2':
        ws_new.SaveAs('{1}/workspaces/{0}/combined/2lep.root'.format(wstag,datdir))
    elif sys.argv[1]=='1':
        ws_new.SaveAs('{1}/workspaces/{0}/combined/1lep.root'.format(wstag,datdir))
    elif sys.argv[1]=='0':
        ws_new.SaveAs('{1}/workspaces/{0}/combined/0lep.root'.format(wstag,datdir))
    else:
        ws_new.SaveAs('{1}/workspaces/{0}/combined/{2}-unfold.root'.format(wstag,datdir,'btag' if btag else 'jes'))
else:
    ws_new.SaveAs('{1}/workspaces/{0}/combined/{2}-unfold.root'.format(wstag,datdir,'btag' if btag else 'jes'))
