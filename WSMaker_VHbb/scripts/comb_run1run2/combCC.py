#!/usr/bin/env python

import AnalysisMgr_VHbbRun2 as Mgr
import mergeBreakdown as Breakdown
import BatchMgr as Batch
import glob,subprocess,socket,sys,os,argparse,ROOT

harvard = 'harvard' in socket.gethostname()
printonly=False
reboot_stragglers=True
do_local=False
# injected mu values
mus=[0,0.25,0.5,0.75,1,1.25,1.5,-0.25]
mus=[0,0.2,0.5,1,-0.25]
mus=[0,0.2,0.5,1]

# jes/btag: doExp ONE; except for jes FULL
# pull comp: zero corr, JES+mu, 2xbtag
# correlation schemes
types={'combined':{'normal':mus,'btag':[-0.25,1],'jes':[-0.25,1]},'run1':{'normal':[-0.25]},'ichep':{'normal':mus}}
types={'combined':{'normal':mus,'btag':[1],'jes':[1]},'run1':{'normal':[-0.25]},'ichep':{'normal':mus}}
types={'combined':{'normal':[1],'poi-comp':[1],'btag':[1],'btag-b':[1],'btag-c':[1],'btag-l':[1],'btag-bl':[1],'jes':[1],'jes2l':[1],'jes8':[1],'jesu':[1]},'run1':{'normal':[-0.25]},'ichep':{'normal':[-0.25]},'run2':{'normal':[1]},'test':{'normal':[1]},'x':{'normal':[-0.25]}}
types={'combined':{'normal':[1],'poi-comp':[1],'poi-012lep':[1],'poi-whzh':[1],'btag-b':[1],'jes':[1],'jesu':[1],'db':[-0.25],'db_r12poi':[-0.25]},'run1':{'normal':[-0.25]},'ichep':{'normal':[-0.25]},'run2':{'normal':[1]},'test':{'normal':[1]},'x':{'normal':[-0.25]}}
types={'combined':{'normal':[1],'poi-comp':[1],'poi-012lep':[1],'poi-whzh':[1],'btag-b':[1],'jes':[1],'jesu':[1]},'run1':{'normal':[-0.25]},'ichep':{'normal':[-0.25]},'run2':{'normal':[1]},'test':{'normal':[1]},'x':{'normal':[-0.25]}}
types['combined'].update({'poi-2lep':[1],'poi-1lep':[1],'poi-0lep':[1],'btag-nf':[1]})
#types={'normal':[1]}
runReduced = False
breakdown_opt='-1'  # 1 is default; 9 is for combined; 10 is the Run1/Run2 specific breakdowns; 11 is jets; 12 is b-tagging; 13 is modeling 
bdresult_home=('/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/20161115/bd-results/' if harvard else '/afs/cern.ch/work/s/stchan/vhbb/maker_ws/20161110/bd-results/')
bdresult_home=os.getenv('PWD')+'/bd-results/'
if not os.path.exists(bdresult_home):
    os.makedirs(bdresult_home)
bd_hash = {'combined':bdresult_home+'run1-{0}-results.root','ichep':bdresult_home+'ichep-{0}-results.root','run1':bdresult_home+'run1-results.root','run2':bdresult_home+'run2-results.root','test':bdresult_home+'jes2-results.root','x':bdresult_home+'jes1-results.root'}
wslink_home=('/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/'if harvard else '/afs/cern.ch/work/s/stchan/public/hcomb/workspaces/')
wslink_hash = {'combined':wslink_home+'Run1Run2Comb/combined/125-{0}.root','ichep':wslink_home+'ICHEP2016WS/combined/125_wAsimov.root','run1':wslink_home+'Run1WS/combined/125.root','run2':wslink_home+'SMVHVZ_LHCP17_MVA_v06/combined/125.root','test':wslink_home+'SMVHVZ_LHCP17_MVA_v05/combined/jes-unfold.root','x':wslink_home+'Run1WS/combined/jes-unfold.root'}
obsDataName = "combData"

def is_number(string):
    try:
        float(string)
        return True
    except ValueError as yup:
        return False

def is_legacy(outv):
    # for outversions corresponding to dates later than 2 June, use the new convention
    # at a later date this may trivially return False
    check = 997
    if is_number(outv[:4]):
        check = float(outv[:4])
    elif is_number(outv[:3]):
        check = float(outv[:3])
    return check < 602

def asimovDataName(mu_asimov=1,isConditional=True,isr2=True):
    # mu values of less than 0 represent (for now anyway) not doing Asimov
    if mu_asimov<0:
        return 'obsData' if isr2 else 'combData'
    number = str(int(mu_asimov)) if mu_asimov%1==0  else str(mu_asimov).rstrip('0')
    return 'asimovData_cnd{0}_{1}'.format(int(isConditional),number)

def input_version(mu_asimov,corrType,full_corr,isConditional=True,runtype='combined',mur1=0.51,condr1=True,outv='999'):
    # the extra verbiage and outputversion check is here to maintain legacy running.
    # I typically use output versions of the form "date" + some identifier
    # I realize this becomes oblescent when dates start having four digits, but all relevant studies should be redone by then
    # and I will phase the check out when everything has been updated
    if 'db' in corrType:
        return corrType
    if runtype=='ichep':
        return 'ichep'
    elif runtype in ['run1','x']:
        if is_legacy(outv):
            return 'run1'
        else:
            return '{1}_{0}'.format(asimovDataName(mur1,condr1,isr2=False),'r1' if runtype=='run1' else runtype)
    elif runtype in ['run2','test']:
        if is_legacy(outv):
            return 'run2'
        else:
            return '{1}_{0}'.format(asimovDataName(mu_asimov,condr1,isr2=True),'r2' if runtype=='run2' else runtype)
    return "r1_{0}-r2_{1}-{2}_{3}".format(asimovDataName(mur1,condr1,False),asimovDataName(mu_asimov,isConditional),corrType,'full' if full_corr else 'partial')

def wslink_bdname(inputv,runtype,outv='996'):
    wsname = wslink_hash[runtype].format(inputv) if runtype == 'combined' else wslink_hash[runtype]
    bdresult = bdresult_home+'{0}-results.root'.format(inputv)
    if is_legacy(outv):
        bdresult = bd_hash[runtype] if runtype == 'run1' else bd_hash[runtype].format(inputv)
    return wsname,bdresult

def add_config(inputv, mass='125', syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, use4pjet=False, doptvbin=True):
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, InputVersion=inputv, MassPoint=mass, DoInjection=0, SkipUnkownSysts=True)

    if "0" in channel:
        conf.append_regions(var2tag, var1tag, True, False, False,
                            use1tag, use4pjet, doptvbin)
    if "1" in channel:
        conf.append_regions(var2tag, var1tag, False, True, False,
                            use1tag, use4pjet, doptvbin)
    if "2" in channel:
        conf.append_regions(var2tag, var1tag, False, False, True,
                            use1tag, use4pjet, doptvbin)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag
    #COMBINATION (i.e. WSlink) ONLY
    fullversion =  'Run1Run2Comb_'+inputv+'.'+outversion
    return conf.write_configs(fullversion)

def define_algorithms(inv,out,runtype,pull,rank,reduced,limit,sig,breakdown,lcl=False,bdmode=-2,doobs=False,silverbullet=False):
    if silverbullet:
        doobs,lcl,bdmode=True,False,2
        if 'jes_partial' in inv:
            pull,limit,sig,breakdown=True,True,True,True 
        elif 'poi-012lep' in inv or 'poi-whzh' in inv or 'poi-comp' in inv: #wh/zh, 0/1/2 lep, r1/r2 pois
            pull,limit,sig,breakdown=False,False,False,True 
        elif 'poi-' in inv and 'lep' in inv: #2 or 3 poi fits
            pull,limit,sig,breakdown=False,True,True,False
        else:
            print 'You are trying to run an weird outputversion in catch all mode. Fix this',inv
            sys.exit()
    dset = 'combData'
    if doobs:
        if runtype in ['ichep','run2','test']:
            dset = 'obsData'
    else:
        if runtype in ['ichep','run2','test','run1','x']:
            if is_legacy(out):
                start = inv.find('r1_' if runtype in ['run1','x'] else 'r2_')+3
                dset = inv[start:inv.find('-',start)]
            else:
                dset = inv[inv.find('_')+1:]
    algos=[]
    # the config file is a dummy that allows us to use doActions.py and gives a nice input/output naming convention for the workspace link
    # should make names short and useful
    wsname,bd_results=wslink_bdname(inv,runtype,out)
    wsAlg,configfiles=['-w','-k',wsname],add_config(inv)
    
    # remember, doExp is: "0" to run observed, "1" to run expected only
    # if we're using injected Asimov data for Run2, no need to do Asimov for Run1 is well
    # "observed" will be the Run1 data + Run2 Asimov
    doExp = '0' if (runtype in ['ichep','run1','x'] or ('r1_as' in inv and 'r2_as' in inv) or doobs) else '1'#'asimovData' in inv else '1' 
    
    nums='5,7,9'
    if doExp=='0':
        nums='2,5'#,7,9'
    if pull:
        #algos.append(['_fullRes',wsAlg+["--fcc", nums+"@{MassPoint}@"+dset, "-m", nums, "-p", "0,3@{MassPoint}", "-t", "0,1@{MassPoint}"],configfiles])
        algos.append(['_fullRes',wsAlg+["--fcc", nums+"@{MassPoint}@"+dset, "-m", nums],configfiles])
        #algos.append(['_fullRes',wsAlg+["--fcc", nums+"@{MassPoint}@"+dset],configfiles])
            
    if rank:# or big_switch:
        algos.append(['_rank',wsAlg+['-r','{{MassPoint}},{0}'.format(dset)],configfiles])

    if reduced:
        algos.append(["_fullRes",wsAlg+["-m", nums],configfiles])

    if limit and sig and not harvard:
        algos.append(["_limits",wsAlg+["-l", doExp+",{MassPoint}@"+dset,"-s", doExp+",{MassPoint}@"+dset],configfiles])
    else:
        if limit:
            algos.append(["_limits",wsAlg+["-l", doExp+",{MassPoint}@"+dset],configfiles])
        if sig:
            algos.append(["_sigs" if harvard else "_limits",wsAlg+["-s", doExp+",{MassPoint}@"+dset],configfiles]) #lxplus getResults.py fetching requires us to follow launch_default_jobs syntax

    if breakdown:
        bdopt,njobs,bd_add=bdjobs(bdmode,bd_results,inv,lcl or silverbullet)
        algos.append(["_breakdown",wsAlg+["-u", doExp+",{MassPoint}@"+dset+","+bd_add],configfiles,njobs])
    return algos

def in_versions(types,mur1=0.51,condr1=True,allobs=False,runv='combined',outv='998'):
    inputvs=[]
    fulls={'normal':[True],'poi-comp':[True,False],'btag-b':[False],'btag-c':[False],'btag-l':[False],'btag-bl':[False],'btag':[True,False],'jes':[True,False],'jes8':[True],'jes2l':[True],'jesu':[True,False]}
    fulls={'normal':[True],'poi-comp':[False],'poi-012lep':[False,True],'poi-whzh':[False],'btag-b':[False],'btag-c':[False],'btag-l':[False],'btag-bl':[False],'btag':[True,False],'jes':[False],'jes8':[True],'jes2l':[True],'jesu':[False],'db':[True],'db_r12poi':[True]}
    fulls.update({'poi-2lep':[False],'poi-1lep':[False],'poi-0lep':[False],'btag-nf':[False]})
    for t in types:
        for tf in fulls[t]:
            #for c in [True]: #[True] if '-' in mu else [True,False]: # don't bother with conditional for the standard; just always do the conditional
            if allobs:
                # if we are doing both things with obsData, no need to cycle through mu's just do the single case of mu pairs that matters
                inputvs.append(input_version(-0.25,t,tf,mur1=-0.25,runtype=runv,outv=outv))
            else:
                for mu in types[t]:
                    inputvs.append(input_version(mu,t,tf,mur1=mur1,condr1=condr1,runtype=runv,outv=outv))
    return inputvs

def launch_alg(local,hurc,alg,queue='',stag='',smart_launch=True,ram='',nj=-1,debug=False):
    if local:
        print 'Locally run {0}'.format(' '.join(alg[1]))
        if not debug:
            Batch.run_local_batch(alg[2], outversion+alg[0],alg[1])
    elif not hurc:
        njobs=nj if nj>0 else 1
        if 'rank' in alg[0]: 
            njobs=10#alg[3]
        elif 'breakdown' in alg[0]: 
            njobs=alg[3]
        njlaunch = nj if nj>0 else njobs
        banana=('2nd'if queue=='' else queue)
        print '{3} in {0} with {1} jobs with {2}'.format(banana,njlaunch,' '.join(alg[1]),'Would have launched' if debug else 'Launching')
        if not debug:
            Batch.run_lxplus_batch(alg[2],outversion+alg[0],alg[1],queue=banana,jobs=njlaunch)
    else:
        partition,mem,njobs='serial_requeue','32000',1 #limits the wait;
        if 'rank' in alg[0]:
            njobs,mem=100,'32000'
        # pulls are expensive; put them on atlast3 nodes
        elif 'fullRes' in alg[0]:
            mem='96000'
        # for some reason, significances and limits are expensive
        elif 'limits' in alg[0] or 'sigs' in alg[0]:
            mem='96000'
        elif 'breakdown' in alg[0]: 
            njobs=alg[3]
        njlaunch = nj if nj>0 else njobs
        spoon=(mem if ram=='' else ram)
        toobig=(partition if queue=='' else queue)
        print '{4} in {0} with {1} jobs with {2} mem for {3}'.format(toobig,njlaunch,spoon,' '.join(alg[1]),'Would have launched' if debug else 'Launching')
        if not debug:
            Batch.run_slurm_batch(alg[2], outversion+alg[0],alg[1],queue=toobig,jobs=njlaunch,mem=spoon,only_redo=smart_launch,stag=stag)

def results_printout(outv,results,runtype,topdir='./'):
    dir=topdir+'results-summaries'
    if not os.path.exists(dir):
        os.makedirs(dir)
    filename='{2}/{0}_{1}-results.txt'.format(runtype,outv,dir)
    write = file(filename,'w')
    for inv_outv in results:
        for kind in results[inv_outv]:
            write.write(runtype+'-'+inv_outv+'-'+kind+' '+' '.join(results[inv_outv][kind])+'\n')
    write.close()
    print 'Wrote results to ',filename
    return

def result_exists(alg,results,inv,outv):
    variety=inv+'.'+outv
    if not variety in results:
        return False

    # limits
    if '-l' in alg[1]:
        return 'limit-exp' in results[variety] or 'limit-obs' in results[variety] 

    # significances
    if '-s' in alg[1]:
        return 'sig-exp' in results[variety] or 'sig-obs' in results[variety] 

    # pulls
    if '--fcc' in alg[1]:
        return 'pulls' in results[variety]

    # breakdown
    if '-u' in alg[1]:
        return 'breakdown' in results[variety]

    # ranks
    if '-r' in alg[1]:
        return 'ranks' in results[variety]

    return False # safe enough default

def find_ranks(workspace,formats=['eps','pdf','png']):
    plots=[workspace]
    for end in formats:
        for p in ['_pulls_prefit_125','_pulls_125']:
            plot='./{0}-files/{1}.{0}'.format(end,workspace+p)
            if os.path.exists(plot):
                plots.append(plot)
    return plots

def gather_results(outv,invs,dontget=False,queue='serial_requeue',lxget=False):
    # so we weant to see where results exist
    results_base='/afs/cern.ch/work/s/stchan/analysis/statistics/batch/'
    forelle={inv+'.'+outv:{} for inv in invs} #dictionary that stores what results exist for different inputversion's for a given outversion

    # beginning with the lxbatch stuff; results first need to be fetched
    if not harvard and lxget: 
        for muntern in forelle:
            if not os.path.exists(results_base+muntern):
                print 'Cannot find lxbatch directory for ',muntern
            else:
                subprocess.Popen(['python','scripts/getResults.py','get',muntern]).wait()

    # (attempt to) extract results
    type_hash={'Observed':'obs','Expected':'exp','Injected':'inj'}
    for muntern in forelle:
        #limits and significances can be expected or observed
        for type in ['exp','obs']:
            # fetch limits results
            # pattern looks like root-files/inv.outv_(wsname part = limits_Run1Run2Comb_outv)_(exp|obs)/limit_125.txt
            lfile='./root-files/{0}_limits_Run1Run2Comb_{0}_{1}/limit_125.txt'.format(muntern,type)
            if os.path.exists(lfile):
                cup = file(lfile,'r')
                buffer = cup.read().split('\n')
                # files look like ...Expected limit: 0.597613759567 +0.234091692246 -0.166999954263... on lines
                # find the right line (ignore the ones with _ because these are tex formatted with low precision)
                for l in buffer:
                    words = l.split(' ')
                    typ=words[0]
                    if len(words)>0:
                        if typ in type_hash:
                            key='limit-'+type_hash[typ]
                            if not key in forelle[muntern] and words[2]!='0.0' and words[2]!='0.00' and '_' not in l:
                                forelle[muntern][key]=words[2:]
                cup.close()

            # and now for significances
            # pattern looks like root-files/inv.outv_(wsname part = (limits|sigs)_Run1Run2Comb_outv)_(exp|obs)_p0/125.root
            # bin contents are (observed,expected,injected) x (sig,p0)
            sfile='./root-files/{0}_limits_Run1Run2Comb_{0}_{1}_p0/125.root'.format(muntern,type)
            if not os.path.exists(sfile): sfile=sfile.replace('limits','sigs')
            if os.path.exists(sfile):
                bucket=ROOT.TFile(sfile)
                hist=bucket.Get("hypo")
                if hist.GetEntries()==6: #should always be six
                    forelle[muntern]['sig-'+type]=list(str(hist.GetBinContent(i+1)) for i in range(6))

        # and now for pull plots
        # these are stored plots/inv.outv_(wsname part = fullRes_Run1Run2Comb_outv)/fcc/
        # the FCC lives in fcc/FitCrossChecks_inv.outv_(wsname part = fullRes_Run1Run2Comb_outv)_combined/FitCrossChecks.root
        pullws='{0}_fullRes_Run1Run2Comb_{0}'.format(muntern)
        pulldir,fccfile='./plots/{0}/fcc'.format(pullws),'./fccs/FitCrossChecks_{0}_combined/FitCrossChecks.root'.format(pullws)
        pulls_done,good=[],False
        if os.path.exists(fccfile):
            pulls_done.append(pullws)
            good=True
        if os.path.exists(pulldir):
            for kind in os.listdir(pulldir):
                if os.path.exists(pulldir+'/'+kind+'/NP_all.pdf'): pulls_done.append(kind)
            if len(pulls_done)>0: 
                # if there's stuff, make the first entry the name of the WS for comparePulls purposes
                good=True
        if good:
            forelle[muntern]['pulls']=pulls_done

        # and the ranks
        rankws = '{0}_rank_Run1Run2Comb_{0}'.format(muntern)
        if os.path.exists('./root-files/{0}_breakdown_add/total.root'.format(rankws)):
            if dontget:
                forelle[muntern]['ranks']=find_ranks(rankws)
            else:
                log=file('./logs/output_{0}_rankPlot.log'.format(rankws),'w')
                npprint=subprocess.Popen(['python','scripts/makeNPrankPlots.py',rankws],stdout=log)
                npprint.wait()
                log.close()
        else:
            # job divided; see if we have the raw materials to remake the ranking plots
            rankwsfront,rankwsback,njobs,search_pattern = '{0}_rank_job'.format(muntern),'_Run1Run2Comb_{0}_breakdown_add'.format(muntern),-1,'{2}_rank_job{0}of{1}_Run1Run2Comb_{2}'
            for dir in os.listdir('./root-files'):
                if rankwsfront in dir and rankwsback in dir:
                    start0=dir.find('_job')
                    start1=dir.find('_',start0+1)
                    if start0==dir.rfind('_job'):
                        njobs = int(dir[dir.find('of',start0)+2:start1])
                        break
            if njobs>0:
                finalws=search_pattern.format(njobs,njobs,muntern)
                if dontget:
                    forelle[muntern]['ranks']=find_ranks(finalws)
                else:
                    allclear,unfinished=True,0
                    # check each of the jobs to see if they're okay
                    for i in range(njobs):
                        fischer=search_pattern.format(i,njobs,muntern)
                        if not os.path.exists('./root-files/{0}_breakdown_add/total.root'.format(fischer)):
                            allclear=False
                            # try to find a log--looks like output_r2-asimovData_cnd1_1-normal_full.223alpha_rank_job0of10_Run1Run2Comb_r2-asimovData_cnd1_1-normal_full.223alpha_job0of10.log 
                            # script name is like  slurm_scripts/run-613alpha_rank_job41of100_Run1Run2Comb_r1_combData-r2_obsData-jes_partial.613alpha-r-{MassPoint},combData.sh
                            log = './slurm_logs/output_{0}_job{1}of{2}.log'.format(fischer,i,njobs)
                            coop=glob.glob('./slurm_scripts/*{0}*.sh'.format(log[log[1:].find('.')+2:log[:-4].rfind('.')]))
                            pat = ''
                            if len(coop)==1:
                                pat=coop[0]
                            if Breakdown.check_log(log,True,queue='serial_requeue' if queue=='' else queue,script_name=pat):
                                unfinished+=1
                    if unfinished>0:
                        print unfinished,'jobs still running for ranking: ',fischer
                    # if so, make the plots
                    if allclear:
                        logname='./logs/output_{0}_rankPlot.log'.format(finalws)
                        log=file(logname,'w')
                        npprint=subprocess.Popen(['python','scripts/makeNPrankPlots.py',finalws],stdout=log)
                        npprint.wait()
                        print 'Ranking plotting log printed to',logname
                        log.close()
                        forelle[muntern]['ranks']=find_ranks(finalws)
            # if raw materials aren't there, maybe we just copied the plots over for consolidation in slides
            else:
                for dir in os.listdir('./pdf-files'):
                    if rankwsfront in dir:
                        start0=dir.find('_job')
                        start1=dir.find('_',start0+1)
                        if start0==dir.rfind('_job'):
                            njobs = int(dir[dir.find('of',start0)+2:start1])
                if njobs>0:
                    finalws=search_pattern.format(njobs,njobs,muntern)
                    forelle[muntern]['ranks']=find_ranks(finalws)

        # now the breakdowns
        workspace='{0}_breakdown_Run1Run2Comb_{0}'.format(muntern)
        herald=scan_bddir(workspace)
        if herald != ['none']:
            forelle[muntern]['breakdown']=herald

    return forelle

def bd_mode_precedence(workspace):
    return [20,9,21,12,22,14,23,24,2,-1] if 'btag' in workspace else [20,9,22,14,21,12,23,24,2,-1]

def bd_type_precedence():
    return ['','_13TeV','_78TeV','_L0','_L1','_L2','WH','ZH','_L0_78TeV','_L1_78TeV','_L2_78TeV','_L0_13TeV','_L1_13TeV','_L2_13TeV']

def scan_bddir(workspace,favor_obs=True):
    mode_priorities,asimov_priorities,txt,tex=bd_mode_precedence(workspace),[0,1] if favor_obs else [1,0],'',''
    mode_priorities+=list(set(range(25)).difference(mode_priorities))
    bddir='./plots/{0}/breakdown/'.format(workspace) # pattern for local/non-parallel running
    possibilities=[]
    if os.path.exists(bddir):
        files=os.listdir(bddir)
        # muhat table files look like: [mode,Asimov,format]
        muhat_pattern='muHatTable_'+workspace+'_mode{0}_Asimov{1}_SigXsecOverSM{3}.{2}'
        for m in mode_priorities:
            for a in asimov_priorities: 
                for t in bd_type_precedence():
                    slides,human=muhat_pattern.format(m,a,'tex',t),muhat_pattern.format(m,a,'txt',t)
                    if slides in files:
                        if human in files:
                            return bddir+human,bddir+slides
                        else:
                            print 'You have a good tex',slides,'but no txt counterpart...check that out'

    return Breakdown.fetch_and_merge('.',workspace)

def bdjobs(bd_opt,bd_results,input,lcl=False):
    breakdown_opt,bd_add=bd_opt,''
    if bd_results!='':
        if not os.path.exists(bd_results) and not lcl:
            breakdown_opt = '-1' # if the overhead file doesn't exist, make it
        elif bd_opt=='99': #auto
            if 'run1-results' in bd_results or 'ichep' in bd_results or 'run2-results' in bd_results: #Run1 or Run2 alone, just do the regular categories, no subdivisions
                breakdown_opt = '9'
            else:
                breakdown_opt = '22' #HACK FIX!
            """
        elif 'normal' in input:
            breakdown_opt = '10' # do the kitchen sink for the normal ones
        elif 'jes' in input:
            breakdown_opt = '11' # just the jet systematics
        elif 'btag' in input:
            breakdown_opt = '12' # b-tagging
            """
        if breakdown_opt != '-1' and not os.path.exists(bd_results):
            print 'File for fetching breakdown results {} does not exist....you might run into trouble'.format(bd_results)
        bd_path = './' if '/' not in bd_results else bd_results[0:bd_results.rfind('/')]
        if not os.path.exists(bd_path):
            os.makedirs(bd_path)
        bd_add = "{},{}".format(breakdown_opt,bd_results)
    njobs=30
    if lcl:
        njobs=1
    elif breakdown_opt=='2':
        njobs=2
    elif breakdown_opt in ['9','20']:
        njobs=23
    elif breakdown_opt in ['10','24']:
        njobs=46
    elif breakdown_opt=='11':
        njobs=3
    elif breakdown_opt in ['12','21']: #just b-tagging (combined, b, c, l)
        njobs=4
    elif breakdown_opt==['13','23']:
        njobs=30
    elif breakdown_opt in ['14','22']:
        njobs=2
    elif breakdown_opt=='-1':
        njobs=1
    return breakdown_opt,njobs,bd_add

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run1+Run2 combination WSMaker job checking/steering')
    parser.add_argument('outversion',help='The output version')
    parser.add_argument('--redo',help="Don't check to see if results exist; just redo everything",action='store_true')
    parser.add_argument('--dontget',help="Don't fetch results but check to see if they exist",action='store_true')
    parser.add_argument('--lxget',help="Fetch results from lxbatch directories",action='store_true')
    parser.add_argument('--debug',help="Print only; don't launch",action='store_true')
    parser.add_argument('--local',help="Run things locally",action='store_true')
    parser.add_argument('--mem',help="Memory allocation for slurm jobs",default='')
    parser.add_argument('--hack',help="Hack--string that must always appear in the input version",default='')
    parser.add_argument('--run1o',help="Run1 observed data",action='store_true')
    parser.add_argument('-o','--obs',help="Observed data for both; make sure you want to unblind",action='store_true')
    parser.add_argument('-q','--queue',help="Which queue to submit jobs?",default='')
    parser.add_argument('-n','--njobs',help="How many jobs?",default='-1')
    parser.add_argument('-p','--pulls',help="Run pulls",action='store_true')
    parser.add_argument('-b','--breakdown',help="Run breakdowns",action='store_true')
    parser.add_argument('-l','--limits',help="Run limits",action='store_true')
    parser.add_argument('-s','--sigs',help="Run significances",action='store_true')
    parser.add_argument('-m','--reduced',help="Run reduced diag plots only (superseded by -p)",action='store_true')
    parser.add_argument('--rank',help="Run ranks",action='store_true')
    parser.add_argument('--silverbullet',help="Launch jobs necessary for plots requested paper",action='store_true')
    parser.add_argument('--bdmode',help="Breakdown mode",default='99')
    parser.add_argument('-r','--run',help="Specify which Run to look at (default = combined)",default='combined')
    args=parser.parse_args()

    outversion = args.outversion
    if args.silverbullet:
        # obviously all -o
        # plots
        # 1. CL's 2. 0,1,2,012 lep p0, limits, SoB 3. muhat plots 4. SoB plot (in theory just need FCC)
        # poi-whzh, poi-012lep: -b --bdmode 2 plot #3
        # poi-[012]lep -s -l plot #2
        # jes_partial: -p -s -l -b --bdmode 2 ingredients for all plots (1-4)
        invs = list(input_version(mu_asimov=-1,corrType=c,full_corr=False,mur1=-0.25) for c in ['jes','poi-comp','poi-whzh','poi-012lep','poi-0lep','poi-1lep','poi-2lep'])
    else:
        invs = in_versions(types[args.run.lower()],mur1=(-0.25 if args.run1o else 0.51),condr1=True,allobs=args.obs,runv=args.run.lower(),outv=outversion)
    ins=[]
    for inputv in invs:
        if not args.hack in inputv:
            print 'Because you said so, will ignore ',inputv
            continue
        ins.append(inputv)
    results,all_done,turds,goods=gather_results(outversion,ins,args.redo or args.dontget,queue=args.queue,lxget=args.lxget),True,[],[]
    

    for inputv in ins:
        algos=define_algorithms(inputv,outversion,args.run.lower(),args.pulls,args.rank,args.reduced,args.limits,args.sigs,args.breakdown,lcl=args.local,bdmode=args.bdmode,doobs=args.obs,silverbullet=args.silverbullet)
        for alg in algos:
            if result_exists(alg,results,inputv,outversion) and not args.redo:
                print 'Result exists; skipping ',inputv+alg[0]
                goods.append(inputv+alg[0])
                continue
            all_done=False
            turds.append(inputv+alg[0])
            launch_alg(args.local,harvard,alg,smart_launch = not args.redo,queue=args.queue,ram=args.mem,debug=args.debug,nj=int(args.njobs))

    if len(turds):
        print 'Results do NOT exist for:' if args.debug else 'Launched jobs for'
        for t in turds:    print t
    else:
        print 'No jobs to launch'
    print 'Results EXIST for:'
    for g in goods: print g

    # if jobs were done locally, gather and update results
    if args.local: results=gather_results(outversion,ins,True)
    results_printout(outversion,results,args.run.lower())
