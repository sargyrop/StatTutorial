#!/usr/bin/env python
import ROOT,sys,os,argparse,socket
import matrix_manipulator as mm
npn='21NP' #19NP
harvard='harvard' in socket.gethostname()
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem
#Figure out how to do linear combinations Maybe use RooFormulaVar's and clone/re-write workspaces as appropriate?


def asimovDataName(mu_asimov=1,isConditional=True):
    # mu values of less than 0 represent (for now anyway) not doing Asimov
    if mu_asimov<0:
        return 'obsData'
    number = str(int(mu_asimov)) if mu_asimov%1==0  else str(mu_asimov).rstrip('0')
    return 'asimovData_cnd{0}_{1}'.format(int(isConditional),number)

def unfolded_jes_nps(strong):
    datdir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/'if harvard else '/afs/cern.ch/work/s/stchan/public/hcomb/'
    fr1map="{0}/ProjectionCoefficientFile_{1}_test_withAmplitudes_April29.root".format(datdir,'Final2012')
    enps_r1=list('alpha_SysJetNP{0}_Y2012_8TeV'.format(i+1) for i in range(4)) #5 isn't in there for some reason even though 6 is the "rest" one

    fr2map="{0}/ProjectionCoefficientFile_{1}_test_withAmplitudes_April29.root".format(datdir,'Moriond2017')
    enps_r2=list('alpha_SysJET_21NP_JET_EffectiveNP_{0}'.format(i+1) for i in range(7))

    nps_r1=mm.parse_JES_map(mapfile=fr1map,effective_nps=enps_r1,qrun2=False)[1]
    nps_r2=mm.parse_JES_map(mapfile=fr2map,effective_nps=enps_r2,qrun2=True)[1]

    tidus=[]
    for one in nps_r1:
        ok=[one]
        for two in nps_r2:
            if mm.ismatch(one,two,noemu=False,jes_strong=strong):
                ok.append(two)
                tidus.append(ok)
                break
    #print '\n'.join(list(' '.join(t) for t in tidus))
    return tidus

#do a CombinationTool job--the Run2 dataset name (to allow for Asimov), correlation scheme ('btag','jes', and uncorr), extent of correlations (full or partial), and Run2 WS path can be specified
def do_combination(dataset13TeV='obsData',dataset78TeV='combData',corrType='normal',full_corr=True,run2path="Run2WS-20170421/combined/125_wAsimov.root",run1path="Run1WS/combined/125_wAsimov.root"): #ICHEP2016WS/combined/
    corrType=corrType.lower()
    fulljes=(full_corr and 'jesu' in corrType) 
    wsdir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/'if harvard else '/afs/cern.ch/work/s/stchan/public/hcomb/workspaces/'
    OutfileName =  "{0}Run1Run2Comb/combined/125-r1_{1}-r2_{2}-{3}_{4}.root".format(wsdir,dataset78TeV,dataset13TeV,corrType,'full' if full_corr else 'partial')
    f78TeVfile = ('' if run1path[0]=='/' else wsdir)+run1path
    f13TeVfile = ('' if run2path[0]=='/' else wsdir)+run2path
    if corrType=='jesu':
        f13TeVfile = f13TeVfile.replace('125_wAsimov','jes-unfold')
        f78TeVfile = f78TeVfile.replace('125_'+dataset78TeV,'jes-unfold')
    if 'btag' in corrType:
        f78TeVfile = f78TeVfile.replace('125_'+dataset78TeV,'btag-unfold')
        
    print 'OutfileName = ', OutfileName
    print 'f78TeVfile = ', f78TeVfile
    print 'f13TeVfile = ', f13TeVfile

    combined = ROOT.CombinedMeasurement("combined_master")
 
    # specify the Run1 measurement; (for now) this won't change
    m78TeV = ROOT.Measurement("78TeV") ##78TeV #carlo
    m78TeV.SetSnapshotName("nominalNuis") #snapshot_paramsVals_initial _GlobalFit
    m78TeV.SetFileName(f78TeVfile)
    m78TeV.SetWorkspaceName("combined")
    m78TeV.SetModelConfigName("ModelConfig")
    m78TeV.SetDataName(dataset78TeV)
    combined.AddMeasurement(m78TeV) #carlo
    
    # and now the Run2
    m13TeV = ROOT.Measurement("13TeV")
    m13TeV.SetSnapshotName("snapshot_paramsVals_initial") #snapshot_paramsVals_initial
    m13TeV.SetFileName(f13TeVfile)
    m13TeV.SetWorkspaceName("combined")
    m13TeV.SetModelConfigName("ModelConfig")
    m13TeV.SetDataName(dataset13TeV)
    combined.AddMeasurement(m13TeV)

    # define the correlations
    correlation = ROOT.CorrelationScheme("CorrelationScheme") #carlo
    correlation.SetParametersOfInterest("SigXsecOverSM") #carlo 
    correlation.CorrelateParameter("78TeV::SigXsecOverSM,13TeV::SigXsecOverSM", "SigXsecOverSM") #always correlate the signal strengths 
    # non-trivial correlations
    # start with signal systematics that are always correlated (for now)
    correlation.CorrelateParameter("78TeV::ATLAS_BR_bb,13TeV::alpha_SysTheoryBRbb","alpha_SysTheoryBRbb")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryQCDscale_ggZH,13TeV::alpha_SysTheoryQCDscale_ggZH","alpha_SysTheoryQCDscale_ggZH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryQCDscale_qqVH,13TeV::alpha_SysTheoryQCDscale_qqVH","alpha_SysTheoryQCDscale_qqVH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryPDF_ggZH_8TeV,13TeV::alpha_SysTheoryPDF_ggZH","alpha_SysTheoryPDF_ggZH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryPDF_qqVH_8TeV,13TeV::alpha_SysTheoryPDF_qqVH","alpha_SysTheoryPDF_qqVH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryVHPt_8TeV,13TeV::alpha_SysVHNLOEWK","alpha_SysVHNLOEWK")

    if corrType!='jes8':
        correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_BJES_Response,78TeV::alpha_SysJetFlavB_7TeV,78TeV::alpha_SysJetFlavB_8TeV","alpha_SysJET_BJES_Response") # the only unfolded thing you correlate in the JES "weak" scheme

    # non-trivial correlations
    if 'btag' in corrType:
        all,corr_hash = corrType=='btag',dict()
        if all or corrType=='btag-b' or corrType=='btag-bl':
            #correlation.CorrelateParameter("78TeV::alpha_SysBTagB0Effic_7TeV,78TeV::alpha_SysBTagB0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_0", "alpha_SysFT_EFF_Eigen_B_0")
            correlation.CorrelateParameter("78TeV::alpha_SysBTagB0Effic_7TeV,78TeV::alpha_SysFT_EFF_Eigen_B_0_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_0", "alpha_SysFT_EFF_Eigen_B_0")
            if full_corr:
                for i in range(1,3):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagB{0}Effic_7TeV,78TeV::alpha_SysBTagB{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_{0}".format(i), "alpha_SysFT_EFF_Eigen_B_{0}".format(i))
        if all or corrType=='btag-c':
            correlation.CorrelateParameter("78TeV::alpha_SysBTagC0Effic_7TeV,78TeV::alpha_SysBTagC0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_C_0", "alpha_SysFT_EFF_Eigen_C_0")
            if full_corr:
                for i in range(1,4):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagC{0}Effic_7TeV,78TeV::alpha_SysBTagC{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_C_{0}".format(i), "alpha_SysFT_EFF_Eigen_C_{0}".format(i))
        if all or corrType=='btag-l' or corrType=='btag-bl':
            correlation.CorrelateParameter("78TeV::alpha_SysBTagLEffic_7TeV,78TeV::alpha_SysBTagL0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_Light_0", "alpha_SysFT_EFF_Eigen_Light_0")
            if full_corr:
                for i in range(1,4):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagL{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_Light_{0}".format(i), "alpha_SysFT_EFF_Eigen_Light_{0}".format(i))
        for bits in corr_hash:
            for i in range(corr_hash[bits][0]+1):
                correlation.CorrelateParameter("78TeV::alpha_SysBTag{1}{0}Effic_7TeV,78TeV::alpha_SysBTag{1}{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_{2}_{0}".format(i,bits[0],corr_hash[bits][1]), "alpha_SysFT_EFF_Eigen_{0}_{1}".format(corr_hash[bits][1],i))
    elif corrType in ['jes','jesu']:
        if corrType=='jesu':
            for np in unfolded_jes_nps(strong=full_corr):
                correlation.CorrelateParameter("78TeV::{0},13TeV::{1}".format(np[0],np[1]),np[1])
        if full_corr:
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_RhoTopology,78TeV::alpha_SysJetPileRho_Y2012_8TeV","alpha_SysJET_Pileup_RhoTopology")  	 #no 7 TeV
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Response,78TeV::alpha_SysJetFlavResp_(Wjets|Zjets|Diboson|Top)_[78]TeV_78TeV")  
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetMu,78TeV::alpha_SysJetMu_7TeV,78TeV::alpha_SysJetMu_8TeV","alpha_SysJET_Pileup_OffsetMu")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetNPV,78TeV::alpha_SysJetNPV_7TeV,78TeV::alpha_SysJetNPV_8TeV","alpha_SysJET_Pileup_OffsetNPV")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_PtTerm,78TeV::alpha_SysJetPilePt_Y2012_8TeV","alpha_SysJET_Pileup_PtTerm")	 #no 7 TeV
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_Modelling,78TeV::alpha_SysJetEtaModel_7TeV,78TeV::alpha_SysJetEtaModel_8TeV","alpha_SysJET_EtaIntercalibration_Modelling")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_TotalStat,78TeV::alpha_SysJetEtaStat_7TeV,78TeV::alpha_SysJetEtaStat_Y2012_8TeV","alpha_SysJET_EtaIntercalibration_TotalStat")
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_NonClosure,78TeV::alpha_SysJetNonClos_7TeV,78TeV::alpha_SysJetNonClos_8TeV","alpha_SysJET_EtaIntercalibration_NonClosure")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Top,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_ttbar_L2,78TeV::alpha_SysJetFlavComp_Top_7TeV,78TeV::alpha_SysJetFlavComp_Top_8TeV","alpha_SysJET_Flavor_Composition_Top")   
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Vjets,78TeV::alpha_SysJetFlavComp_Wjets_7TeV,78TeV::alpha_SysJetFlavComp_Wjets_8TeV,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   # W/Z+jets serparate in Run1, not Run2
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_VV,78TeV::alpha_SysJetFlavComp_Diboson_7TeV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   # VHVV separate in Run2, not Run1
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Wjets,78TeV::alpha_SysJetFlavComp_Wjets_7TeV,78TeV::alpha_SysJetFlavComp_Wjets_8TeV","alpha_SysJET_Flavor_Composition_Wjets")   
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Zjets,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,78TeV::alpha_SysJetFlavComp_Diboson_7TeV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   
    elif corrType=='jes8':
        correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_BJES_Response,78TeV::alpha_SysJetFlavB_8TeV","alpha_SysJET_BJES_Response")
        if full_corr:
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_Modelling,78TeV::alpha_SysJetEtaModel_8TeV","alpha_SysJET_EtaIntercalibration_Modelling")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_TotalStat,78TeV::alpha_SysJetEtaStat_Y2012_8TeV","alpha_SysJET_EtaIntercalibration_TotalStat")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_NonClosure,78TeV::alpha_SysJetNonClos_8TeV","alpha_SysJET_EtaIntercalibration_NonClosure")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetMu,78TeV::alpha_SysJetMu_8TeV","alpha_SysJET_Pileup_OffsetMu")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetNPV,78TeV::alpha_SysJetNPV_8TeV","alpha_SysJET_Pileup_OffsetNPV")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_PtTerm,78TeV::alpha_SysJetPilePt_Y2012_8TeV","alpha_SysJET_Pileup_PtTerm")	 #no 7 TeV
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_RhoTopology,78TeV::alpha_SysJetPileRho_Y2012_8TeV","alpha_SysJET_Pileup_RhoTopology")  	 #no 7 TeV
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Response,78TeV::alpha_SysJetFlavResp_(Wjets|Zjets|Diboson|Top)_[78]TeV_78TeV")   
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Top,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_ttbar_L2,78TeV::alpha_SysJetFlavComp_Top_8TeV","alpha_SysJET_Flavor_Composition_Top") 
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Vjets,78TeV::alpha_SysJetFlavComp_Wjets_8TeV,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   # W/Z+jets serparate in Run1, not Run2
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_VV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   # VHVV separate in Run2, not Run1
            
    combined.SetCorrelationScheme(correlation) #carlo
    combined.CollectMeasurements()
    print '~*~*~*~*~COLLECTED MEASUREMENTS!~*~*~*~*~'
    combined.CombineMeasurements()
    #combined.MakeAsimovData(True)
    combined.writeToFile(OutfileName)
    combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal)
    combined.writeToFile(OutfileName)
    combined.Print()

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Parse options for automated CombinationTool job launching')
  parser.add_argument('-c','--corrtype',help='Type of correlation scheme to use',default='normal')
  parser.add_argument('-a','--run1a',help='Use Run1 Asimov data instead of observed (go forth and fit with reckless abandon)',action='store_true')
  parser.add_argument('-u','--mu',help='Mu value for injected mu <0 --> obsData not Asimov',default=-99,type=float)
  parser.add_argument('-f','--fullcorr',help='Do full correlation schemes (if applicable)',action='store_true')
  parser.add_argument('-r','--run2path',help='Points to the Run2 WS (should contain the Asimov)',default='Run2WS-20170223/combined/125_wAsimov.root')
  parser.add_argument('-1','--run1path',help='Points to the Run1 WS (should contain the Asimov)',default='Run1WS/combined/125_wAsimov.root')
  parser.add_argument('--cond',help='Look at conditional Asimov data',action='store_true')
  args=parser.parse_args()

  try:
      loadcmd="{0}lib/libCombinationTool.so".format('/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/'if harvard else '/afs/cern.ch/work/s/stchan/vhbb/combtools/wsm/')
      loadcmd="lib/libCombinationTool.so"
      ROOT.gSystem.Load(loadcmd)
      print 'Successfully loaded',loadcmd
  except:
      print ("Could not load library. Make sure that it was compiled correctly.")

  do_combination(dataset78TeV=asimovDataName(0.51,True),dataset13TeV=asimovDataName(args.mu,args.cond),corrType=args.corrtype,full_corr=args.fullcorr,run2path=args.run2path, run1path=args.run1path)
