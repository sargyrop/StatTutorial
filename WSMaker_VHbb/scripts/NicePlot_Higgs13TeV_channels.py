import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
from math import sqrt

onlyOne=False
gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
SetAtlasStyle()

def transformName(inputname):
    return inputname.replace("_13TeV","").replace("13TeV","").replace("GBREAK","G_BREAK")

#some injection
##3.897  : From Javier  (combination)
##3.32196: From Valerio (combination) S+B asimov
##3.839  : From Valerio (l+jets)      S+B asimov

def main():
    gStyle.SetEndErrorSize(7.0);

    xlo = -1.5
    xhi = 10.5

#           NAME     index  up2sig, up1sig, down1sig, down2sig, nomin, obs, inj, mu, mu err 
# array elements 2-8: limits; 9:muhat 10:toterror+ 11:toterror- 12:staterror+ 13:staterror-
    value=[
        ["Combination",0, 4.79379, 3.5759, 1.85143, 1.37909, 2.56945, 4.13966, 3.38938 , 0.65, 0.76, 0.73, 0.59,0.56 ],
        ["0 lepton",1, 5.98854, 4.3701, 2.2393 , 1.668  , 3.10774, 4.21064, 3.91    , 0.67, 1.12, 1.02, 0.98,0.89  ],
        ["1 lepton"   ,2, 8.41   , 6.05  , 3.07   , 2.29   , 4.27   , 6.95231, 5.09    , 1.32, 1.42, 1.31, 1.08,1.00  ],
        ["2 lepton"   ,3, 8.41   , 6.05  , 3.07   , 2.29   , 4.27   , 6.95231, 5.09    , -0.09, 1.38, 1.31, 1.07,0.98  ]
        ]

    plots(value, "MVA13TeV_channels",xlo,xhi)

    value=[
        ["Combination",0, 4.79379, 3.5759, 1.85143, 1.37909, 2.56945, 4.13966, 3.38938 , 0.65, 0.76, 0.73, 0.59,0.56 ],
        ["WH",1, 5.98854, 4.3701, 2.2393 , 1.668  , 3.10774, 4.21064, 3.91    , 1.38, 1.43, 1.32, 1.09, 1.01  ],
        ["ZH"   ,2, 8.41   , 6.05  , 3.07   , 2.29   , 4.27   , 6.95231, 5.09    , 0.21, 1.01, 0.97, 0.82,0.77  ]
    ]

    plots(value, "MVA13TeV_WHZH",xlo,xhi)

def plots(value, name,xlo,xhi):


    value.append( ["Fake"   ,2, 8.41   , 6.05  , 3.07   , 2.29   , 4.27   , 6.95231, 5.09    , 1.0, 0.5, 0.6, 0.3,0.4  ] )
     
    theHisto2=TH2D("plot2","plot2",20,xlo,xhi,len(value),-0.5,len(value)-0.5)
    theHisto2.GetXaxis().SetTitle("Best fit #mu=#sigma/#sigma_{SM} for m_{H}=125 GeV")
    theHisto2.GetYaxis().SetLabelSize(0.06)
    for obj in value:
        if "Fake" in obj[0]: continue
        print "setting: "+str(obj[1])
        theHisto2.GetYaxis().SetBinLabel(obj[1]+1,obj[0])

    c2 = TCanvas("test2","test2",800,600)
    gPad.SetLeftMargin(0.20)
    gPad.SetTopMargin(0.10)
    theHisto2.Draw()

    tmp5=TH1D("tmp5", "tmp5", 1,1,2)
    tmp6=TH1D("tmp6", "tmp6", 1,1,2)
    tmp7=TH1D("tmp7", "tmp7", 1,1,2)
    tmp5.SetLineColor(1)
    tmp6.SetLineColor(8)
    tmp7.SetLineColor(4)


    g = TGraphAsymmErrors()
    g.SetLineWidth(4)
    g.SetMarkerColor(2)
    g.SetMarkerStyle(20)
    g.SetMarkerSize(1.6)

    g2 = TGraphAsymmErrors()
    g2.SetLineWidth(3)
    g2.SetMarkerColor(2)
    g2.SetMarkerStyle(20)
    g2.SetMarkerSize(1.6)
    g2.SetLineColor(8)

    g3 = TGraphAsymmErrors()
    g3.SetLineWidth(3)
    g3.SetMarkerColor(2)
    g3.SetMarkerStyle(20)
    g3.SetMarkerSize(1.6)
    g3.SetLineColor(4)


    for obj in value:
        if "Fake" in obj[0]: continue
        
        tothi = obj[10]
        totlo = obj[11]
        stathi = obj[12]
        statlo = obj[13]
        systhi = sqrt(tothi*tothi - stathi*stathi)
        systlo = sqrt(totlo*totlo - statlo*statlo)
        
        g.SetPoint(         obj[1], obj[9],     float(obj[1]) )
        #error- comes before error+
        g.SetPointError(    obj[1], obj[11],    obj[10],    float(0),   float(0) )
        g2.SetPoint(        obj[1], obj[9],     float(obj[1]) )
        #g2.SetPointError(   obj[1], obj[13],    obj[12],    float(0),   float(0) )
        g2.SetPointError(   obj[1], statlo,    stathi,    float(0),   float(0) )
        g3.SetPoint(        obj[1], obj[9],     float(obj[1]) )
        g3.SetPointError(   obj[1], systlo,    systhi,    float(0),   float(0) )
        print " --> Setting point: "+str(obj[9])+"  +/- "+str(obj[10])

        mystring1 = "#bf{%.2f}" % (obj[9])
        mystring2 = " #bf{#splitline{#plus %.2f}{#minus %.2f}}" % (obj[10], obj[11])
        #mystring3 = "#splitline{#plus %.2f}{#minus %.2f}" % (stathi, statlo)
        mystring3 = "#splitline{#plus %.2f  #plus %.2f}{#minus %.2f  #minus %.2f}" % (stathi, systhi, statlo, systlo)
        #mystring3 = "(#splitline{+ %.1f}{- %.1f})" % (obj[12], obj[13])
        
        sca = 0.20
        if(len(value)==4): sca = 0.20
        elif(len(value)==5): sca = 0.15

        offset1 = 0.54
        if (obj[9]<0): offset1 = offset1 -0.01
        myText(offset1,0.22+sca*obj[1],1, mystring1, 0.05)  # 0.042
        myText(0.60,0.22+sca*obj[1],1, mystring2, 0.04) ##8
        myText(0.71,0.22+sca*obj[1],1, "(", 0.05) ##8
        myText(0.73,0.22+sca*obj[1],1, mystring3, 0.04) ##8
        myText(0.90,0.22+sca*obj[1],1, ")", 0.05) ##8

#myText(0.70,0.12+0.20*3,1, " ( tot )", 0.05) ##8
#myText(0.82,0.12+0.20*3,1, "( stat )", 0.05) ##8

    myText(0.60,0.16+sca*(len(value)-1.0),1, "   #bf{Tot.}  ", 0.05) ##8
    myText(0.71,0.16+sca*(len(value)-1.0),1, "( Stat.    Syst. )", 0.05) ##8
    #myText(0.81,0.16+sca*(len(value)-1.0),1, " syst )", 0.05) ##8
    #myText(0.45,0.16+sca*(len(value)-0.6),1, "7 TeV: dijet mass analysis; 8 TeV: MVA", 0.04) ##8

    legend4=TLegend(0.28,0.73,0.60,0.85)
    legend4.SetTextFont(42)
    legend4.SetTextSize(0.04)
    legend4.SetFillColor(0)
    legend4.SetLineColor(0)
    legend4.SetFillStyle(0)
    legend4.SetBorderSize(0)
    legend4.AddEntry(tmp5,"Tot." ,"l")
    legend4.AddEntry(tmp6,"Stat.","l")
    #legend4.AddEntry(tmp7,"syst.","l")
    legend4.Draw("SAME")

    lall = TLine(1.0,-0.5,1.0,len(value)-0.5)
    lall.SetLineWidth(3)
    lall.SetLineColor(kGray)
    lall.Draw("SAME")

    g.Draw("SAMEP")
    g2.Draw("SAMEP")
    #g3.Draw("SAMEP")

    ATLAS_LABEL(0.15,0.935,1)
    myText(     0.30,0.935,1,"Internal")
    myText(     0.50,0.935,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt= 5.8 fb^{-1}",0.045)

    c2.SaveAs("Muhat_{0}.png".format(name))
    c2.SaveAs("Muhat_{0}.eps".format(name))
    c2.SaveAs("Muhat_{0}.pdf".format(name))
    c2.Update()



if __name__ == "__main__":
    gROOT.SetBatch(True)
    main()
