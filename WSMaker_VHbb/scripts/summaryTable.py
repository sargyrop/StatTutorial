import math
import sys
import os

def mergeTwoYields(muName,elName):
    theString=""
    if muName is elName and "multicolumn" in muName :
        return muName
    valueMu = 0. 
    errorMu = 0. 
    valueEl = 0. 
    errorEl = 0. 

    if "multicolumn" not in muName :
        valueMu=float(muName.split()[0])
        errorMu=float(muName.split()[-1])

    if "multicolumn" not in elName :
        valueEl=float(elName.split()[0])
        errorEl=float(elName.split()[-1])

    valueComb = valueMu + valueEl

    # squareEl=math.pow(errorEl/valueEl,2)
    # squareMu=math.pow(errorMu/valueMu,2)
    # errorComb = valueComb * math.sqrt ( squareEl + squareMu )

    squareEl=math.pow(errorEl,2)
    squareMu=math.pow(errorMu,2)
    errorComb = math.sqrt ( squareEl + squareMu )


    theString += " %.0f & %.0f "%(valueComb,errorComb)
    # valueComb + "$\\pm$" + errorComb
    return theString
    

def adjustName(name):
    if "pm" not in name: return name
    newName=""
    value=float(name.split()[0])
    error=float(name.split()[-1])
    print "Value: "+str(value)+" --> err: "+str(error)
    isTooSmall=(value<1)
    if isTooSmall:
        return " \multicolumn{2}{c|}{$<1$} "
    ### guessing the precision of the error
    theString=""
    if error<1:
        if error<0.1: error=0.1
        # theString+=" %.1f $\pm$ %.1f "%(value,error)
        theString+=" %.1f & %.1f "%(value,error)
    elif error<10:
        # theString+=" %.1f $\pm$ %.1f "%(value,error)
        theString+=" %.1f & %.1f "%(value,error)

    
    else:
        if error>100:
            ### need to round to the last digit
            value=int(round(value, -1))
            error=int(round(error, -1))
            # theString+=" %.0f $\pm$ %.0f "%(value,error)
            theString+=" %.0f & %.0f "%(value,error)
            print "rounding!!!!!"
            print "              "+theString
        else:
            # theString+=" %.0f $\pm$ %.0f "%(value,error)
            theString+=" %.0f & %.0f "%(value,error)
    return theString




fileread = open('Yields_GlobalFit_unconditionnal_mu1_final.tex', 'r').read()

fileread = fileread.replace("\\begin{document}\n","")


fileread = fileread.replace("% \\newcolumntype{d}{D{+}{\\hspace{-3pt}\\;\\pm\\;}{-1}}\n","")
fileread = fileread.replace("\\documentclass{article}\n","")
fileread = fileread.replace("\\usepackage{graphicx}\n","")
fileread = fileread.replace("\\end{document}\n","")
fileread = fileread.replace("\\centering\n","")

fileread = fileread.replace("\\small\n","")
fileread = fileread.replace("\\begin{table}\n","")
fileread = fileread.replace("\\end{table}\n","")

fileread = fileread.replace("\\begin{tabular}{l|c|}\n","")
fileread = fileread.replace("\\end{tabular}\n","")


fileread = fileread.replace("\n\\hline","")
fileread = fileread.replace("\\cline{2-2}\n","")
fileread = fileread.replace(" \\hline","")
fileread = fileread.replace(" & \\multicolumn{1}{c|}{","")

fileread = fileread.replace("Region\\_","")
fileread = fileread.replace("\\\\","")
fileread = fileread.replace("}","")


fileread = fileread.replace("BMax150\\_BMin75\\_","low-ptv ")
fileread = fileread.replace("BMin150\\_","high-ptv ")
fileread = fileread.replace("J2\\_T2\\_","2 jets ")
fileread = fileread.replace("incJet1\\_J3\\_T2\\_","3p jets ")

fileread = fileread.replace("J3\\_T2\\_","3 jets ")


fileread = fileread.replace("\\_Y2015\\_distmva\\_DSR"," SR")
fileread = fileread.replace("\\_Y2015\\_distmBBMVA\\_Dtopemucr"," CR")

fileread = fileread.replace("\\_Y2015\\_distmva\\_DWhfCR"," CR")
fileread = fileread.replace("\\_Y2015\\_distmva\\_DWhfSR"," SR")


regions = []

regions = fileread.split("\n\n\n")

test=True
regionNames=[]

Zl = []
Zcl = []
Zhf = []
Wl = []
Wcl = []
Whf = []
stop = []
ttbar = []
diboson = []
VH125 = []
Bkg = []
Signal = []
SignalExpected = []
SoB = []
SosqrtSB = []
data = []
multijetEl=[]
multijetMu=[]
multijet = []

doControlRegion = True # False # 

# print regions[1]
# exit()

samples=[]

for ireg in regions :
    samples=ireg.split("\n")


    flagZl = False
    flagZcl = False
    flagZhf = False
    flagWl = False
    flagWcl = False
    flagWhf = False
    flagstop = False
    flagttbar = False
    flagdiboson = False
    flagVH125 = False
    flagBkg = False
    flagSignal = False
    flagSignalExpected = False
    flagSoB = False
    flagSosqrtSB = False
    flagdata = False
    flagmultijetEl = False
    flagmultijetMu = False
       
    if not samples[0] :
        break

    # print "samples[0]", samples[0]
    # print "samples[1]", samples[1]
    
    if samples[0] == samples[1] :
        del samples[0]
        
    regionNames.append(samples[0])
    
    del samples[0]

    # for isample in samples :
    #     pair = isample.split(" & ")
    #     if pair[0] == "multijetEl" :
    #         multijetEl.append(pair[1])
    #     elif pair[0] == "multijetMu" :
    #         multijetMu.append(pair[1])
                            
    for isample in samples :
        pair = isample.split(" & ")
        pair[1]=adjustName(pair[1]) 
        
        # if test :
        #     print "pair", pair
        #     test=False
            

        if pair[0] == "Zl" :
            Zl.append(pair[1])
            flagZl = True

        elif pair[0] == "Zcl" :
            Zcl.append(pair[1])
            flagZcl = True

        elif pair[0] == "Zhf" :
            Zhf.append(pair[1])
            flagZhf = True
        elif pair[0] == "Wl" :
            Wl.append(pair[1])
            flagWl = True
        elif pair[0] == "Wcl" :
            Wcl.append(pair[1])
            flagWcl = True
        elif pair[0] == "Whf" :
            Whf.append(pair[1])
            flagWhf = True

        elif pair[0] == "stop" :
            stop.append(pair[1])
            flagstop = True
        elif pair[0] == "ttbar" :
            ttbar.append(pair[1])
            flagttbar = True
        elif pair[0] == "diboson" :
            diboson.append(pair[1])
            flagdiboson = True
        elif pair[0] == "VH125" :
            VH125.append(pair[1])
            flagVH125 = True
        elif pair[0] == "Bkg" :
            Bkg.append(pair[1])
            flagBkg = True

        elif pair[0] == "Signal" :
            Signal.append(pair[1])
            flagSignal = True
        elif pair[0] == "SignalExpected" :
            SignalExpected.append(pair[1])
            flagSignalExpected = True
        elif pair[0] == "S/B" :
            SoB.append(pair[1])
            flagSoB = True
        elif pair[0] == "S/sqrt(S+B)" :
            SosqrtSB.append(pair[1])
            flagSosqrtSB = True
        elif pair[0] == "data" :
            data.append(pair[1])
            flagdata = True
        elif pair[0] == "multijetEl" :
            multijetEl.append(pair[1])
            flagmultijetEl = True
        elif pair[0] == "multijetMu" :
            multijetMu.append(pair[1])
            flagmultijetMu = True

    if not flagZl              : Zl              .append("\multicolumn{2}{c|}{---}")
    if not flagZcl             : Zcl             .append("\multicolumn{2}{c|}{---}")
    if not flagZhf             : Zhf             .append("\multicolumn{2}{c|}{---}")
    if not flagWl              : Wl              .append("\multicolumn{2}{c|}{---}")
    if not flagWcl             : Wcl             .append("\multicolumn{2}{c|}{---}")
    if not flagWhf             : Whf             .append("\multicolumn{2}{c|}{---}")
    if not flagstop            : stop            .append("\multicolumn{2}{c|}{---}")
    if not flagttbar           : ttbar           .append("\multicolumn{2}{c|}{---}")
    if not flagdiboson         : diboson         .append("\multicolumn{2}{c|}{---}")
    if not flagVH125           : VH125           .append("\multicolumn{2}{c|}{---}")
    if not flagBkg             : Bkg             .append("\multicolumn{2}{c|}{---}")
    if not flagSignal          : Signal          .append("\multicolumn{2}{c|}{---}")
    if not flagSignalExpected  : SignalExpected  .append("\multicolumn{2}{c|}{---}")
    if not flagSoB             : SoB             .append("\multicolumn{2}{c|}{---}")
    if not flagSosqrtSB        : SosqrtSB        .append("\multicolumn{2}{c|}{---}")
    if not flagdata            : data            .append("\multicolumn{2}{c|}{---}")
    if not flagmultijetEl      : multijetEl      .append("\multicolumn{2}{c|}{---}")
    if not flagmultijetMu      : multijetMu      .append("\multicolumn{2}{c|}{---}")

# print "regionNames", regionNames


regionsToTable=[]
index=0

if doControlRegion :
    for ireg in regionNames :
        if ireg.find("CR") != -1 and ireg.find("L0") != -1:
            regionsToTable.append(index)
        index = index + 1
            
    index=0
    for ireg in regionNames :
        if ireg.find("CR") != -1 and ireg.find("L1") != -1:
            regionsToTable.append(index)
        index = index + 1
    
    index=0
    for ireg in regionNames :
        if ireg.find("CR") != -1 and ireg.find("L2") != -1:
            regionsToTable.append(index)
        index = index + 1
else :

    for ireg in regionNames :
        if ireg.find("SR") != -1 and ireg.find("L0") != -1:
            regionsToTable.append(index)
        index = index + 1

    index=0
    for ireg in regionNames :
        if ireg.find("SR") != -1 and ireg.find("L1") != -1:
            regionsToTable.append(index)
        index = index + 1
    
    index=0
    for ireg in regionNames :
        if ireg.find("SR") != -1 and ireg.find("L2") != -1:
            regionsToTable.append(index)
        index = index + 1


    
regionNames2=[]
# print "regionNames",regionNames
for ireg in regionNames :
    ireg=ireg.replace("L0 ","")
    ireg=ireg.replace("L1 ","")
    ireg=ireg.replace("L2 ","")
    ireg=ireg.replace("SR","")
    ireg=ireg.replace("CR","")

    ireg=ireg.replace("high-ptv ","")
    ireg=ireg.replace("low-ptv ","")
    ireg=ireg.replace("2 jets ","\\multicolumn{2}{c|}{2-jet}")
    ireg=ireg.replace("3 jets ","\\multicolumn{2}{c|}{3-jet}")
    ireg=ireg.replace("3p jets ","\\multicolumn{2}{c|}{$\geq$3-jet}")


    regionNames2.append(ireg)
    

for (mu,el) in zip (multijetMu,multijetEl) :
    multijetValue=mergeTwoYields(mu,el)
    multijet.append(multijetValue)


# print "multijetEl",multijetEl
# print "multijetMu",multijetMu
# print "multijet",multijet

    
# print multijetEl
# exit(0)
    
# print regionsToTable

print "\\begin{table}"
print "\\centering"
print "\\small"
print "\\begin{tabular}{|l|",
for aa in range(len(regionsToTable)) :
    print "r@{\,$\pm$\,}l|",
print "}"
print "\\hline"
print "",
print "\\multirow{2}{*}{",
if doControlRegion : print "Control regions",
else : print "Signal regions",
print "}",

if not doControlRegion :
    print "& \\multicolumn{4}{c|}{0-lepton} ",
print "& \\multicolumn{4}{c|}{1-lepton} ",
print "& \\multicolumn{8}{c|}{2-lepton} ",
print "\\\\"

if doControlRegion :
    print "\\cline{2-13}"
else :
    print "\\cline{2-17}"

if not doControlRegion :
    print "& \\multicolumn{4}{c|}{$p_\\mathrm{T}^V > 150$~GeV, 2-tag } ",
print "& \\multicolumn{4}{c|}{$p_\\mathrm{T}^V > 150$~GeV, 2-tag } ",
print "& \\multicolumn{4}{c|}{$75 < p_\\mathrm{T}^V < 150$~GeV, 2-tag } ",
print "& \\multicolumn{4}{c|}{$p_\\mathrm{T}^V > 150$~GeV, 2-tag } ",
print "\\\\"

print "\\hline"

print "Sample",

for ireg in regionsToTable :
    print "&",regionNames2[ireg],
print "\\\\"
print "\\hline"

print "$Z+ll$",
for ireg in regionsToTable :
    print "&",Zl[ireg],
print "\\\\"

print "$Z+cl$",
for ireg in regionsToTable :
    print "&",Zcl[ireg],
print "\\\\"

print "$Z+hf$",
for ireg in regionsToTable :
    print "&",Zhf[ireg],
print "\\\\"


print "$W+ll$",
for ireg in regionsToTable :
    print "&",Wl[ireg],
print "\\\\"

print "$W+cl$",
for ireg in regionsToTable :
    print "&",Wcl[ireg],
print "\\\\"

print "$W+hf$",
for ireg in regionsToTable :
    print "&",Whf[ireg],
print "\\\\"

print "Single-top",
for ireg in regionsToTable :
    print "&",stop[ireg],
print "\\\\"

# print "MultijetEl",
# for ireg in regionsToTable :
#     print "&",multijetEl[ireg],
# print "\\\\"

# print "MultijetMu",
# for ireg in regionsToTable :
#     print "&",multijetMu[ireg],
# print "\\\\"

print "$t \\bar t$",
for ireg in regionsToTable :
    print "&",ttbar[ireg],
print "\\\\"

print "Multijet",
for ireg in regionsToTable :
    print "&",multijet[ireg],
print "\\\\"

print "Diboson",
for ireg in regionsToTable :
    print "&",diboson[ireg],
print "\\\\"

# print "VH125",
# for ireg in regionsToTable :
#     print "&",VH125[ireg],
# print "\\\\"
# print "\\hline"
print "\\hline"

print "Total bkg.",
for ireg in regionsToTable :
    print "&",Bkg[ireg],
print "\\\\"
print "\\hline"

# print "Signal",
print "VH (bb) (fit)",
for ireg in regionsToTable :
    print "&",Signal[ireg],
print "\\\\"
print "\\hline"

# print "SignalExpected",
# for ireg in regionsToTable :
#     print "&",SignalExpected[ireg],
# print "\\\\"
# print "\\hline"

# print "S/B",
# for ireg in regionsToTable :
#     print "&",SoB[ireg],
# print "\\\\"

# print "S/sqrt(S+B)",
# for ireg in regionsToTable :
#     print "&",SosqrtSB[ireg],
# print "\\\\"
# print "\\hline"

print "Data",
for ireg in regionsToTable :
    print "& \\multicolumn{2}{c|}{",data[ireg],"} ",
print "\\\\"

print "\\hline"

print "\\end{tabular}"
print "\\end{table}"
