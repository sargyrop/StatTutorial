#!/usr/bin/env python

import sys
import os
import AnalysisMgr_VHbbRun2 as Mgr
import BatchMgr as Batch

InputVersion = "SMVH_mva_v1"

def add_config(mass, lumi, var2tag = 'mva', var1tag = 'dRBB', unc = "Systs", one_bin=False,mcstats=True, 
               ZeroLep=True, OneLep=True, TwoLep=True, debug=False, use1tag=False, use4pjet=False, doptvbin=True):
    conf = Mgr.WorkspaceConfig_VHbbRun2(unc, InputVersion=InputVersion, MassPoint=mass, OneBin=one_bin,
                                   UseStatSystematics=mcstats, Debug=debug,
                                   SkipUnkownSysts=True,
                                   DoShapePlots=False,DoSystsPlots=False,DeleteNormFiles=True ,
                                   DoLumirescale=lumi,
                                   )
    conf.set_regions(var2tag=var2tag, var1tag=var1tag, zero_lepton=ZeroLep, one_lepton=OneLep, two_lepton=TwoLep, use1tag=use1tag, use4pjet=use4pjet, doptvbin=doptvbin)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + unc + "_" + mass + "_" + str(lumi) + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + unc + "_" + mass + "_" + str(lumi) + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag

    return conf.write_configs(fullversion)

if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: Analysis_VHbbRun2.py <outversion>"
        exit(0)

    outversion = sys.argv[1]


    # For running on existing workspaces (-k option):
    #WSlink = "/afs/cern.ch/user/g/gwilliam/workspace/WSMaker/workspaces/MonoH0708152p.180815_MonoH_13TeV_180815_Systs_Scalar_2000/combined/2000.root"
    #os.system("ls -lh " + WSlink)

    ########## create config files

    print "Adding config files..."
    configfiles_stat = []
    configfiles = []
    mass = [125]
    vs2tag = ['BDT']
    vs1tag = ['dRBB']
    lumi = [10]
    use1tag = False
    use4pjet = False
    doptvbin = False
    syst_type="StatOnly"

    for l in lumi:
        for m in mass:
            for var2tag in vs2tag:
                for var1tag in vs1tag:
                    configfiles += add_config(str(m), l, var2tag=var2tag, var1tag=var1tag, unc=syst_type, one_bin=False, mcstats=True, 
                                              ZeroLep=True, OneLep=False, TwoLep=False, 
                                              use1tag=use1tag, use4pjet=use4pjet, doptvbin=doptvbin )
    for fi in configfiles_stat:
        print fi
    for fi in configfiles:
        print fi

    ########## define tasks

    tasks = ["-w"]             # create workspace (do always)
    # tasks += ["-k", WSlink]
    tasks += ["-l", 1]         # limit
    tasks += ["-s", 1]         # significance
    tasks += ["--fcc", "4,10@{MassPoint}"]  # fitCrossChecks
    tasks += ["-u", 1]         # breakdown of muhat errors stat/syst/theory
    # # requires fitCrossChecks:
    # tasks += ["-m", "4,10"]     # reduced diag plots (quick)
    # # tasks += ["-b", "2"]       # unfolded b-tag plots (quick)
    # the '!' signifies the reference workspace, & signifies to do sum plots
    # tasks += ["-p", "0,1@{MassPoint}"]       # post-fit plots (CPU intense)
    # # tasks += ["-p", "0,1;@{MassPoint};blah,blah"]       # post-fit plots (CPU intense)
    # # tasks += ["-r", "{MassPoint}", "-n", "{MassPoint}"]   # NP ranking
    # tasks += ["-t", "0,1@{MassPoint}"]       # yield tables (CPU intense)
    # tasks += ["-a", "current;p,4,10"]       # yield ratio tables

    ########## submit jobs

    # in case you want to run something locally
    #Batch.run_local_batch(configfiles, outversion, tasks)
    # Batch.run_lxplus_batch(configfiles, outversion, tasks, '8nh')

    # adjust to recover failed ranking subjobs
    #redoSubJob = -1

    #if redoSubJob < 0:
        #print "Submitting stat only jobs..."
        #Batch.run_lxplus_batch(configfiles_stat, outversion, ["-w", "-l", 1,"-s",1], '2nd')
        #print "Submitting systs jobs..."
        #Batch.run_lxplus_batch(configfiles, outversion, tasks, '2nd')

    #print "Submitting rankings..."
    #for m in mass:
    #WSlink = "/afs/cern.ch/user/n/nmorange/Hbb/WSMaker/workspaces/21013.testJERsamplesFine_AZh_13TeV_testJERsamplesFine_Systs_500/combined/500.root"
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "{MassPoint}"], '8nh', jobs=20, subJob=redoSubJob)
    #Batch.run_local_batch(configfiles, outversion, ["-w", "-k", WSlink, "-l", "0,{MassPoint}"])
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "{0}".format(m)], '1nd', jobs=20, subJob=redoSubJob)
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-r", "{MassPoint}"], '2nd', jobs=20, subJob=redoSubJob)
    #Batch.run_local_batch(configfiles, outversion, ["-w", "-k", WSlink, "-n", "{MassPoint}"])

    ########## non-default stuff. Warning: don't submit rankings / toys / NLL scans with the same outversion and nJobs!
    #print "Submitting toys..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "6"], '2nd', jobs=50, subJob=redoSubJob)
    #print "Submitting NLL scans..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "7,2,doNLLscan"], '2nd', jobs=50, subJob=redoSubJob)
