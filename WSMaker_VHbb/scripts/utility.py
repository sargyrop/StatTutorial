class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

def Error(info):
  print bcolors.FAIL + info + bcolors.ENDC

def Sucess(info):
    print bcolors.OKGREEN + info + bcolors.ENDC

def Warn(info):
    print bcolors.WARNING + info + bcolors.ENDC
