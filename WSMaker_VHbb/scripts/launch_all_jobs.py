#!/usr/bin/env python
# old
import sys
import os
import AnalysisMgr_VHbbRun2 as Mgr
import BatchMgr as Batch
import time,copy
import signal

# Inputs
InputVersion = "SMVHVZ_LHCP17_MVA_v06" # cut-based inputs are change automaticaly if GlobalRun is True
# InputVersion = "SM_2L_v62" # cut-based inputs for two letpns are the same as for MVA (added variable mBB)

GlobalRun = True # caution: when true, it will change the following settings based on the sys.argv[1]
# How to run
run_on_batch = True # False # 

doCutBase = False
doDiboson = False
# Channels
#channels = ["0", "1", "2", "012"]
channels = ["2"]

# Syst or not
syst_type = ["Systs"]#, "StatOnly", "MCStat"]

#Blinding / injection
signalInjection = 0 # DEFAULT 0: no injection
doExp = "1" # "0" to run observed, "1" to run expected only


doPostFit=False

# What to run
createSimpleWorkspace = False # True
runPulls = True
runBreakdown = True
runRanks = True
runLimits = False
runP0 = True
runToyStudy = False

# Turn on additional debug plots
doftonly = False
doplots = True

masses = [125]
doptvbin = False # will be reset depending on vs2tag in AnalysisMgr_VHbbRun2
vs1tag = ['dRBB']
use1tag = False
runRankingOnPostfitAsimov = True

if GlobalRun:
  if sys.argv[1].endswith('.01'):
    print 'run set 1'
    doExp = "1"
    runPulls = True
    runBreakdown = True
    runRanks = True
    runLimits = False
    runP0 = True
    doftonly = False
    doplots = True
    runRankingOnPostfitAsimov = True
  elif sys.argv[1].endswith('.02'):
    print 'run set 2'
    doExp = "0"
    runPulls = False
    runBreakdown = False
    runRanks = False
    runLimits = False
    runP0 = True
    doftonly = True
    doplots = False
  elif sys.argv[1].endswith('.03'):
    print 'run set 3'
    doExp = "1"
    runPulls = False
    runBreakdown = False
    runRanks = True
    runLimits = False
    runP0 = False
    doftonly = True
    doplots = False
    runRankingOnPostfitAsimov = False
  elif sys.argv[1].endswith('.05'):
    print 'run set 5'
    doExp = "1"
    runPulls = False
    runBreakdown = True
    runRanks = False
    runLimits = False
    runP0 = True
    doftonly = True
    doplots = False
    runRankingOnPostfitAsimov = False
  else :
    print "In GlobalRun, the version should end in '.0X', where X is"
    print "1 for doExp set at '1' and runRankingOnPostfitAsimov set at True"
    print "2 for doExp set at '0' and runRankingOnPostfitAsimov set at True"
    print "3 for doExp set at '1' and runRankingOnPostfitAsimov set at False"
    print "exiting..."
    exit(0)


  if sys.argv[1].startswith('YMVA.'): # Multi-variate analysis for signal VH
    vs2tag = ['mva']

  elif sys.argv[1].startswith('YDBA.'): # Multi-variate analysis for dibosons
    doCutBase = False
    doDiboson = True
    vs2tag = ['mvadiboson']

  elif sys.argv[1].startswith('YCBA.'): # Cut-based analysis for signal VH
    doCutBase = True
    doDiboson = False
    vs2tag = ['mBB']
    doptvbin = True
    InputVersion = "SMVH_LHCP17_CUT_v08" # CUT

  elif sys.argv[1].startswith('YCDB.'): # Cut-based analysis for dibosons
    doCutBase = True
    doDiboson = True
    vs2tag = ['mBB']
    doptvbin = True
    InputVersion = "SMVH_LHCP17_CUT_v08" # CUT

  else :
    print "In GlobalRun, the version should start with in 'Yxxx.', where xxx is"
    print "YMVA for MVA signal analysis"
    print "YDBA for MVA diboson analysis"
    print "YCBA for cut-based signal analysis"
    print "YCDB for cut-based diboson analysis"
    print "exiting..."
    exit(0)

  if sys.argv[1].find('.0.') == 4 :
    channels = ["0"]
  elif sys.argv[1].find('.1.') == 4 :
    channels = ["1"]
  elif sys.argv[1].find('.2.') == 4 :
    channels = ["2"]
  elif sys.argv[1].find('.02.') == 4 : # Run 0 and 2 leptons combined
    channels = ["02"]
  elif sys.argv[1].find('.012.') == 4 :
    channels = ["012"]
  else :
    print "In GlobalRun, one can set the lepton analysis adding '.X??.' (e.g. YMVA.2.01 to run MVA signal on 2-lepton)."
    print "Options are '0', '1', '2', '02' and '012'"
    print "(option '02' is for 0 and 2 lepton together but was not tested yet)"
    print "The channel is currently", channels
    print "Waiting 10 seconds before continuing if you want to kill the job ( several CTRL+c 's ). Dont forget to delete the config file created."
    os.system("sleep 10")

  if sys.argv[1].find('.topemu.') != -1 :
    InputVersion = "SM_topemu_v62" # cut-based inputs for two letpns are the same as for MVA (added variable mBB)



else :
  # Lesser used flags:
  if doCutBase:
    vs2tag = ['mBB']
    doptvbin = True
    InputVersion = "SMVH_LHCP17_CUT_v08" # CUT

  elif doDiboson:
    vs2tag = ['mvadiboson']
  else :
    vs2tag = ['mva']

systematic2use="None"
types2use="BMin,J,D,L" # <-- This are all options: ptvbin, #jets, SR/CR and # leptons

if len(sys.argv) is not 2:
  doplots=False                 # shape,syst and suspicious plots are the same without decorrelation
  systematic2use=sys.argv[2]
  if len(sys.argv) is not 3:
    types2use=sys.argv[3]

# print "doplots",doplots
# exit(0)


#the argument for ConfigFlags; delimited and camel-cased for readability (cf. scripts/AnalysisMgr.py, parse_config_flags for full options):
baseline_configs = {'UseJet21NP':True,'CutBase':False,'DoDiboson':False,'FitWHZH':False, 'FitVHVZ':False,'DoSystsPlots':False,"DoShapePlots":False, 'StatThreshold':0.0, 'PlotsFormat':"eps", 'CreateROOTfiles':True, 'DecorrSys':systematic2use, 'DecorrTypes':types2use}#, 'DecorrPOI':'L'}
if doCutBase:
    baseline_configs.update({"CutBase":True})
if doDiboson:
    baseline_configs.update({"DoDiboson":True})

if os.environ['USER'] == 'mdacunha' : 
  baseline_configs.update({"CreateROOTfiles":True})

if doPostFit:
    baseline_configs.update({"PostFit":True})
    baseline_configs.update({"DefaultFitConfig":True})

baseline_configs_SimpleWS = copy.deepcopy(baseline_configs)
if createSimpleWorkspace and doplots:
    baseline_configs_SimpleWS.update({"DoAllFTJETShapePlots":True} if doftonly else {'DoSystsPlots':True,"DoShapePlots":True, 'SmoothingControlPlots':True, "ShowMCStatBandInSysPlot": True})


def add_config(config, mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, doptvbin=True, doPostFit=False):
    config.update({'InputVersion':InputVersion,'MassPoint':mass,'DoInjection':signalInjection,'SkipUnkownSysts':True})
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, **config)

    if "0" in channel:
        conf.append_regions(var2tag, var1tag, True, False, False,
                            use1tag, False, doptvbin)
    if "1" in channel:
        conf.append_regions(var2tag, var1tag, False, True, False,
                            use1tag, False, doptvbin, doPostFit)
    if "2" in channel:
        conf.append_regions(var2tag, var1tag, False, False, True,
                            use1tag, False, doptvbin, doPostFit)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag

    return conf.write_configs(fullversion)


if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: launch_default_jobs.py <outversion>"
        exit(0)

    outversion = sys.argv[1]
    """
    print "WARNING: if you haven't touched the base configuration, prepare to"
    print "see your lxbatch quota die quickly"

    print "I'll give you 5 seconds to abort..."
    time.sleep(5)
    """

    print "Adding config files..."

    print "Let's start by a few jobs where we run simple fits to get pulls"
    print "and other things when they are ready"
    configfiles = []
    configfiles_simpleWS = []
    if signalInjection is not 0:
        doExp="1"

    for sys in syst_type:
        for c in channels:
            for m in masses:
                for var2tag in vs2tag:
                    for var1tag in vs1tag:
                        configfiles += add_config(baseline_configs, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin, doPostFit=doPostFit)
                        configfiles_simpleWS += add_config(baseline_configs_SimpleWS, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin, doPostFit=doPostFit)
    for fi in configfiles:
        print fi

    dataForRanking = "obsData"
    if doExp == '1':
        if runRankingOnPostfitAsimov:
            dataForRanking = "asimovData_paramsVals_GlobalFit_mu1.00,conditionalGlobs_1"
        else:
            dataForRanking = "asimovData"




    # outversion=outversion.replace("SysJET_JER_SINGLE_NP","JER")
    # outversion=outversion.replace("SysTTbarMBB","ttMBB")
    outversion=outversion.replace("Sys","")
    outversion=outversion.replace("BMin","B")
    # The following information already appears in the WS name as _CHANNEL_
    outversion=outversion.replace(".0.",".")
    outversion=outversion.replace(".1.",".")
    outversion=outversion.replace(".2.",".")
    outversion=outversion.replace(".02.",".")
    outversion=outversion.replace(".012.",".")


    if run_on_batch:

        if createSimpleWorkspace:
            print 'creating simple ws named %s' % outversion
            Batch.run_lxplus_batch(configfiles_simpleWS, outversion+"_fullRes", ["-w"], '1nh')

        # hopefully queue 8nh is enough for now.
        if runPulls:
            Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                   ["-w", "--fcc", "5,9@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"],
                                   '8nh')
        if runBreakdown:
            Batch.run_lxplus_batch(configfiles, outversion+"_breakdown",
                                   ["-w", "-l", doExp+",{MassPoint}", "-u", doExp+",{MassPoint},10"],
                                   '8nh')

        if runLimits:
            #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-l", doExp+",{MassPoint}"],mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_limits",
                                   ["-w", "-l", doExp+",{MassPoint}"],
                                   '8nh')
        if runP0:
            #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-s", doExp+",{MassPoint}"],mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_p0",
                                   ["-w", "-s", doExp+",{MassPoint}"],
                                   '8nh')
        if runRanks:
            # create workspaces locally
            commands = ["-w"]
            if doExp == "1" and runRankingOnPostfitAsimov:
                commands.extend(["--fcc", "5s@{MassPoint}"])
            Batch.run_local_batch(configfiles, outversion+"_ranking", commands)
            # Then run the rankings on batch
            #Batch.run_slurm_batch(configs, out+"_fullRes",cmd_opts,jobtype='_rank',mem='16384',jobs=10)
            Batch.run_lxplus_batch(configfiles, outversion+"_ranking",['-w', '-k', "jobs", '-r','{MassPoint},'+dataForRanking],
                                   jobs=10, queue='8nh')
        if runToyStudy:
            # create workspaces locally
            Batch.run_local_batch(configfiles, outversion+"_toys", ["-w"])
            # Then run the toys on batch
            Batch.run_lxplus_batch(configfiles, outversion+"_toys",['-w', '-k', "jobs", "--fcc", "6"],
                                   jobs=50, queue='1nd')

    else: # local running
        commands = ["-w"]
        #commands = []
        if runPulls:
            commands.extend(["--fcc", "5s,9@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"])
        if runLimits or runBreakdown:
            commands.extend(["-l", doExp+",{MassPoint}"])
        if runBreakdown:
            commands.extend(["-u", doExp+",{MassPoint}"])
        if runP0:
            commands.extend(["-s", doExp+",{MassPoint}"])
        if runRanks:
            commands.extend(['-r','{MassPoint},'+dataForRanking])
        if runToyStudy:
            commands.extend(["--fcc", "6@{MassPoint}"])
        Batch.run_local_batch(configfiles_simpleWS, outversion+"_fullRes", commands)

































# baseline_configs_SimpleWS = copy.deepcopy(baseline_configs)
# if createSimpleWorkspace and doplots:
#     baseline_configs_SimpleWS.update({"DoAllFTJETShapePlots":True} if doftonly else {'DoSystsPlots':True,"DoShapePlots":True, 'SmoothingControlPlots':True, "ShowMCStatBandInSysPlot": True})

# # baseline_configs_noFCCplotsWS = copy.deepcopy(baseline_configs)
# # baseline_configs_noFCCplotsWS.update({'DoAllFTJETShapePlots':False, 'DoSystsPlots':False,"DoShapePlots":False, 'SmoothingControlPlots':False, "ShowMCStatBandInSysPlot": False})


# def add_config(config, mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, doptvbin=True, doPrimary=True):
#     print config
#     config.update({'InputVersion':InputVersion,'MassPoint':mass,'DoInjection':signalInjection,'SkipUnkownSysts':True})
#     conf = Mgr.WorkspaceConfig_VHbbRun2(syst, **config)


#     if "0" in channel:
#         conf.append_regions(var2tag, var1tag, True, False, False,
#                             use1tag, False, doptvbin)
#     if "1" in channel:
#         conf.append_regions(var2tag, var1tag, False, True, False,
#                             use1tag, False, doptvbin)
#     if "2" in channel:
#         conf.append_regions(var2tag, var1tag, False, False, True,
#                             use1tag, False, doptvbin)

#     if (var2tag is not var1tag) and (use1tag is True) :
#         fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag + "2tag_" + var1tag + "1tag"
#     else:
#         fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag

#     if not doPrimary :
#         fullversion += "_secondary"


#     # return conf.write_configs(fullversion)


# if __name__ == "__main__":

#     if len(sys.argv) is not 2:
#       if len(sys.argv) is not 3:
#         if len(sys.argv) is not 4:
#           print "Usage: python launch_all_jobs.py <outversion> [Syst_to_decorr] [Decorr_types]"
#           exit(0)

#     outversion = sys.argv[1]
#     if len(sys.argv) is not 2:
#       outversion = sys.argv[1] + "_" + sys.argv[2]
#       if len(sys.argv) is not 3:
#         outversion = sys.argv[1] + "_" + sys.argv[2] + "_Decorr" + sys.argv[3]
#         # if sys.argv[2] == "SysTTbarMBB" and sys.argv[3] == "BMin" :
#         #   outversion = sys.argv[1] + "_" + "ttMBB"

#         # if sys.argv[2] == "SysJET_JER_SINGLE_NP" :
#         #   outversion = sys.argv[1] + "_JER_Decorr" + sys.argv[3]


#       #   if sys.argv[3] is not in ["BMin","J","D","L"] :
#       #     print "The decorrelation should be only one of the following: BMin,J,D,L"
#       #     print "For combination between them please hard code variable types2use and rerun like this:"
#       #     print "python launch_all_jobs.py <outversion> [Syst_to_decorr]"
#       #     print "Note: the decorrelation type will not appear in the WS name!"
#       #     exit(0)

#       # if sys.argv[2].find("Sys") == -1 :
#       #   print "Systematic to decorrelate should start with Sys."
#       #   exit(0)



#     # outversion=outversion.replace("SysJET_JER_SINGLE_NP","JER")
#     # outversion=outversion.replace("SysTTbarMBB","ttMBB")
#     outversion=outversion.replace("Sys","")
#     outversion=outversion.replace("BMin","B")
#     # The following information already appears in the WS name as _CHANNEL_
#     outversion=outversion.replace(".0.",".")
#     outversion=outversion.replace(".1.",".")
#     outversion=outversion.replace(".2.",".")
#     outversion=outversion.replace(".02.",".")
#     outversion=outversion.replace(".012.",".")


#     """
#     print "WARNING: if you haven't touched the base configuration, prepare to"
#     print "see your lxbatch quota die quickly"

#     print "I'll give you 5 seconds to abort..."
#     time.sleep(5)
#     """

#     print "Adding config files..."

#     print "Let's start by a few jobs where we run simple fits to get pulls"
#     print "and other things when they are ready"
#     configfiles = []
#     configfiles_simpleWS = []
#     configfiles_noFCCplotsWS = []
#     if signalInjection is not 0:
#         doExp="1"

#     print "baseline_configs -> ",baseline_configs
#     print ""
#     print "baseline_configs_SimpleWS -> ",baseline_configs_SimpleWS
#     # print ""
#     # print "baseline_configs_noFCCplotsWS -> ",baseline_configs_noFCCplotsWS
#     # exit(0)

#     for sys in syst_type:
#         for c in channels:
#             for m in masses:
#                 for var2tag in vs2tag:
#                     for var1tag in vs1tag:
#                         configfiles += add_config(baseline_configs, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin)
#                         configfiles_simpleWS += add_config(baseline_configs_SimpleWS, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin)
#                         # configfiles_noFCCplotsWS += add_config(baseline_configs_noFCCplotsWS, str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, doptvbin=doptvbin, doPrimary=False)
#     for fi in configfiles:
#         print fi

#     # print "configfiles",configfiles
#     # print "configfiles_simpleWS",configfiles_simpleWS
#     # print "configfiles_noFCCplotsWS",configfiles_noFCCplotsWS
#     # exit(0)


#     dataForRanking = "obsData"
#     if doExp == '1':
#         if runRankingOnPostfitAsimov:
#             dataForRanking = "asimovData_paramsVals_GlobalFit_mu1.00,conditionalGlobs_1"
#         else:
#             dataForRanking = "asimovData"

#     if run_on_batch:

#         # if createSimpleWorkspace:
#         #     print 'creating simple ws named %s' % outversion
#         #     Batch.run_lxplus_batch(configfiles_simpleWS, outversion+"_fullRes", ["-w"], '1nh')

#         # hopefully queue 8nh is enough for now.
#         if runPulls:
#             commands = ["-w", "--fcc", "5s,9@{MassPoint}", "-m", "5,9"]
#             if systematic2use == "None" :
#               commands.extend(["-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"])
#             Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",commands,'8nh')
#                                    # ["-w", "--fcc", "5,9@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"],'8nh')
#         if runBreakdown:
#             Batch.run_lxplus_batch(configfiles_noFCCplotsWS, outversion+"_breakdown",
#                                    ["-w", "-l", doExp+",{MassPoint}", "-u", doExp+",{MassPoint}"],
#                                    '8nh')

#         if runLimits:
#             #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-l", doExp+",{MassPoint}"],mem='16384',jobs=10)
#             Batch.run_lxplus_batch(configfiles, outversion+"_limits",
#                                    ["-w", "-l", doExp+",{MassPoint}"],
#                                    '8nh')
#         if runP0:
#             #Batch.run_slurm_batch(configfiles, outversion+"_limits",["-w", "-s", doExp+",{MassPoint}"],mem='16384',jobs=10)
#             Batch.run_lxplus_batch(configfiles, outversion+"_p0",
#                                    ["-w", "-s", doExp+",{MassPoint}"],
#                                    '8nh')
#         if runRanks:
#             # create workspaces locally
#             commands = ["-w"]
#             if doExp == "1" and runRankingOnPostfitAsimov:
#                 commands.extend(["--fcc", "5s@{MassPoint}"])
#             commands.extend(['-r','{MassPoint},'+dataForRanking])
#             Batch.run_lxplus_batch(configfiles, outversion+"_ranking", commands, jobs=1, queue='8nh')
#             # Then run the rankings on batch
#             #Batch.run_slurm_batch(configs, out+"_fullRes",cmd_opts,jobtype='_rank',mem='16384',jobs=10)
#             # Batch.run_lxplus_batch(configfiles_noFCCplotsWS, outversion+"_ranking",['-w', '-k', "jobs", '-r','{MassPoint},'+dataForRanking],
#             #                        jobs=10, queue='8nh')

#         if runToyStudy:
#             # create workspaces locally
#             Batch.run_local_batch(configfiles, outversion+"_toys", ["-w"])
#             # Then run the toys on batch
#             Batch.run_lxplus_batch(configfiles, outversion+"_toys",['-w', '-k', "jobs", "--fcc", "6"],
#                                    jobs=50, queue='1nd')

#     else: # local running
#         commands = ["-w"]
#         #commands = []
#         if runPulls:
#           if systematic2use != "None" :
#             commands.extend(["--fcc", "5s,9@{MassPoint}", "-m", "5,9"])
#           if systematic2use == "None" :
#             commands.extend(["--fcc", "5s,9@{MassPoint}", "-m", "5,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}"])
#         if runLimits or runBreakdown:
#             commands.extend(["-l", doExp+",{MassPoint}"])
#         if runBreakdown:
#             commands.extend(["-u", doExp+",{MassPoint}"])
#         if runP0:
#             commands.extend(["-s", doExp+",{MassPoint}"])
#         if runRanks:
#             commands.extend(['-r','{MassPoint},'+dataForRanking])
#         if runToyStudy:
#             commands.extend(["--fcc", "6@{MassPoint}"])
#         Batch.run_local_batch(configfiles_simpleWS, outversion+"_fullRes", commands)

