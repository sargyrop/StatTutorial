#lxplus only

export ANALYSISTYPE="VHbbRun2"

if [[ ${0} == "bash" ]]; then # sourcing this script in the bash shell
  export ANALYSISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
else # zsh shell ( at least not bash )
  export ANALYSISDIR="$( cd "$( dirname "$0" )" && pwd )"
fi

if [[ $USER == "changqia" ]]; then
  if [ ! -f "launch_default_jobs.py" ]; then
    echo "Soft link of launch job doesn't exit! Generating it!"
    ln -s scripts/launch_default_jobs.py
  fi
  if [ ! -f "AdvGatherResults.sh" ]; then
    echo "Soft link of Merging script doesn't exit! Generating it!"
    ln -s scripts/AdvGatherResults.sh
  fi
fi

echo "Setting ANALYSISTYPE to : "$ANALYSISTYPE

source WSMakerCore/setup.sh

export NCORE=4
